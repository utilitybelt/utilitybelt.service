﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib.Settings;
using UtilityBelt.Networking;
using Decal.Adapter;
using System.IO;
using UtilityBelt.Networking.Lib;
using static System.Net.Mime.MediaTypeNames;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Networking.Messages;
using static AcClient.UIQueueManager;
using System.Threading;
using WattleScript.Interpreter;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using UtilityBelt.Scripting.Actions;
using UtilityBelt.Service.Lib.UBNetModule;
using System.Collections.Concurrent;

namespace UtilityBelt.Service.Lib {
    public class UBNet : ISetting, IDisposable {
        [Summary("Host to connect to. If the client starts its own server, it will also use this as the listening host.")]
        public Setting<string> Host = new Global<string>("127.0.0.1");

        [Summary("Port to connect to. If the client starts its own server, it will also use this as the listening port.")]
        public Setting<int> Port = new Global<int>(21424);

        [Summary("Attempt to start a local UBNet server if none exists")]
        public Setting<bool> StartServer = new Global<bool>(true);

        [Summary("Server idle timeout, in seconds. The server will shut itself down after this many seconds without any connected clients. This is only applicable if this client starts the server.")]
        public Setting<int> Timeout = new Global<int>(5);

        [Summary("Log Level")]
        public Setting<LogLevel> LogLevel = new Global<LogLevel>(Microsoft.Extensions.Logging.LogLevel.Error);

        [Summary("Print Level (This is the level of logging to print to chat)")]
        public Setting<LogLevel> PrintLevel = new Global<LogLevel>(Microsoft.Extensions.Logging.LogLevel.Information);
        private CancellationTokenSource tokenSource;
        private ILogger logger;
        private ConcurrentQueue<Action> _mainThreadActionQueue = new ConcurrentQueue<Action>();

        public UBNetClient UBNetClient { get; private set; }

        public UBNet(ILogger logger) {
            this.logger = logger;
        }

        public async void Init(string clientName) {
            try {
                if (StartServer) {
                    TryLaunchServer();
                }

                tokenSource = new CancellationTokenSource();

                UBNetClient = new UBNetClient(clientName, ClientType.GameClient, tokenSource.Token, RunOnMainThread, new UBNetClientOptions(Host, Port), logger);

                var connected = await UBNetClient.Connect();
            }
            catch (Exception ex) { logger.LogError(ex.ToString()); }

            UBNetClient.OnTypedBroadcastRequest += UBNetClient_OnTypedBroadcastRequest;
            UBService.OnTick += UBService_OnTick;
        }

        private void UBService_OnTick(object sender, EventArgs e) {
            try {
                while (_mainThreadActionQueue.TryDequeue(out var action)) {
                    action?.Invoke();
                }
            }
            catch (Exception ex) { logger.LogError(ex.ToString()); }
        }

        private void RunOnMainThread(Action action) {
            _mainThreadActionQueue?.Enqueue(action);
        }

        private async void UBNetClient_OnTypedBroadcastRequest(object sender, TypedBroadcastEventArgs e) {
            if (e.IsType(typeof(UBNetRemoteExecRequest)) && e.TryDeserialize(out UBNetRemoteExecRequest req)) {
                string error = null;
                logger.LogTrace($"Got UBNetRemoteExecRequest: {req.ScriptName} // {req.Function} // {req.Arguments}");
                try {
                    var script = UBService.Scripts.GetScript(req.ScriptName);
                    if (script == null) {
                        logger.LogTrace($"RemoteExec on script {req.ScriptName} is using global context, because the script is not loaded.");
                        script = UBService.Scripts.GlobalScriptContext;
                    }
                    var args = WattleScript.Interpreter.Serialization.Json.JsonTableConverter.JsonToTable(req.Arguments);
                    DynValue result = DynValue.NewTable(script.WattleScript);
                    try {
                        var closure = await script.WattleScript.DoStringAsync($"return {req.Function}");

                        logger.LogTrace($"RemoteExec Closure is {closure.Type} args is {string.Join(",", args.Values.Select(v => v.Type))} ({req.Arguments})");
                        var funcResult = await script.WattleScript.CallAsync(closure, args.Values.ToArray());
                        logger.LogTrace($"RemoteExec result is {funcResult.Type}");
                        switch (funcResult.Type) {
                            case DataType.Table:
                                result = funcResult;
                                break;
                            case DataType.Tuple:
                                result.Table.Append(funcResult.Tuple);
                                break;
                            default:
                                result.Table.Append(funcResult);
                                break;
                        }
                        var res = new UBNetRemoteExecResponse(true, WattleScript.Interpreter.Serialization.Json.JsonTableConverter.TableToJson(result.Table), "");
                        res.SenderId = UBNetClient.Id;
                        UBNetClient.SendBroadcastResponse(res, e.Message);
                    }
                    catch (OperationCanceledException ex) { }
                    catch (ScriptRuntimeException ex) {
                        logger.LogError($"An error occured while running a RemoteExec! {ex.DecoratedMessage} {ex}");
                        error = ex.DecoratedMessage;
                    }
                    catch (SyntaxErrorException ex) {
                        logger.LogError($"A syntax error occured while running a RemoteExec! {ex.DecoratedMessage}");
                        error = ex.DecoratedMessage;
                    }
                    catch (Exception ex) {
                        error = ex.Message;
                        logger.LogError($"Error while running a RemoteExec:\n{ex}");
                    }
                    if (!string.IsNullOrEmpty(error)) {
                        var res = new UBNetRemoteExecResponse(false, "", error);
                        res.SenderId = UBNetClient.Id;
                        UBNetClient.SendBroadcastResponse(res, e.Message);
                    }
                }
                catch (Exception ex) {
                    UBService.LogException(ex);
                    var res = new UBNetRemoteExecResponse(false, "", ex.Message);
                    res.SenderId = UBNetClient.Id;
                    UBNetClient.SendBroadcastResponse(res, e.Message);
                }
            }
        }

        public void SetClientInfo(string clientName, List<string> tags) {
            try {
                UBNetClient.SetName(clientName);
                UBNetClient.AddTags(tags);
            }
            catch (Exception ex) { logger.LogError(ex.ToString()); }
        }

        private void TryLaunchServer() {
            try {
                bool isAnotherInstanceOpen = false;
                using (var mutex = new Mutex(false, "com.UtilityBelt.Server.InstanceCheck")) {
                    isAnotherInstanceOpen = !mutex.WaitOne(TimeSpan.Zero);
                }
                if (!isAnotherInstanceOpen) {
                    Process p = new Process();
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.FileName = Path.Combine(UBService.AssemblyDirectory, "UtilityBelt.Server.exe");
                    p.StartInfo.Arguments = $"--host {Host} --port {Port} --verbosity {LogLevel} --timeout 5";
                    UBService.WriteLog($"Starting UBNet server with args: {p.StartInfo.Arguments}", Microsoft.Extensions.Logging.LogLevel.Debug);
                    p.Start();
                }
            }
            catch (Exception ex) { logger.LogError(ex.ToString()); }
        }


        internal void Shutdown() {
            try {
                UBService.OnTick -= UBService_OnTick;
                UBNetClient.OnTypedBroadcastRequest -= UBNetClient_OnTypedBroadcastRequest;

                tokenSource?.Cancel();
                UBNetClient?.Dispose();
            }
            catch (Exception ex) { logger.LogError(ex.ToString()); }
        }

        public void Dispose() {
            Shutdown();
        }
    }
}
