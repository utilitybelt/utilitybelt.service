﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using AcClient;
using ACE.Entity;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Microsoft.Extensions.Logging;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Service.Lib.ScriptInterface {
    public class ACClientActions : IClientActionsRaw {
        private ILogger _log;

        public ACClientActions(ILogger logger) {
            _log = logger;
        }

        public void AttributeAddExperience(AttributeId attribute, uint experienceToSpend) {
            try {
                CoreManager.Current.Actions.AddAttributeExperience((Decal.Adapter.Wrappers.AttributeType)attribute, (int)experienceToSpend);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SkillAddExperience(SkillId skill, uint experienceToSpend) {
            try {
                CoreManager.Current.Actions.AddSkillExperience((Decal.Adapter.Wrappers.SkillType)skill, (int)experienceToSpend);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VitalAddExperience(VitalId vital, uint experienceToSpend) {
            try {
                CoreManager.Current.Actions.AddVitalExperience((Decal.Adapter.Wrappers.VitalType)(vital + 1), (int)experienceToSpend);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void ApplyWeenie(uint sourceObjectId, uint targetObjectId) {
            try {
                CoreManager.Current.Actions.ApplyItem((int)sourceObjectId, (int)targetObjectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void AutoWield(uint objectId, EquipMask slot) {
            try {
                AcClient.CM_Inventory.Event_GetAndWieldItem(objectId, (uint)slot);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void CastSpell(uint spellId, uint targetId) {
            try {
                CoreManager.Current.Actions.CastSpell((int)spellId, (int)targetId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void Dispose() {

        }

        public void DropObject(uint objectId) {
            try {
                CoreManager.Current.Actions.DropItem((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipCreate(string name, bool shareExperience) {
            try {
                UBHelper.Fellow.Create(name/*, shareExperience */);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipDisband() {
            try {
                UBHelper.Fellow.Disband();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipDismiss(uint objectId) {
            try {
                UBHelper.Fellow.Dismiss((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipSetLeader(uint objectId) {
            try {
                UBHelper.Fellow.Leader = (int)objectId;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipQuit(bool disband) {
            try {
                UBHelper.Fellow.Quit(/* disband */);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipRecruit(uint objectId) {
            try {
                UBHelper.Fellow.Recruit((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void FellowshipSetOpen(bool open) {
            try {
                UBHelper.Fellow.Open = open;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void GiveWeenie(uint objectId, uint targetId) {
            try {
                CoreManager.Current.Actions.GiveItem((int)objectId, (int)targetId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }


        [DllImport("Decal.dll")]
        static extern int DispatchOnChatCommand(ref IntPtr str, [MarshalAs(UnmanagedType.U4)] int target);

        private static bool Decal_DispatchOnChatCommand(string cmd) {
            IntPtr bstr = Marshal.StringToBSTR(cmd);

            try {
                bool eaten = (DispatchOnChatCommand(ref bstr, 1) & 0x1) > 0;

                return eaten;
            }
            finally {
                Marshal.FreeBSTR(bstr);
            }
        }

        public void InvokeChatParser(string text) {
            try {
                if (!Decal_DispatchOnChatCommand(text))
                    CoreManager.Current.Actions.InvokeChatParser(text);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void MoveWeenie(uint objectId, uint containerId, uint slot, bool stack) {
            try {
                CoreManager.Current.Actions.MoveItem((int)objectId, (int)containerId, (int)slot, stack);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }


        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        private delegate bool RequestIdDelegate(int objectId);
        private RequestIdDelegate _requestIdInternal;

        public void Appraise(uint objectId) {
            try {
                if (_requestIdInternal == null) {
                    _requestIdInternal = (RequestIdDelegate)Marshal.GetDelegateForFunctionPointer((IntPtr)(436554 << 4), typeof(RequestIdDelegate));
                }

                _requestIdInternal((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SalvagePanelAdd(uint objectId) {
            try {
                CoreManager.Current.Actions.SalvagePanelAdd((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SalvagePanelSalvage() {
            try {
                CoreManager.Current.Actions.SalvagePanelSalvage();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SelectWeenie(uint objectId) {
            try {
                CoreManager.Current.Actions.SelectItem((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SetAutorun(bool enabled) {
            try {
                CoreManager.Current.Actions.SetAutorun(enabled);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SetCombatMode(CombatMode combatMode) {
            try {
                CoreManager.Current.Actions.SetCombatMode((Decal.Adapter.Wrappers.CombatState)combatMode);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void TradeAccept() {
            try {
                CoreManager.Current.Actions.TradeAccept();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void TradeAdd(uint objectId) {
            try {
                CoreManager.Current.Actions.TradeAdd((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void TradeDecline() {
            try {
                CoreManager.Current.Actions.TradeDecline();
            }
            catch (Exception ex) {
                _log.LogError(ex.ToString());
            }
        }

        public void TradeEnd() {
            try {
                CoreManager.Current.Actions.TradeEnd();
            }
            catch (Exception ex) {
                _log.LogError(ex.ToString());
            }
        }

        public void TradeReset() {
            try {
                CoreManager.Current.Actions.TradeReset();
            }
            catch (Exception ex) {
                _log.LogError(ex.ToString());
            }
        }

        public void UseWeenie(uint objectId) {
            try {
                CoreManager.Current.Actions.UseItem((int)objectId, 0);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public uint SelectedObject() {
            try {
                return (uint)CoreManager.Current.Actions.CurrentSelection;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
            return 0;
        }

        unsafe public void Login(uint id) {
            try {
                AcClient.CPlayerSystem.GetPlayerSystem()->LogOnCharacter(id);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        unsafe public void Logout() {
            try {
                AcClient.CPlayerSystem.GetPlayerSystem()->LogOffCharacter(0);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SkillAdvance(SkillId skill, uint creditsToSpend) {
            try {
                AcClient.CM_Train.Event_TrainSkillAdvancementClass((uint)skill, creditsToSpend);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void AllegianceBreak(uint objectId) {
            try {
                AcClient.CM_Allegiance.Event_BreakAllegiance(objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void AllegianceSwear(uint objectId) {
            try {
                AcClient.CM_Allegiance.Event_SwearAllegiance(objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorAddToBuyList(uint objectId, uint amount = 1) {
            try {
                CoreManager.Current.Actions.VendorAddBuyList((int)objectId, (int)amount);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorAddToSellList(uint objectId) {
            try {
                CoreManager.Current.Actions.VendorAddSellList((int)objectId);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorBuyAll() {
            try {
                CoreManager.Current.Actions.VendorBuyAll();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorSellAll() {
            try {
                CoreManager.Current.Actions.VendorSellAll();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorClearBuyList() {
            try {
                CoreManager.Current.Actions.VendorClearBuyList();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void VendorClearSellList() {
            try {
                CoreManager.Current.Actions.VendorClearSellList();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SplitObject(uint objectId, uint targetId, uint newStackSize, uint slot = 0) {
            try {
                var weenie = new UBHelper.Weenie((int)objectId);
                weenie.Split((int)targetId, (int)slot, (int)newStackSize);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void SendTellByObjectId(uint objectId, string message) {
            try {
                UBHelper.Core.SendTellByGUID((int)objectId, message);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public void AcceptNextConfirmationRequest() {
            try {
                UBHelper.ConfirmationRequest.ConfirmationRequestEvent += ConfirmationRequest_ConfirmationRequestEvent;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        private void ConfirmationRequest_ConfirmationRequestEvent(object sender, UBHelper.ConfirmationRequest.ConfirmationRequestEventArgs e) {
            UBHelper.ConfirmationRequest.ConfirmationRequestEvent -= ConfirmationRequest_ConfirmationRequestEvent;
            e.ClickYes = true;
        }

        public void CastEquippedWandSpell(uint targetId = 0) {
            try {
                var wos = CoreManager.Current.WorldFilter.GetByOwner(CoreManager.Current.CharacterFilter.Id);

                var eq = wos.FirstOrDefault((wo) => (EquipMask)wo.Values(LongValueKey.EquippedSlots) == EquipMask.Wand);
                if (eq != null && eq.Values(LongValueKey.AssociatedSpell) != 0) {

                    if (targetId != 0) {
                        var old = CoreManager.Current.Actions.CurrentSelection;
                        CoreManager.Current.Actions.CurrentSelection = (int)targetId;
                        CoreManager.Current.Actions.UseItem(eq.Id, 1, 1);
                        CoreManager.Current.Actions.CurrentSelection = old;
                    }
                    else {
                        CoreManager.Current.Actions.UseItem(eq.Id, 1, 1);
                    }
                }

                wos.Dispose();
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
        }

        public unsafe void InscribeWeenie(uint objectId, string inscription) {
            try {
                if (string.IsNullOrEmpty(inscription))
                    inscription = "";

                AC1Legacy.PStringBase<char> newInscription = $"{inscription}\0";
                CM_Writing.Event_SetInscription(objectId, &newInscription);
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
}

        public int GetVitalBase(uint objectId, VitalId vital) {
            try {
                if ((int)objectId != CoreManager.Current.CharacterFilter?.Id) {
                    return 0;
                }

                return CoreManager.Current.CharacterFilter.Vitals[(CharFilterVitalType)(vital + 1)].Base;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
            return 0;
        }

        public int GetVitalCurrent(uint objectId, VitalId vital) {
            try {
                if ((int)objectId != CoreManager.Current.CharacterFilter?.Id) {
                    return 0;
                }

                return CoreManager.Current.CharacterFilter.Vitals[(CharFilterVitalType)(vital + 1)].Current;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
            return 0;
        }

        public int GetVitalMax(uint objectId, VitalId vital) {
            try {
                if ((int)objectId != CoreManager.Current.CharacterFilter?.Id) {
                    return 0;
                }

                var v = CoreManager.Current.CharacterFilter.Vitals[(CharFilterVitalType)(vital + 1)];
                CoreManager.Current.Actions.AddChatText($"Get Vital {vital}: {v.Name} // {v.Current}/{v.Buffed} ({v.Current*0.95f}/{v.Buffed*0.95f})", 1);

                return CoreManager.Current.CharacterFilter.Vitals[(CharFilterVitalType)(vital + 1)].Buffed;
            }
            catch (Exception ex) { _log.LogError(ex.ToString()); }
            return 0;
        }
    }
}