﻿using Hellosam.Net.Collections;
using WattleScript.Interpreter;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service.Lib.Settings;
using UtilityBelt.Service.Views.Inspector;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using Microsoft.Extensions.Logging;
using System.IO;
using Microsoft.Extensions.Options;
using System.Xml.Linq;

namespace UtilityBelt.Service.Lib {
    public class ScriptSettings : ISetting {
        public enum ScriptRunType {
            None,
            Global,
            Account,
            Character
        }

        public class ScriptFileSystemAccessData {
            public string RequestedPath { get; set; } = "NO_ACCESS";
            public string ProvidedPath { get; set; } = "NO_ACCSES";
            public bool Allowed { get; set; } = false;
            public string Reason { get; set; } = "No reason given.";
            public ScriptFileSystemAccessData() { }
        }

        [Summary("Lua script directory. (If you change this you must relog for it to take effect.)")]
        public Global<string> ScriptDirectory = new Global<string>(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Decal Plugins", "UtilityBelt", "scripts"));

        [Summary("Lua script data directory. (If you change this you must relog for it to take effect.)")]
        public Global<string> ScriptDataDirectory = new Global<string>(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "Decal Plugins", "UtilityBelt", "scriptdata"));

        [Summary("Enable lua script interface. (If you change this you must relog for it to take effect.)")]
        public Global<bool> Enable = new Global<bool>(true);

        [Summary("Enable lua script api inspector")]
        public Global<bool> EnableAPIInspector = new Global<bool>(false);

        [Summary("Reload scripts on file changes")]
        public Global<bool> ReloadScriptsOnFileChanges = new Global<bool>(false);

        [Summary("List of scripts to auto load globally. Does not take effect until client is restarted.")]
        public readonly Global<ObservableCollection<string>> GlobalScripts = new Global<ObservableCollection<string>>(new ObservableCollection<string>());

        [Summary("List of scripts to auto load on this account. Does not take effect until client is restarted.")]
        public readonly Account<ObservableCollection<string>> AccountScripts = new Account<ObservableCollection<string>>(new ObservableCollection<string>());

        [Summary("List of scripts to auto load on this character. Does not take effect until client is restarted.")]
        public readonly CharacterSetting<ObservableCollection<string>> CharacterScripts = new CharacterSetting<ObservableCollection<string>>(new ObservableCollection<string>());

        [Summary("Allowed FileSystem access for scripts")]
        public readonly Global<ObservableDictionary<string, ObservableCollection<ScriptFileSystemAccessData>>> ScriptFileSystemAccess = new Global<ObservableDictionary<string, ObservableCollection<ScriptFileSystemAccessData>>>(new ObservableDictionary<string, ObservableCollection<ScriptFileSystemAccessData>>());

        [Summary("Log Level")]
        public Setting<LogLevel> LogLevel = new Global<LogLevel>(Microsoft.Extensions.Logging.LogLevel.Warning);

        [Summary("Print Level (This is the level of logging to print to chat)")]
        public Setting<LogLevel> PrintLevel = new Global<LogLevel>(Microsoft.Extensions.Logging.LogLevel.Information);

        private Dictionary<string, ScriptRunType> _manualRunTypes = new Dictionary<string, ScriptRunType>();
        private Inspector _inspector;

        public void Init() {
            UBService.Scripts.OnScriptStarted += Scripts_OnScriptStarted;
            UBService.Scripts.OnScriptStopped += Scripts_OnScriptStopped;

            EnableAPIInspector.Changed += EnableAPIInspector_Changed;
            TryCreateInspector();


            UBHelper.ConfirmationRequest.ConfirmationRequestEvent += ConfirmationRequest_ConfirmationRequestEvent;
        }

        private void ConfirmationRequest_ConfirmationRequestEvent(object sender, UBHelper.ConfirmationRequest.ConfirmationRequestEventArgs e) {
            try {
                var eventArgs = new ConfirmationRequestEventArgs((ClientConfirmationType)e.Confirm, e.Text);
                UBService.WriteLog($"ConfirmationRequest_ConfirmationRequestEvent: {e.Confirm} // {e.Text}", Microsoft.Extensions.Logging.LogLevel.Debug);
                UBService.Scripts?.GameState?.WorldState?.HandleConfirmationPopup(eventArgs);
                e.ClickYes = eventArgs.ClickYes;
                e.ClickNo = eventArgs.ClickNo;
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }

        private void EnableAPIInspector_Changed(object sender, SettingChangedEventArgs e) {
            TryCreateInspector();
        }

        internal void TryCreateInspector() {
            if (EnableAPIInspector && _inspector == null && UBService.Scripts?.GlobalScriptContext?.Context?.Globals["game"].UserData?.Object != null) {
                _inspector = new Inspector("UBScripting API Inspector", UBService.Scripts.GlobalScriptContext.Context.Globals["game"].UserData.Object);
                _inspector.Hud.Visible = false;
            }
            else if (!EnableAPIInspector && _inspector != null) {
                _inspector.Dispose();
                _inspector = null;
            }
        }

        internal ScriptFileSystemAccessData GetScriptFileSystemAccess(string scriptName, string requestedPath) {
            if (ScriptFileSystemAccess.Value.ContainsKey(scriptName)) {
                var res = ScriptFileSystemAccess.Value[scriptName].Where(s => s.RequestedPath == requestedPath).FirstOrDefault();
                if (res != null) {
                    return res;
                }
            }
            return null;
        }

        internal void SetScriptFileSystemAccess(string scriptName, string requestedPath, string providedPath, string reason, bool allowed) {
            if (!ScriptFileSystemAccess.Value.ContainsKey(scriptName)) {
                ScriptFileSystemAccess.Value.Add(scriptName, new ObservableCollection<ScriptFileSystemAccessData>());
            }

            RemoveScriptFileSystemAccess(scriptName, requestedPath);
            ScriptFileSystemAccess.Value[scriptName].Add(new ScriptFileSystemAccessData() {
                RequestedPath = requestedPath,
                ProvidedPath = providedPath,
                Reason = reason,
                Allowed = allowed
            });
            ScriptFileSystemAccess.InvokeChange();
        }

        internal void RemoveScriptFileSystemAccess(string scriptName, string requestedPath) {
            if (ScriptFileSystemAccess.Value.ContainsKey(scriptName)) {
                var res = ScriptFileSystemAccess.Value[scriptName].Where(s => s.RequestedPath == requestedPath).FirstOrDefault();
                if (res != null) {
                    ScriptFileSystemAccess.Value[scriptName].Remove(res);
                }
            }
            ScriptFileSystemAccess.InvokeChange();
        }

        private void Scripts_OnScriptStopped(object sender, Scripting.Events.ScriptEventArgs e) {
            if (e.Script != null)
                _manualRunTypes.Remove(e.Script.Name);
        }

        private void Scripts_OnScriptStarted(object sender, Scripting.Events.ScriptEventArgs e) {
            if (GetScriptRunType(e.Script.Name) == ScriptRunType.None) {
                _manualRunTypes.AddOrUpdate(e.Script.Name, UBService.IsInGame ? ScriptRunType.Character : ScriptRunType.Account);
            }
        }

        public static ScriptRunType GetScriptRunType(string scriptName) {
            if (UBService.ScriptSettings.GlobalScripts.Value.Contains(scriptName)) {
                return ScriptRunType.Global;
            }
            if (UBService.ScriptSettings.AccountScripts.Value.Contains(scriptName)) {
                return ScriptRunType.Account;
            }
            if (UBService.ScriptSettings.CharacterScripts.Value.Contains(scriptName)) {
                return ScriptRunType.Character;
            }

            return ScriptRunType.None;
        }

        public static void StartScripts(ScriptRunType runType) {
            var scripts = new List<string>();
            switch (runType) {
                case ScriptRunType.Global:
                    scripts.AddRange(UBService.ScriptSettings.GlobalScripts.Value);
                    break;
                case ScriptRunType.Account:
                    scripts.AddRange(UBService.ScriptSettings.AccountScripts.Value);
                    break;
                case ScriptRunType.Character:
                    scripts.AddRange(UBService.ScriptSettings.CharacterScripts.Value);
                    break;
            }

            var distinctScripts = scripts.Distinct();
            foreach (var script in distinctScripts) {
                try {
                    string directory = Path.Combine(UBService.Scripts.Options.ScriptDirectory, script);
                    if (!Directory.Exists(directory)) continue;
                    UBService.Scripts.StartScript(script);
                }
                catch (Exception ex) { UBService.LogException(ex); }
            }
        }

        public static void StopScripts(ScriptRunType runType) {
            var scripts = new List<string>();
            switch (runType) {
                case ScriptRunType.Global:
                    scripts.AddRange(UBService.ScriptSettings.GlobalScripts.Value);
                    break;
                case ScriptRunType.Account:
                    scripts.AddRange(UBService.ScriptSettings.AccountScripts.Value);
                    scripts.AddRange(UBService.ScriptSettings._manualRunTypes.Where(kv => {
                        return GetScriptRunType(kv.Key) == ScriptRunType.None && kv.Value == ScriptRunType.Account;
                    }).Select(kv => kv.Key));
                    break;
                case ScriptRunType.Character:
                    scripts.AddRange(UBService.ScriptSettings.CharacterScripts.Value);
                    scripts.AddRange(UBService.ScriptSettings._manualRunTypes.Where(kv => {
                        return GetScriptRunType(kv.Key) == ScriptRunType.None && kv.Value == ScriptRunType.Character;
                    }).Select(kv => kv.Key));
                    break;
            }

            var distinctScripts = scripts.Distinct();

            foreach (var script in distinctScripts) {
                try {
                    UBService.Scripts.StopScript(script);
                }
                catch (Exception ex) { UBService.LogException(ex); }
            }
        }
    }
}
