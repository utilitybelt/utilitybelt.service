﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Service.Lib.Settings {
    public class Global<T> : Setting<T> {
        public Global(T initialValue) : base(initialValue) {
            SettingType = SettingType.Global;
        }
    }
}
