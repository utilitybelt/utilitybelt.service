﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Service.Lib.Settings {
    public class Account<T> : Setting<T> {
        public Account(T initialValue) : base(initialValue) {
            SettingType = SettingType.Account;
        }
    }
}
