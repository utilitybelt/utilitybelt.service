﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilityBelt.Service.Lib.Settings {
    public class ViewsProfileSetting<T> : Setting<T> {
        public ViewsProfileSetting(T initialValue) : base(initialValue) {
            SettingType = SettingType.Views; 
        }
    }
}
