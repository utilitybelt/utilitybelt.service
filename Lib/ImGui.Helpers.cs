﻿using ImGuiNET;
using System;
using System.Numerics;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using UtilityBelt.Service.Views;

namespace ImGuiNET {
    public static unsafe partial class ImGui {
        private static Vector2 v2_0 = new Vector2(0, 0);
        private static Vector2 v2_1 = new Vector2(1, 1);
        private static Vector4 v4_color_clear = new Vector4(0, 0, 0, 0);

        public static bool TextureButton(string id, ManagedTexture texture, Vector2 size, int framePadding = 1) {
            return TextureButton(id, texture, size, framePadding, v4_color_clear);
        }

        public static bool ImageButton(IntPtr user_texture_id, Vector2 size, Vector2 uv0, Vector2 uv1, int framePadding, Vector4 bg_col, Vector4 tint_col) {
            ImGui.PushStyleVar(ImGuiStyleVar.FramePadding, new Vector2(framePadding, framePadding));
            var ret = ImGui.ImageButton(((int)user_texture_id).ToString(), user_texture_id, size, uv0, uv1, bg_col, tint_col);
            ImGui.PopStyleVar();
            return ret;
        }

        public static bool TextureButton(string id, ManagedTexture texture, Vector2 size, int framePadding, Vector4 bgColor) {
            ImGui.PushID(id);
            ImGui.PushStyleColor(ImGuiCol.Button, bgColor); // button bg
            var ret = ImGui.ImageButton(texture.TexturePtr, size, v2_0, v2_1, framePadding, bgColor, *ImGui.GetStyleColorVec4(ImGuiCol.Text));
            ImGui.PopStyleColor(); // button bg
            ImGui.PopID(); // id

            return ret;
        }

        public static bool TextureButton(string id, ManagedTexture texture, Vector2 size, int framePadding, Vector4 bgColor, Vector4 tint) {
            ImGui.PushID(id);
            ImGui.PushStyleColor(ImGuiCol.Button, bgColor); // button bg
            var ret = ImGui.ImageButton(texture.TexturePtr, size, v2_0, v2_1, framePadding, bgColor, tint);
            ImGui.PopStyleColor(); // button bg
            ImGui.PopID(); // id

            return ret;
        }

        public static Vector4 ColToVec4(uint col) {
            var b = (float)((col) & 0xFF) / 255.0f;
            var g = (float)((col >> 8) & 0xFF) / 255.0f;
            var r = (float)((col >> 16) & 0xFF) / 255.0f;
            var a = (float)((col >> 24) & 0xFF) / 255.0f;

            return new Vector4(b, g, r, a);
        }

        public static uint Vec4ToCol(Vector4 v4) {
            return ((uint)(v4.X * 255.0f)) |
            ((uint)(v4.Y * 255.0f) << 8) |
            ((uint)(v4.Z * 255.0f) << 16) |
            ((uint)(v4.W * 255.0f) << 24);
        }
    }
}