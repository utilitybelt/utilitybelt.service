﻿using AcClient;
using ACE.Entity;
using Microsoft.DirectX.Direct3D;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Policy;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityBelt.Common.Messages.Types;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service.Lib.ACClientModule;
using WattleScript.Interpreter;

namespace UtilityBelt.Service.Lib {
    public class HttpResponseData {
        public int StatusCode;
        public string Body;
        public Dictionary<string, string> Headers;
    }

    /// <summary>
    /// Cancel any current Melee / missile attack
    /// </summary>
    public class HttpRequestAction : QueueAction {
        private static HttpClient _httpClient = null;
        private Task<HttpResponseMessage> _httpTask;
        private Task<string> _readTask;
        private CancellationTokenSource _cancelationToken;

        public override ActionType ActionType => ActionType.Immediate;

        public string Url { get; }
        public string Body { get; }
        public Dictionary<string, string> Headers { get; }
        public HttpResponseData Response { get; private set; }

        public HttpRequestAction(string url, string body = null, Dictionary<string, string> headers = null, ActionOptions options = null) : base(options) {
            Url = url;
            Body = body;
            Headers = headers;
        }

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 30000;
            if (!Options.MaxRetryCount.HasValue)
                Options.MaxRetryCount = 1;
        }

        protected override void UpdatePreconditions() {

        }

        public override bool IsValid() {
            return true;
        }

        protected override void Start() {
            if (_httpClient == null) {
                _httpClient = new HttpClient();
                _httpClient.Timeout = TimeSpan.FromMilliseconds(Options.TimeoutMilliseconds.Value);
            }

            _cancelationToken = new CancellationTokenSource();

            if (Body == null) {
                var req = new HttpRequestMessage(HttpMethod.Get, Url);
                if (Headers != null) {
                    foreach (var header in Headers) {
                        req.Headers.Add(header.Key, header.Value);
                    }
                }
                _httpTask = _httpClient.SendAsync(req, _cancelationToken.Token);
            }
            else {
                var req = new HttpRequestMessage(HttpMethod.Post, Url);
                if (Headers != null) {
                    foreach (var header in Headers) {
                        req.Headers.Add(header.Key, header.Value);
                    }
                }
                req.Content = new StringContent(Body, Encoding.UTF8, "application/json");
                _httpTask = _httpClient.SendAsync(req, _cancelationToken.Token);
            }
        }

        protected override void Stop() {
            _cancelationToken?.Cancel();
        }

        unsafe protected override bool Execute() {
            if (_httpTask.IsCompleted) {
                if (_readTask != null) {
                    if (_readTask.IsCompleted) {
                        Response.Body = _readTask.Result;
                        SetPermanentResult(ActionError.None);
                        return true;
                    }
                }
                else {
                    var response = _httpTask.Result;
                    if (response != null) {
                        Response = new HttpResponseData();
                        Response.StatusCode = (int)response.StatusCode;
                        Response.Headers = new Dictionary<string, string>();
                        foreach (var header in response.Headers) {
                            Response.Headers.Add(header.Key, header.Value.FirstOrDefault());
                        }
                        _readTask = response.Content.ReadAsStringAsync();
                    }
                    else {
                        SetPermanentResult(ActionError.Exception, "Request failed.");
                        return true;
                    }
                }
            }
            return false;
        }

        public string Test() {
            return Url;
        }

        public override string ToString() {
            return $"HttpRequestAction: {Url}[{Response?.StatusCode.ToString() ?? "null"}]";
        }
    }

    public class SocketHttpModule {
        private static HttpClient _httpClient = null;

        public SocketHttpModule() {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        }

        /// <summary>
        /// If the first argument of the request function is a string, it should be an url. In that case, if a
        /// body is provided as a string, the function will perform a POST method in the url. Otherwise, it performs
        /// a GET in the url.
        /// In case of failure, the function returns nil followed by an error message. If successful, returns a lua table
        /// with like following: { body: "", status: 200, headers: {} }
        /// </summary>
        /// <param name="url">The url to request.</param>
        /// <param name="body">Optional JSON body to post.</param>
        public HttpRequestAction Request(string url, string body = null, Dictionary<string, string> headers = null, ActionOptions options = null, Action<HttpRequestAction> callback = null) {
            return UBService.Scripts.GameState.Actions.MakeAndEnqueuePromise(new HttpRequestAction(url, body, headers, options), callback);
        }

        public async Task<HttpResponseData> InternalRequest(string url, string body) {
            HttpResponseData responseData = new();
            if (_httpClient == null) {
                _httpClient = new HttpClient();
                _httpClient.Timeout = TimeSpan.FromSeconds(30);
            }

            HttpResponseMessage response = null;

            try {
                if (body == null) {
                    response = await _httpClient.GetAsync(url);
                }
                else {
                    response = await _httpClient.PostAsync(url, new StringContent(body, Encoding.UTF8, "application/json"));
                }
            }
            catch (Exception ex) {
                UBService.LogException(ex);
            }
            if (response != null) {
                responseData.StatusCode = (int)response.StatusCode;
                responseData.Headers = new Dictionary<string, string>();
                foreach (var header in response.Headers) {
                    responseData.Headers.Add(header.Key, header.Value.FirstOrDefault());
                }
                responseData.Body = await response.Content.ReadAsStringAsync();
            }

            return responseData;
        }
    }
}
