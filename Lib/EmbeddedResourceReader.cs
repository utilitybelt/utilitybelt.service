﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Service.Lib {
    internal static class EmbeddedResourceReader {
        internal static byte[]? GetEmbeddedResourceBytes(string filename) {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "UtilityBelt.Service.Resources." + filename;

            using var stream = assembly.GetManifestResourceStream(resourceName);

            if (stream is not null) {
                var buffer = new byte[stream.Length];
                stream.Read(buffer, 0, (int)stream.Length);
                return buffer;
            }

            return null;
        }

        internal static Stream? GetEmbeddedResourceStream(string filename) {
            var assembly = Assembly.GetExecutingAssembly();
            var resourceName = "UtilityBelt.Service.Resources." + filename;

            return assembly.GetManifestResourceStream(resourceName);
        }
    }
}
