﻿using AcClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Service.Lib.ACClientModule {
    /// <summary>
    /// Combat stuff. This is an experimental api and may change / move in the future.
    /// </summary>
    public class ACCombat {
        /// <summary>
        /// Returns true if the client is set to automatically repeat attacks.
        /// </summary>
        unsafe public bool RepeatAttacks {
            get {
                if (UBService.IsInGame && CPlayerSystem.GetPlayerSystem() != null) {
                    return CPlayerSystem.GetPlayerSystem()->playerModule.PlayerModule.GetOption(PlayerOption.AutoRepeatAttack_PlayerOption) == 1;
                }
                return false;
            }
            set {
                if (UBService.IsInGame && CPlayerSystem.GetPlayerSystem() != null) {
                    CPlayerSystem.GetPlayerSystem()->playerModule.PlayerModule.SetOption(PlayerOption.AutoRepeatAttack_PlayerOption, (byte)(value ? 1 : 0));
                }
            }
        }

        /// <summary>
        /// Wether or not you are currrently attacking something with melee or missile attacks
        /// </summary>
        unsafe public bool AttackInProgress {
            get {
                if (UBService.IsInGame && ClientCombatSystem.GetCombatSystem() != null) {
                    return ClientCombatSystem.GetCombatSystem()->attackInProgress == 1;
                }
                return false;
            }
        }

        /// <summary>
        /// Your current attack power / accuracy for melee / missile attacks. You can set this value to change the power / accuracy future attacks use.
        /// 0 is minimum, 1 is maximum.
        /// </summary>
        unsafe public float AttackPower {
            get {
                if (UBService.IsInGame && ClientCombatSystem.GetCombatSystem() != null) {
                    return ClientCombatSystem.GetCombatSystem()->m_rUIRequestedPower;
                }
                return 1f;
            }
            set {
                if (UBService.IsInGame && ClientCombatSystem.GetCombatSystem() != null) {
                    var newValue = Math.Min(1, Math.Max(0, value));
                    ClientCombatSystem.GetCombatSystem()->m_rUIRequestedPower = newValue;
                    CM_Combat.SendNotice_DesiredAttackPowerChanged(newValue);
                }
            }
        }

        /// <summary>
        /// Your current attack height. You can set this value to change the height future attacks use.
        /// </summary>
        unsafe public AttackHeight AttackHeight {
            get {
                if (UBService.IsInGame && ClientCombatSystem.GetCombatSystem() != null) {
                    return (AttackHeight)ClientCombatSystem.GetCombatSystem()->requestedAttackHeight;
                }
                return AttackHeight.Medium;
            }
            set {
                if (UBService.IsInGame && ClientCombatSystem.GetCombatSystem() != null) {
                    ClientCombatSystem.GetCombatSystem()->SetRequestedAttackHeight((ATTACK_HEIGHT)value);
                    CM_Combat.SendNotice_AttackHeightChanged((ATTACK_HEIGHT)value);
                }
            }
        } 

        public ACCombat() {
            //ClientCombatSystem.GetCombatSystem()->
        }

        /// <summary>
        /// Check if the specified object is attackable.
        /// </summary>
        /// <param name="objectId">The id of the object to check</param>
        /// <returns>True if the object is something you can attack</returns>
        unsafe public bool IsAttackable(uint objectId) {
            if (UBService.IsInGame && ClientCombatSystem.GetCombatSystem() != null && UBService.Scripts.GameState.WorldState.WeenieExists(objectId)) {
                return ClientCombatSystem.GetCombatSystem()->ObjectIsAttackable(objectId) == 1;
            }
            return false;
        }

        /// <summary>
        /// Cancel your current attack.
        /// </summary>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ACCancelAttackAction CancelAttack(ActionOptions options = null, Action<ACCancelAttackAction> callback = null) {
            return UBService.Scripts.GameState.Actions.MakeAndEnqueuePromise(new ACCancelAttackAction(options), callback);
        }

        /// <summary>
        /// Turn your character to the specified heading, in degrees.
        /// </summary>
        /// <param name="objectId">The id of the object to attack</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ACAttackAction Attack(uint objectId, ActionOptions options = null, Action<ACAttackAction> callback = null) {
            return UBService.Scripts.GameState.Actions.MakeAndEnqueuePromise(new ACAttackAction(objectId, options), callback);
        }
    }
}
