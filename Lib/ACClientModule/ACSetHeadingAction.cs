﻿using Decal.Adapter;
using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Service.Lib.ACClientModule {

    /// <summary>
    /// Set your characters heading to the specified degrees, within the specified accuracy in degrees.
    /// </summary>
    public class ACSetHeadingAction : QueueAction {
        /// <summary>
        /// The degrees to set your heading to
        /// </summary>
        public double Degrees { get; }

        /// <summary>
        /// The accuracy in degrees to allow, when detecting finished.
        /// </summary>
        public double Accuracy { get; }

        private double lastDiff = 0;
        private DateTime lastDiffCheck = DateTime.UtcNow;

        public ACSetHeadingAction(double degrees, double accuracy = 2, ActionOptions options = null) : base(options) {
            Degrees = degrees % 360;
            Accuracy = accuracy;
        }

        public override ActionType ActionType => ActionType.Navigation;

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 3000;
            if (!Options.MaxRetryCount.HasValue)
                Options.MaxRetryCount = 15;
        }

        protected override void UpdatePreconditions() {

        }

        public override bool IsValid() {
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }
            return true;
        }

        protected override void Start() {
            CoreManager.Current.RenderFrame += Current_RenderFrame;
        }

        protected override void Stop() {
            CoreManager.Current.RenderFrame -= Current_RenderFrame;
        }

        protected override bool Execute() {
            if (UBService.IsInGame) {
                lastDiff = Math.Abs(Degrees - CoreManager.Current.Actions.Heading);
                lastDiffCheck = DateTime.UtcNow;
                CoreManager.Current.Actions.Heading = Degrees;
                return true;
            }
            SetPermanentResult(ActionError.NotLoggedIn);
            return false;
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                // are we there?
                if (Math.Abs(Degrees - CoreManager.Current.Actions.Heading) <= Accuracy) {
                    SetPermanentResult(ActionError.None);
                    return;
                }

                // are we not making progress?
                var currentDiff = Math.Abs(Degrees - CoreManager.Current.Actions.Heading);
                if (DateTime.UtcNow - lastDiffCheck > TimeSpan.FromMilliseconds(200) && (currentDiff > lastDiff || Math.Abs(currentDiff - lastDiff) < 2)) {
                    lastDiffCheck = DateTime.UtcNow;
                    Execute();
                }

                lastDiff = currentDiff;
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }
    }
}
