﻿
using AcClient;
using ACE.DatLoader.FileTypes;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Decal.Interop.Core;
using Decal.Interop.D3DService;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Media;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using static UtilityBelt.Service.Lib.ACClientModule.ACMovement;
using Vector3 = System.Numerics.Vector3;

namespace UtilityBelt.Service.Lib.ACClientModule {

    internal static class ACClientModuleInternal {
        private static bool hasHooks = false;

        internal static bool HasWantedMovements => WantedMotionStatus.Values.ToList().Any((c) => c);

        internal static Dictionary<ACMotion, bool> WantedMotionStatus = new Dictionary<ACMotion, bool> { { ACMotion.Forward, false }, { ACMotion.Backward, false }, { ACMotion.TurnRight, false }, { ACMotion.TurnLeft, false }, { ACMotion.StrafeRight, false }, { ACMotion.StrafeLeft, false }, { ACMotion.Walk, false } };

        internal static void Init() {
            TryMakeHooks();
        }

        unsafe private static void TryMakeHooks() {
            if (!hasHooks) {
                hasHooks = true;

                if (!ACCmdInterp_SetMotion_Hook.Setup(new ACCmdInterp_SetMotion_Def(ACCmdInterp_SetMotion))) {
                    UBService.WriteLog($"ACClientModuleInternal > HOOK > ACCmdInterp_SetMotion install falure", LogLevel.Error);
                    return;
                }
                
                UBService.OnTick += UBService_OnTick;
            }
        }


        private static void UBService_OnTick(object sender, EventArgs e) {
            try {
                if (HasWantedMovements)
                    EnsureMovement();
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }

        #region internal motion stuff
        unsafe internal static void SetForcedPosition(Coordinates coordinates) {
            if (UBService.Scripts.GameState.State != ClientState.In_Game || Coordinates.Me.DistanceToFlat(coordinates) > 3)
                return;
            try {
                CPhysicsObj* phy = *CPhysicsObj.player_object;
                if (phy == null)
                    return;
                var velocity = new AC1Legacy.Vector3() {
                    a0 = new AcClient.Vector3() {
                        x = 0,
                        y = 0,
                        z = 0
                    }
                };
                var position = new AcClient.Position() {
                    frame = new AcClient.Frame() {
                        m_fOrigin = new AcClient.Vector3() {
                            x = coordinates.LocalX,
                            y = coordinates.LocalY,
                            z = coordinates.LocalZ
                        },
                        qw = phy->m_position.frame.qw,
                        qx = phy->m_position.frame.qx,
                        qy = phy->m_position.frame.qy,
                        qz = phy->m_position.frame.qz
                    },
                    objcell_id = coordinates.LandCell
                };
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }

        unsafe internal static void SetForcedHeading(double targetDegrees) {
            if (UBService.Scripts.GameState.State != ClientState.In_Game)
                return;
            try {
                CPhysicsObj* phy = *CPhysicsObj.player_object;
                if (phy == null)
                    return;
                phy->set_heading((float)targetDegrees, 1);

            }
            catch (Exception ex) { UBService.LogException(ex); }
        }

        internal static unsafe void SetMotion(ACMotion motion, bool fOn) {
            if (UBService.Scripts.GameState.State != ClientState.In_Game)
                return;

            WantedMotionStatus[motion] = fOn;
            CPhysicsObj* phy = *CPhysicsObj.player_object;
            CMotionInterp* cmi = phy->movement_manager->motion_interpreter;
            UBService.WriteLog($"SetMotion: {motion} // {fOn}", LogLevel.Debug);
            if (GetMotion(motion) != fOn) {
                if (motion == ACMotion.Walk) {
                    if (cmi->raw_state.current_holdkey != (fOn ? HoldKey.HoldKey_None : HoldKey.HoldKey_Run)) {
                        cmi->raw_state.current_holdkey = fOn ? HoldKey.HoldKey_None : HoldKey.HoldKey_Run;
                    }
                    foreach (var kv in WantedMotionStatus) {
                        if (kv.Value) {
                            ((ACCmdInterp*)(*SmartBox.smartbox)->cmdinterp)->SetMotion((uint)kv.Key, false);
                            ((ACCmdInterp*)(*SmartBox.smartbox)->cmdinterp)->SetMotion((uint)kv.Key, true);
                        }
                    }
                }
                else {
                    ((ACCmdInterp*)(*SmartBox.smartbox)->cmdinterp)->SetMotion((uint)motion, fOn);
                }
            }
        }

        internal static unsafe bool GetMotion(ACMotion motion) {
            if (UBService.Scripts.GameState.State != ClientState.In_Game)
                return false;

            CPhysicsObj* phy = *CPhysicsObj.player_object;
            CMotionInterp* cmi = phy->movement_manager->motion_interpreter;

            switch (motion) {
                case ACMotion.Walk:
                    return cmi->raw_state.current_holdkey == HoldKey.HoldKey_None;
                case ACMotion.Forward:
                    return (cmi->raw_state.forward_command == 44000007 || cmi->raw_state.forward_command == (uint)ACMotion.Forward) && cmi->raw_state.forward_speed > 0;
                case ACMotion.Backward:
                    return (cmi->raw_state.forward_command == 44000007 || cmi->raw_state.forward_command == (uint)ACMotion.Backward) && cmi->raw_state.forward_speed < 0;
                case ACMotion.TurnLeft:
                    return cmi->raw_state.turn_command == (uint)ACMotion.TurnRight && cmi->raw_state.turn_speed < 0;
                case ACMotion.TurnRight:
                    return cmi->raw_state.turn_command == (uint)ACMotion.TurnRight && cmi->raw_state.turn_speed > 0;
                case ACMotion.StrafeLeft:
                    return cmi->raw_state.sidestep_command == (uint)ACMotion.StrafeRight && cmi->raw_state.sidestep_speed < 0;
                case ACMotion.StrafeRight:
                    return cmi->raw_state.sidestep_command == (uint)ACMotion.StrafeRight && cmi->raw_state.sidestep_speed > 0;
                case ACMotion.Point:
                    return cmi->raw_state.forward_command == (uint)ACMotion.Point;

            }
            return false;
        }

        private static void EnsureMovement() {
            return;
            if (!HasWantedMovements) {
                return;
            }
            foreach (var kv in WantedMotionStatus.ToArray()) {
                if (WantedMotionStatus[kv.Key] && !GetMotion(kv.Key)) {
                    SetMotion(kv.Key, true);
                }
            }
        }

        #region acclient hooks
        // .text:0068A3D0 ; protected: void __thiscall CInputManager_WIN32::GenerateKeyboardEvent(struct tagMSG const &)
        // .text:0068A3D0? GenerateKeyboardEvent@CInputManager_WIN32 @@IAEXABUtagMSG@@@Z proc near
        //private static Hook CInputManager_WIN32_GenerateKeyboardEvent_Hook = new Hook(0x0068A3D0, 0x0068AC95);

        //[UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal unsafe delegate void CInputManager_WIN32_GenerateKeyboardEvent_Def(CInputManager_WIN32* This, tagMSG* tagMsg);
        //private static unsafe void CInputManager_WIN32_GenerateKeyboardEvent(CInputManager_WIN32* This, tagMSG* tagMsg) {
       //     try {
       //         UBService.WriteLog($"CInputManager_WIN32_GenerateKeyboardEvent: {tagMsg->ToString()}", LogLevel.Warning);
       //         This->GenerateKeyboardEvent(tagMsg);
       //     }
       //     catch(Exception ex) { UBService.LogException(ex); }
       // }

        // ACCmdInterp.SetMotion:
        //public void SetMotion(UInt32 motion, bool fOn) => ((delegate* unmanaged[Thiscall]<ref ACCmdInterp, UInt32, bool, void>)0x0058C140)(ref this, motion, fOn);
        // .text:0058C140 ; void __thiscall ACCmdInterp::SetMotion(ACCmdInterp *this, unsigned int motion, bool fOn) .tet:0058C140 ?SetMotion@ACCmdInterp@@IAEXK_N@Z
        private static MultiHook ACCmdInterp_SetMotion_Hook = new AcClient.MultiHook(0x0058C140, 0x0058C1ED, 0x0058C207, 0x0058C221, 0x0058C237, 0x0058C251, 0x0058C26B, 0x0058C285, 0x0058C29F);

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal unsafe delegate void ACCmdInterp_SetMotion_Def(ACCmdInterp* This, uint motion, bool fOn);
        private static unsafe void ACCmdInterp_SetMotion(ACCmdInterp* This, uint motion, bool fOn) {
            if (!HasWantedMovements) {
                This->SetMotion(motion, fOn);
                return;
            }
            UBService.WriteLog($"Allowed pragmatic motion correction", LogLevel.Error);
            switch ((ACMotion)motion) {
                case ACMotion.TurnLeft:
                    if (!GetMotion(ACMotion.TurnRight))
                        This->SetMotion(motion, fOn);
                    break;
                case ACMotion.TurnRight:
                    if (!GetMotion(ACMotion.TurnLeft))
                        This->SetMotion(motion, fOn);
                    break;
                case ACMotion.Forward:
                    if (!GetMotion(ACMotion.Backward))
                        This->SetMotion(motion, fOn);
                    break;
                case ACMotion.Backward:
                    if (!GetMotion(ACMotion.Forward))
                        This->SetMotion(motion, fOn);
                    break;
                case ACMotion.StrafeLeft:
                    if (!GetMotion(ACMotion.StrafeRight))
                        This->SetMotion(motion, fOn);
                    break;
                case ACMotion.StrafeRight:
                    if (!GetMotion(ACMotion.StrafeLeft))
                        This->SetMotion(motion, fOn);
                    break;
                case (ACMotion)MotionDatCommand.Ready:
                    if (!HasWantedMovements)
                        This->SetMotion(motion, fOn);
                    break;

                default:
                    This->SetMotion(motion, fOn);
                    break;
            }
        }

        #endregion
        #endregion

        internal static void Dispose() {
            UBService.OnTick -= UBService_OnTick;
        }
    }

    
    internal static class Geometry {
        public static System.Numerics.Quaternion HeadingToQuaternion(float angle) {
            return ToQuaternion((float)Math.PI * -angle / 180.0f, 0, 0);
        }

        public static System.Numerics.Quaternion RadiansToQuaternion(float angle) {
            return ToQuaternion(angle, 0, 0);
        }

        public static unsafe double QuaternionToHeading(System.Numerics.Quaternion q) {
            double sinr = 2 * (q.W * q.X + q.Y * q.Z);
            double cosr = 1 - 2 * (q.X * q.X + q.Y * q.Y);
            return Math.Atan2(sinr, cosr);
        }

        public static double CalculateHeading(Vector3 start, Vector3 target) {
            var deltaY = target.Y - start.Y;
            var deltaX = target.X - start.X;
            return (360 - (Math.Atan2(deltaY, deltaX) * 180 / Math.PI) + 90) % 360;
        }

        // https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
        public static System.Numerics.Quaternion ToQuaternion(float yaw, float pitch, float roll) { // yaw (Z), pitch (Y), roll (X)
            // Abbreviations for the various angular functions
            float cy = (float)Math.Cos(yaw * 0.5);
            float sy = (float)Math.Sin(yaw * 0.5);
            float cp = (float)Math.Cos(pitch * 0.5);
            float sp = (float)Math.Sin(pitch * 0.5);
            float cr = (float)Math.Cos(roll * 0.5);
            float sr = (float)Math.Sin(roll * 0.5);

            System.Numerics.Quaternion q = new System.Numerics.Quaternion();

            q.W = cy * cp * cr + sy * sp * sr;
            q.X = cy * cp * sr - sy * sp * cr;
            q.Y = sy * cp * sr + cy * sp * cr;
            q.Z = sy * cp * cr - cy * sp * sr;

            return q;
        }

        public static uint GetLandblockFromCoordinates(float EW, float NS) {
            return GetLandblockFromCoordinates((double)EW, (double)NS);
        }

        public static uint GetLandblockFromCoordinates(double EW, double NS) {
            NS -= 0.5f;
            EW -= 0.5f;
            NS *= 10.0f;
            EW *= 10.0f;

            uint basex = (uint)(EW + 0x400);
            uint basey = (uint)(NS + 0x400);

            if ((int)(basex) < 0 || (int)(basey) < 0 || basex >= 0x7F8 || basey >= 0x7F8) {
                Console.WriteLine("Out of Bounds");
            }
            byte blockx = (byte)(basex >> 3);
            byte blocky = (byte)(basey >> 3);
            byte cellx = (byte)(basex & 7);
            byte celly = (byte)(basey & 7);

            int block = (blockx << 8) | (blocky);
            int cell = (cellx << 3) | (celly);

            int dwCell = (block << 16) | (cell + 1);

            return (uint)dwCell;
        }

        public static PointF LandblockOffsetFromCoordinates(float ew, float ns) {
            var landblock = GetLandblockFromCoordinates(ew, ns);
            return new PointF(
                    EWToLandblock(landblock, ew),
                    NSToLandblock(landblock, ns)
            );
        }

        public static PointF LandblockOffsetFromCoordinates(uint originLandblock, float ew, float ns) {
            var landblock = GetLandblockFromCoordinates(ew, ns);
            return new PointF(
                    EWToLandblock(landblock, ew) + LandblockXDifference(originLandblock, landblock),
                    NSToLandblock(landblock, ns) + LandblockYDifference(originLandblock, landblock)
            );
        }

        public static int LandblockXDifference(uint originLandblock, uint landblock) {
            var olbx = originLandblock >> 24;
            var lbx = landblock >> 24;

            return (int)(lbx - olbx) * 192;
        }

        public static int LandblockYDifference(uint originLandblock, uint landblock) {
            var olby = originLandblock << 8 >> 24;
            var lby = landblock << 8 >> 24;

            return (int)(lby - olby) * 192;
        }

        public static float NSToLandblock(uint landcell, float ns) {
            uint l = (uint)((landcell & 0x00FF0000) / 0x2000);
            var yOffset = ((ns * 10) - l + 1019.5) * 24;
            return (float)yOffset;
        }

        public static float EWToLandblock(uint landcell, float ew) {
            uint l = (uint)((landcell & 0xFF000000) / 0x200000);
            var yOffset = ((ew * 10) - l + 1019.5) * 24;
            return (float)yOffset;
        }

        public static float LandblockToNS(uint landcell, float yOffset) {
            uint l = (uint)((landcell & 0x00FF0000) / 0x2000);
            var ns = ((yOffset / 24) + l - 1019.5) / 10;
            return (float)ns;
        }

        public static float LandblockToEW(uint landcell, float xOffset) {
            uint l = (uint)((landcell & 0xFF000000) / 0x200000);
            var ew = ((xOffset / 24) + l - 1019.5) / 10;
            return (float)ew;
        }

        public static float Distance2d(float x1, float y1, float x2, float y2) {
            return (float)Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }

        public static bool LineIntersectsRect(Point p1, Point p2, Rectangle r) {
            return LineIntersectsLine(p1, p2, new Point(r.X, r.Y), new Point(r.X + r.Width, r.Y)) ||
                   LineIntersectsLine(p1, p2, new Point(r.X + r.Width, r.Y), new Point(r.X + r.Width, r.Y + r.Height)) ||
                   LineIntersectsLine(p1, p2, new Point(r.X + r.Width, r.Y + r.Height), new Point(r.X, r.Y + r.Height)) ||
                   LineIntersectsLine(p1, p2, new Point(r.X, r.Y + r.Height), new Point(r.X, r.Y)) ||
                   (r.Contains(p1) && r.Contains(p2));
        }

        public static bool LineIntersectsLine(Point l1p1, Point l1p2, Point l2p1, Point l2p2) {
            float q = (l1p1.Y - l2p1.Y) * (l2p2.X - l2p1.X) - (l1p1.X - l2p1.X) * (l2p2.Y - l2p1.Y);
            float d = (l1p2.X - l1p1.X) * (l2p2.Y - l2p1.Y) - (l1p2.Y - l1p1.Y) * (l2p2.X - l2p1.X);

            if (d == 0) {
                return false;
            }

            float r = q / d;

            q = (l1p1.Y - l2p1.Y) * (l1p2.X - l1p1.X) - (l1p1.X - l2p1.X) * (l1p2.Y - l1p1.Y);
            float s = q / d;

            if (r < 0 || r > 1 || s < 0 || s > 1) {
                return false;
            }

            return true;
        }

        public static bool RectangleContainsPoint(Rectangle container, Point point) {
            return (point.X >= container.X && point.X <= container.X + container.Width && point.Y >= container.Y && point.Y <= container.Y + container.Height);
        }
    }

    internal static class PhysicsObject {
        public static unsafe Vector3 GetPosition(int id) {
            if (CoreManager.Current.Actions.IsValidObject(id)) {
                var p = CoreManager.Current.Actions.Underlying.GetPhysicsObjectPtr(id);
                return new Vector3(*(float*)(p + 0x84), *(float*)(p + 0x88), *(float*)(p + 0x8C));
            }

            return new Vector3();
        }

        public static unsafe double GetDistance(int id) {
            if (CoreManager.Current.Actions.IsValidObject(id)) {
                var pos = PhysicsObject.GetPosition(id);
                var landcell = PhysicsObject.GetLandcell(id);
                var coords = new Coordinates((uint)landcell, pos.X, pos.Y, pos.Z);
                return coords.DistanceTo(Coordinates.Me);
            }

            return double.MaxValue;
        }

        public static unsafe int GetLandcell(int id) {
            if (CoreManager.Current.Actions.IsValidObject(id)) {
                var p = CoreManager.Current.Actions.Underlying.GetPhysicsObjectPtr(id);
                return *(int*)(p + 0x4C);
            }

            return 0;
        }

        internal static unsafe System.Numerics.Quaternion GetRot(int id) {
            if (CoreManager.Current.Actions.IsValidObject(id)) {
                var p = CoreManager.Current.Actions.Underlying.GetPhysicsObjectPtr(id);
                return new System.Numerics.Quaternion(*(float*)(p + 0x50), *(float*)(p + 0x54), *(float*)(p + 0x58), *(float*)(p + 0x5C));
            }

            return new System.Numerics.Quaternion(0, 0, 0, 0);
        }
    }

}
