﻿using AcClient;
using Decal.Adapter;
using System;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Service.Lib.ACClientModule {

    /// <summary>
    /// Melee / missile attack a target
    /// </summary>
    public class ACAttackAction : QueueAction {
        /// <summary>
        /// The id of the object being attacked
        /// </summary>
        public uint ObjectId { get; }

        public ACAttackAction(uint objectId, ActionOptions options = null) : base(options) {
            ObjectId = objectId;
        }

        public override ActionType ActionType => ActionType.Combat;

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue) {
                Options.TimeoutMilliseconds = 5000;
            }
            if (!Options.MaxRetryCount.HasValue) {
                Options.MaxRetryCount = 1;
            }
        }

        protected override void UpdatePreconditions() {

        }

        public override bool IsValid() {
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }
            if (!Manager.GameState.WorldState.WeenieExists(ObjectId)) {
                SetPermanentResult(ActionError.InvalidTargetObject);
                return false;
            }
            if (!(Manager.GameState.Character.Weenie.CombatMode == Common.Enums.CombatMode.Melee || Manager.GameState.Character.Weenie.CombatMode == Common.Enums.CombatMode.Missile)) {
                SetPermanentResult(ActionError.InvalidCombatMode, "Must be in melee or missile combat mode.");
                return false;
            }
            return true;
        }

        protected override void Start() {
            Manager.MessageHandler.Incoming.Combat_HandleAttackDoneEvent += Incoming_Combat_HandleAttackDoneEvent;
        }

        private void Incoming_Combat_HandleAttackDoneEvent(object sender, Common.Messages.Events.Combat_HandleAttackDoneEvent_S2C_EventArgs e) {
            SetPermanentResult(ActionError.None);
        }

        protected override void Stop() {
            Manager.MessageHandler.Incoming.Combat_HandleAttackDoneEvent -= Incoming_Combat_HandleAttackDoneEvent;
        }

        unsafe protected override bool Execute() {
            if (!Manager.GameState.WorldState.WeenieExists(ObjectId)) {
                SetPermanentResult(ActionError.InvalidTargetObject);
                return false;
            }

            if (UBService.IsInGame && ClientCombatSystem.GetCombatSystem() != null) {
                if (ClientCombatSystem.GetCombatSystem()->attackInProgress == 1) return true;

                switch (Manager.GameState.Character.Weenie.CombatMode) {
                    case Common.Enums.CombatMode.Melee:
                        CM_Combat.Event_TargetedMeleeAttack(ObjectId, ClientCombatSystem.GetCombatSystem()->requestedAttackHeight, ClientCombatSystem.GetCombatSystem()->m_rUIRequestedPower);
                        break;
                    case Common.Enums.CombatMode.Missile:
                        CM_Combat.Event_TargetedMissileAttack(ObjectId, ClientCombatSystem.GetCombatSystem()->requestedAttackHeight, ClientCombatSystem.GetCombatSystem()->m_rUIRequestedPower);
                        break;
                    default:
                        return false;
                }
                return true;
            }
            SetPermanentResult(ActionError.NotLoggedIn);
            return false;
        }
    }
}