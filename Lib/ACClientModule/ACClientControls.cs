﻿using AcClient;
using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Service.Lib.ACClientModule {
    public class ACClientControls {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool PostMessage(IntPtr hhwnd, uint msg, IntPtr wparam, UIntPtr lparam);

        /// <summary>
        /// Get the 3d distance between two objects ids
        /// </summary>
        /// <param name="fromObjectId">First object</param>
        /// <param name="toObjectId">Second object</param>
        /// <returns></returns>
        unsafe public float DistanceTo3D(uint fromObjectId, uint toObjectId = 0) {
            try {
                var first = CPhysicsObj.GetObjectA(fromObjectId);
                var second = CPhysicsObj.GetObjectA(toObjectId);

                if (first is null || second is null) {
                    return -1;
                }

                return first->get_distance_to_object(second, 0);
            }
            catch (Exception ex) { UBService.LogException(ex); }

            return -1;
        }

        /// <summary>
        /// Get the 3d distance between the specified object id and the player
        /// </summary>
        /// <param name="objectId">The object to get the distance to</param>
        /// <returns>The distance</returns>
        unsafe public float DistanceToPlayer(uint objectId) {
            try {
                var first = CPhysicsObj.GetObjectA(objectId);
                if (first is null) {
                    return -1;
                }

                return first->player_distance;
            }
            catch (Exception ex) { UBService.LogException(ex); }

            return -1;
        }

        /// <summary>
        /// Request a health update for an object (game.Messages.Incoming.Combat_QueryHealthResponse)
        /// </summary>
        /// <param name="objectId">The id of the object to request health for</param>
        unsafe public void RequestHealthUpdate(uint objectId) {
            try {
                var obj = CPhysicsObj.GetObjectA(objectId);
                if (obj is null) {
                    return;
                }

                CM_Combat.Event_QueryHealth(objectId);
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }

        /// <summary>
        /// Get the current physics coordinates of the specified object id. This is the client side value, not neccesarily where the server thinks the object is.
        /// </summary>
        /// <param name="objectId">The id of the object to get the physics coordinates of</param>
        /// <returns>The current physics coordinates of the specified object id.</returns>
        public Coordinates GetPhysicsCoordinates(uint objectId) {
            if (UBService.IsInGame && UBService.Scripts.GameState?.WorldState?.WeenieExists(objectId) == true) {
                var pos = PhysicsObject.GetPosition((int)objectId);
                return new Coordinates((uint)PhysicsObject.GetLandcell((int)objectId), pos.X, pos.Y, pos.Z);
            }
            return null;
        }

        /// <summary>
        /// Kills the current acclient process
        /// </summary>
        public void Quit() {
            try {
                PostMessage((IntPtr)UBService.iDecal.HWND, 0x0002 /* WM_DESTROY */, (IntPtr)0, (UIntPtr)0);
            }
            catch { }
        }
    }
}
