﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Service.Lib.ACClientModule {

    /// <summary>
    /// Run your character i a straight line to the specified position. This will first attempt to set the correct heading,
    /// then run there. It does not do any collision detection our routing. It just naively moves your character towards the
    /// position specified, until it is within the specified accuracy.
    /// </summary>
    public class ACRunStraightToAction : QueueAction {
        /// <summary>
        /// The Coordinates to run to
        /// </summary>
        public Coordinates Coordinates { get; }

        /// <summary>
        /// The accuracy in units to allow, when detecting finished.
        /// </summary>
        public double Accuracy { get; }

        private double lastDiff = 0;
        private DateTime lastDiffCheck = DateTime.UtcNow;

        private double targetDegrees = 0;

        public ACRunStraightToAction(Coordinates coords, double accuracy = 1, ActionOptions options = null) : base(options) {
            Coordinates = coords;
            Accuracy = accuracy;
        }

        public override ActionType ActionType => ActionType.Navigation;

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 15000;
            if (!Options.MaxRetryCount.HasValue)
                Options.MaxRetryCount = 15;
        }

        protected override void UpdatePreconditions() {

        }

        public override bool IsValid() {
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }
            return true;
        }

        protected override void Start() {
            CoreManager.Current.RenderFrame += Current_RenderFrame;
        }

        protected override void Stop() {
            CoreManager.Current.RenderFrame -= Current_RenderFrame;
            CoreManager.Current.Actions.SetAutorun(false);
        }

        protected override bool Execute() {
            if (UBService.IsInGame) {
                targetDegrees = Coordinates.Me.HeadingTo(Coordinates);

                lastDiff = Math.Abs(targetDegrees - CoreManager.Current.Actions.Heading);
                lastDiffCheck = DateTime.UtcNow;

                if (Coordinates.Me.DistanceTo(Coordinates) < Accuracy) {
                    Finish();
                    return true;
                }

                // are we facing there?
                if (Math.Abs(targetDegrees - CoreManager.Current.Actions.Heading) <= 1) {
                    ACClientModuleInternal.SetForcedHeading(targetDegrees);
                    // move forward...
                    CoreManager.Current.Actions.SetAutorun(true);
                }
                else {
                    CoreManager.Current.Actions.SetAutorun(false);
                    CoreManager.Current.Actions.Heading = targetDegrees;
                }
                return true;
            }
            SetPermanentResult(ActionError.NotLoggedIn);
            return false;
        }

        private void Finish() {
            CoreManager.Current.Actions.SetAutorun(false);
            if (Accuracy <= 1) {
                ACClientModuleInternal.SetForcedPosition(Coordinates);
            }
            SetPermanentResult(ActionError.None);
        }

        private void Current_RenderFrame(object sender, EventArgs e) {
            try {
                // are we there?
                if (Coordinates.Me.DistanceTo(Coordinates) < Accuracy) {
                    Finish();
                    return;
                }

                if (Math.Abs(targetDegrees - CoreManager.Current.Actions.Heading) > 1) {
                    if (DateTime.UtcNow - lastDiffCheck > TimeSpan.FromMilliseconds(500)) {
                        Execute();
                    }
                    return;
                }

                // are we not making progress with turning?
                var currentDiff = Math.Abs(Coordinates.Me.DistanceTo(Coordinates));
                if (DateTime.UtcNow - lastDiffCheck > TimeSpan.FromMilliseconds(500) && (currentDiff > lastDiff || Math.Abs(currentDiff - lastDiff) < 0.1)) {
                    lastDiffCheck = DateTime.UtcNow;
                    Execute();
                }

                lastDiff = currentDiff;
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }
    }
}
