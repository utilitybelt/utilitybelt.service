﻿using AcClient;
using ACE.Entity;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using static System.Net.Mime.MediaTypeNames;

namespace UtilityBelt.Service.Lib.ACClientModule {
    public enum DecalD3DTextType {
        Text2D,
        Text3D
    }

    public enum DecalD3DShape {
        HorizontalArrow,
        VerticalArrow,
        Ring,
        Cylinder,
        Sphere,
        Cube,
        TiltedCube
    }

    public enum PScriptType : UInt32 {
        PS_Invalid = 0x0,
        PS_Test1 = 0x1,
        PS_Test2 = 0x2,
        PS_Test3 = 0x3,
        PS_Launch = 0x4,
        PS_Explode = 0x5,
        PS_AttribUpRed = 0x6,
        PS_AttribDownRed = 0x7,
        PS_AttribUpOrange = 0x8,
        PS_AttribDownOrange = 0x9,
        PS_AttribUpYellow = 0xA,
        PS_AttribDownYellow = 0xB,
        PS_AttribUpGreen = 0xC,
        PS_AttribDownGreen = 0xD,
        PS_AttribUpBlue = 0xE,
        PS_AttribDownBlue = 0xF,
        PS_AttribUpPurple = 0x10,
        PS_AttribDownPurple = 0x11,
        PS_SkillUpRed = 0x12,
        PS_SkillDownRed = 0x13,
        PS_SkillUpOrange = 0x14,
        PS_SkillDownOrange = 0x15,
        PS_SkillUpYellow = 0x16,
        PS_SkillDownYellow = 0x17,
        PS_SkillUpGreen = 0x18,
        PS_SkillDownGreen = 0x19,
        PS_SkillUpBlue = 0x1A,
        PS_SkillDownBlue = 0x1B,
        PS_SkillUpPurple = 0x1C,
        PS_SkillDownPurple = 0x1D,
        PS_SkillDownBlack = 0x1E,
        PS_HealthUpRed = 0x1F,
        PS_HealthDownRed = 0x20,
        PS_HealthUpBlue = 0x21,
        PS_HealthDownBlue = 0x22,
        PS_HealthUpYellow = 0x23,
        PS_HealthDownYellow = 0x24,
        PS_RegenUpRed = 0x25,
        PS_RegenDownREd = 0x26,
        PS_RegenUpBlue = 0x27,
        PS_RegenDownBlue = 0x28,
        PS_RegenUpYellow = 0x29,
        PS_RegenDownYellow = 0x2A,
        PS_ShieldUpRed = 0x2B,
        PS_ShieldDownRed = 0x2C,
        PS_ShieldUpOrange = 0x2D,
        PS_ShieldDownOrange = 0x2E,
        PS_ShieldUpYellow = 0x2F,
        PS_ShieldDownYellow = 0x30,
        PS_ShieldUpGreen = 0x31,
        PS_ShieldDownGreen = 0x32,
        PS_ShieldUpBlue = 0x33,
        PS_ShieldDownBlue = 0x34,
        PS_ShieldUpPurple = 0x35,
        PS_ShieldDownPurple = 0x36,
        PS_ShieldUpGrey = 0x37,
        PS_ShieldDownGrey = 0x38,
        PS_EnchantUpRed = 0x39,
        PS_EnchantDownRed = 0x3A,
        PS_EnchantUpOrange = 0x3B,
        PS_EnchantDownOrange = 0x3C,
        PS_EnchantUpYellow = 0x3D,
        PS_EnchantDownYellow = 0x3E,
        PS_EnchantUpGreen = 0x3F,
        PS_EnchantDownGreen = 0x40,
        PS_EnchantUpBlue = 0x41,
        PS_EnchantDownBlue = 0x42,
        PS_EnchantUpPurple = 0x43,
        PS_EnchantDownPurple = 0x44,
        PS_VitaeUpWhite = 0x45,
        PS_VitaeDownBlack = 0x46,
        PS_VisionUpWhite = 0x47,
        PS_VisionDownBlack = 0x48,
        PS_SwapHealth_Red_To_Yellow = 0x49,
        PS_SwapHealth_Red_To_Blue = 0x4A,
        PS_SwapHealth_Yellow_To_Red = 0x4B,
        PS_SwapHealth_Yellow_To_Blue = 0x4C,
        PS_SwapHealth_Blue_To_Red = 0x4D,
        PS_SwapHealth_Blue_To_Yellow = 0x4E,
        PS_TransUpWhite = 0x4F,
        PS_TransDownBlack = 0x50,
        PS_Fizzle = 0x51,
        PS_PortalEntry = 0x52,
        PS_PortalExit = 0x53,
        PS_BreatheFlame = 0x54,
        PS_BreatheFrost = 0x55,
        PS_BreatheAcid = 0x56,
        PS_BreatheLightning = 0x57,
        PS_Create = 0x58,
        PS_Destroy = 0x59,
        PS_ProjectileCollision = 0x5A,
        PS_SplatterLowLeftBack = 0x5B,
        PS_SplatterLowLeftFront = 0x5C,
        PS_SplatterLowRightBack = 0x5D,
        PS_SplatterLowRightFront = 0x5E,
        PS_SplatterMidLeftBack = 0x5F,
        PS_SplatterMidLeftFront = 0x60,
        PS_SplatterMidRightBack = 0x61,
        PS_SplatterMidRightFront = 0x62,
        PS_SplatterUpLeftBack = 0x63,
        PS_SplatterUpLeftFront = 0x64,
        PS_SplatterUpRightBack = 0x65,
        PS_SplatterUpRightFront = 0x66,
        PS_SparkLowLeftBack = 0x67,
        PS_SparkLowLeftFront = 0x68,
        PS_SparkLowRightBack = 0x69,
        PS_SparkLowRightFront = 0x6A,
        PS_SparkMidLeftBack = 0x6B,
        PS_SparkMidLeftFront = 0x6C,
        PS_SparkMidRightBack = 0x6D,
        PS_SparkMidRightFront = 0x6E,
        PS_SparkUpLeftBack = 0x6F,
        PS_SparkUpLeftFront = 0x70,
        PS_SparkUpRightBack = 0x71,
        PS_SparkUpRightFront = 0x72,
        PS_PortalStorm = 0x73,
        PS_Hide = 0x74,
        PS_UnHide = 0x75,
        PS_Hidden = 0x76,
        PS_DisappearDestroy = 0x77,
        SpecialState1 = 0x78,
        SpecialState2 = 0x79,
        SpecialState3 = 0x7A,
        SpecialState4 = 0x7B,
        SpecialState5 = 0x7C,
        SpecialState6 = 0x7D,
        SpecialState7 = 0x7E,
        SpecialState8 = 0x7F,
        SpecialState9 = 0x80,
        SpecialState0 = 0x81,
        SpecialStateRed = 0x82,
        SpecialStateOrange = 0x83,
        SpecialStateYellow = 0x84,
        SpecialStateGreen = 0x85,
        SpecialStateBlue = 0x86,
        SpecialStatePurple = 0x87,
        SpecialStateWhite = 0x88,
        SpecialStateBlack = 0x89,
        PS_LevelUp = 0x8A,
        PS_EnchantUpGrey = 0x8B,
        PS_EnchantDownGrey = 0x8C,
        PS_WeddingBliss = 0x8D,
        PS_EnchantUpWhite = 0x8E,
        PS_EnchantDownWhite = 0x8F,
        PS_CampingMastery = 0x90,
        PS_CampingIneptitude = 0x91,
        PS_DispelLife = 0x92,
        PS_DispelCreature = 0x93,
        PS_DispelAll = 0x94,
        PS_BunnySmite = 0x95,
        PS_BaelZharonSmite = 0x96,
        PS_WeddingSteele = 0x97,
        PS_RestrictionEffectBlue = 0x98,
        PS_RestrictionEffectGreen = 0x99,
        PS_RestrictionEffectGold = 0x9A,
        PS_LayingofHands = 0x9B,
        PS_AugmentationUseAttribute = 0x9C,
        PS_AugmentationUseSkill = 0x9D,
        PS_AugmentationUseResistances = 0x9E,
        PS_AugmentationUseOther = 0x9F,
        PS_BlackMadness = 0xA0,
        PS_AetheriaLevelUp = 0xA1,
        PS_AetheriaSurgeDestruction = 0xA2,
        PS_AetheriaSurgeProtection = 0xA3,
        PS_AetheriaSurgeRegeneration = 0xA4,
        PS_AetheriaSurgeAffliction = 0xA5,
        PS_AetheriaSurgeFestering = 0xA6,
        PS_HealthDownVoid = 0xA7,
        PS_RegenDownVoid = 0xA8,
        PS_SkillDownVoid = 0xA9,
        PS_DirtyFightingHealDebuff = 0xAA,
        PS_DirtyFightingAttackDebuff = 0xAB,
        PS_DirtyFightingDefenseDebuff = 0xAC,
        PS_DirtyFightingDamageOverTime = 0xAD,
        NUM_PSCRIPT_TYPES = 0xAE,
        FORCE_PScriptType_32_BIT = 0x7FFFFFFF,
    };


    /// <summary>
    /// ACClient stuff, for scripting
    /// </summary>
    public class ACDecalD3D : IDisposable {

        internal List<DecalD3DObj> decalD3DObjs = new List<DecalD3DObj>();
        private static bool _lastClientIsFocused;

        public ACDecalD3D() {

        }

        public System.Numerics.Vector4 ColorToVec4(uint col) {
            var b = (float)((col) & 0xFF) / 255.0f;
            var g = (float)((col >> 8) & 0xFF) / 255.0f;
            var r = (float)((col >> 16) & 0xFF) / 255.0f;
            var a = (float)((col >> 24) & 0xFF) / 255.0f;

            return new System.Numerics.Vector4(r, g, b, a);
        }

        public uint Vec4ToColor(System.Numerics.Vector4 v4) {
            return ((uint)(v4.Z * 255.0f)) |
            ((uint)(v4.Y * 255.0f) << 8) |
            ((uint)(v4.X * 255.0f) << 16) |
            ((uint)(v4.W * 255.0f) << 24);
        }

        /// <summary>
        /// Set an object with the specified id's transparency.
        /// </summary>
        /// <param name="objectId">The id of the object</param>
        /// <param name="transparency">The transparency. 0 is transparent, 1 is opaque</param>
        unsafe public void SetObjectTransparency(uint objectId, float transparency) {
            try {
                if (!UBService.IsInGame)
                    return;

                CPhysicsObj* phy = (CPhysicsObj*)CoreManager.Current.Actions.Underlying.GetPhysicsObjectPtr(unchecked((int)objectId));
                if (phy != null) {
                    phy->SetTranslucency(transparency, 1);
                }
            }
            catch (Exception ex) { UBService.Scripts?.Resolve<ILogger>()?.LogError(ex.ToString()); }
        }

        /// <summary>
        /// Play a physics script on an object.
        /// </summary>
        /// <param name="objectId">The object id to play the script on</param>
        /// <param name="script">The script to play</param>
        /// <param name="intensity">The script intensity</param>
        unsafe public void PlayObjectScript(uint objectId, PScriptType script, float intensity) {
            try {
                if (!UBService.IsInGame)
                    return;

                CPhysicsObj* phy = (CPhysicsObj*)CoreManager.Current.Actions.Underlying.GetPhysicsObjectPtr(unchecked((int)objectId));
                if (phy != null) {
                    phy->weenie_obj->PlayScript((AcClient.PScriptType)script, intensity);
                }
            }
            catch (Exception ex) { UBService.Scripts?.Resolve<ILogger>()?.LogError(ex.ToString()); }
        }

        #region d3d stuff

        /// <summary>
        /// Point an arrow at the specified coordinates. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="lat">The worldspace latitude to point at</param>
        /// <param name="lng">The worldspace longitude to point at</param>
        /// <param name="z">The worldspace z (altitude) to point at</param>
        /// <param name="color">The color of the arrow, #AARRGGBB</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj PointToCoords(float lat, float lng, float z, uint color) {
            if (CoreManager.Current?.D3DService == null)
                return null;

            return new DecalD3DObj(CoreManager.Current.D3DService.PointToCoords(lat, lng, z, unchecked((int)color)), this);
        }

        /// <summary>
        /// Draw 2D text at the specified coordinates. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="lat">The worldspace latitude to point at</param>
        /// <param name="lng">The worldspace longitude to point at</param>
        /// <param name="z">The worldspace z (altitude) to point at</param>
        /// <param name="text">The text to draw</param>
        /// <param name="color">The color of the text, #AARRGGBB</param>
        /// <param name="font">The name of the font to use</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj MarkCoordsWith2DText(float lat, float lng, float z, string text, uint color, string font = "Arial") {
            if (CoreManager.Current?.D3DService == null)
                return null;

            return new DecalD3DObj(CoreManager.Current.D3DService.MarkCoordsWith2DText(lat, lng, z, text, font, unchecked((int)color)), this);
        }

        /// <summary>
        /// Draw 3D text at the specified coordinates. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="lat">The worldspace latitude to point at</param>
        /// <param name="lng">The worldspace longitude to point at</param>
        /// <param name="z">The worldspace z (altitude) to point at</param>
        /// <param name="text">The text to draw</param>
        /// <param name="color">The color of the text, #AARRGGBB</param>
        /// <param name="font">The name of the font to use</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj MarkCoordsWith3DText(float lat, float lng, float z, string text, uint color, string font = "Arial") {
            if (CoreManager.Current?.D3DService == null)
                return null;

            return new DecalD3DObj(CoreManager.Current.D3DService.MarkCoordsWith3DText(lat, lng, z, text, font, unchecked((int)color)), this);
        }

        /// <summary>
        /// Draw an icon id (from portal.dat) at the specified coordinates. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="lat">The worldspace latitude to point at</param>
        /// <param name="lng">The worldspace longitude to point at</param>
        /// <param name="z">The worldspace z (altitude) to point at</param>
        /// <param name="icon">The id of the icon to draw</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj MarkCoordsWithIcon(float lat, float lng, float z, int icon) {
            if (CoreManager.Current?.D3DService == null)
                return null;

            return new DecalD3DObj(CoreManager.Current.D3DService.MarkCoordsWithIcon(lat, lng, z, icon), this);
        }

        /// <summary>
        /// Point an arrow at the specified object. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="objectId">The if of the object to point at</param>
        /// <param name="color">The color of the arrow, #AARRGGBB</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj PointToObject(uint objectId, uint color) {
            if (CoreManager.Current?.D3DService == null)
                return null;

            UBService.WriteLog($"Point to object: {(int)objectId}/{objectId} // color: {(int)color}/{color}", LogLevel.Debug);

            return new DecalD3DObj(CoreManager.Current.D3DService.PointToObject(unchecked((int)objectId), unchecked((int)color)), this);
        }

        /// <summary>
        /// Mark an object with the specified icon. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="objectId">The if of the object to point at</param>
        /// <param name="icon">The id of the icon to draw</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj MarkObjectWithIcon(uint objectId, int icon) {
            if (CoreManager.Current?.D3DService == null)
                return null;

            return new DecalD3DObj(CoreManager.Current.D3DService.MarkObjectWithIcon(unchecked((int)objectId), icon), this);
        }

        /// <summary>
        /// Mark an object with the specified sharp. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="objectId">The if of the object to point at</param>
        /// <param name="shape">The shape to draw</param>
        /// <param name="color">The color of the shape, #AARRGGBB</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj MarkObjectWithShape(uint objectId, DecalD3DShape shape, uint color) {
            if (CoreManager.Current?.D3DService == null)
                return null;

            return new DecalD3DObj(CoreManager.Current.D3DService.MarkObjectWithShape(unchecked((int)objectId), (D3DShape)shape, unchecked((int)color)), this);
        }

        /// <summary>
        /// Mark the specified coordinates with a shape. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="lat">The worldspace latitude to point at</param>
        /// <param name="lng">The worldspace longitude to point at</param>
        /// <param name="z">The worldspace z (altitude) to point at</param>
        /// <param name="shape">The shape to draw</param>
        /// <param name="color">The color of the shape, #AARRGGBB</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj MarkCoordsWithShape(float lat, float lng, float z, DecalD3DShape shape, uint color) {
            if (CoreManager.Current?.D3DService == null)
                return null;

            return new DecalD3DObj(CoreManager.Current.D3DService.MarkCoordsWithShape(lat, lng, z, (D3DShape)shape, unchecked((int)color)), this);
        }

        /// <summary>
        /// Mark the specified object with 2d text. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="objectId">The id of the object to mark</param>
        /// <param name="text">The text to write</param>
        /// <param name="fontName">The font to use</param>
        /// <param name="color">The color of the text, #AARRGGBB</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj MarkObjectWith2DText(uint objectId, string text, string fontName, uint color) {
            if (CoreManager.Current?.D3DService == null)
                return null;

            return new DecalD3DObj(CoreManager.Current.D3DService.MarkObjectWith2DText(unchecked((int)objectId), text, fontName, unchecked((int)color)), this);
        }

        /// <summary>
        /// Mark the specified object with 3d text. Dispose the returned DecalD3DObj to destroy it.
        /// </summary>
        /// <param name="objectId">The id of the object to mark</param>
        /// <param name="text">The text to write</param>
        /// <param name="fontName">The font to use</param>
        /// <param name="color">The color of the text, #AARRGGBB</param>
        /// <returns>A DecalD3DObj object.</returns>
        public DecalD3DObj MarkObjectWith3DText(uint objectId, string text, string fontName, uint color) {
            if (CoreManager.Current?.D3DService == null)
                return null;

            return new DecalD3DObj(CoreManager.Current.D3DService.MarkObjectWith3DText(unchecked((int)objectId), text, fontName, unchecked((int)color)), this);
        }

        /// <summary>
        /// Creates a new DecalD3D object.
        /// </summary>
        /// <returns></returns>
        public DecalD3DObj NewD3DObj() {
            return new DecalD3DObj(CoreManager.Current.D3DService.NewD3DObj(), this);
        }

        #endregion d3d stuff

        public void Dispose() {
            var objs = decalD3DObjs.ToArray();
            foreach (var d3d in objs) {
                d3d?.Dispose();
            }

            decalD3DObjs.Clear();
        }
    }


    /// <summary>
    /// An DecalD3DObj instance.
    /// </summary>
    public class DecalD3DObj : IDisposable {
        internal D3DObj d3DObj { get; set; }
        internal ACDecalD3D Parent { get; }

        /// <summary>
        /// Wether or not this d3d object is visible
        /// </summary>
        public bool Visible { get => d3DObj.Visible; set => d3DObj.Visible = value; }

        /// <summary>
        /// The primary color
        /// </summary>
        public uint Color {
            get {
                return unchecked((uint)d3DObj.Color);
            }
            set {
                d3DObj.Color = unchecked((int)value);
            }
        }

        /// <summary>
        /// The secondary color?
        /// </summary>
        public uint Color2 {
            get {
                return unchecked((uint)d3DObj.Color2);
            }
            set {
                d3DObj.Color2 = unchecked((int)value);
            }
        }

        /// <summary>
        /// Set to true to autoscale the object. TODO: what exactly does this mean?
        /// </summary>
        public bool Autoscale {
            get {
                return d3DObj.Autoscale;
            }
            set {
                d3DObj.Autoscale = value;
            }
        }

        public bool DrawBackface {
            get {
                return d3DObj.DrawBackface;
            }
            set {
                d3DObj.DrawBackface = value;
            }
        }

        /// <summary>
        /// Horizontal bounce
        /// </summary>
        public float HBounce {
            get {
                return d3DObj.HBounce;
            }
            set {
                d3DObj.HBounce = value;
            }
        }

        /// <summary>
        /// Vertical bounce
        /// </summary>
        public float PBounce {
            get {
                return d3DObj.PBounce;
            }
            set {
                d3DObj.PBounce = value;
            }
        }

        public float PFade {
            get {
                return d3DObj.PFade;
            }
            set {
                d3DObj.PFade = value;
            }
        }

        public float POrbit {
            get {
                return d3DObj.POrbit;
            }
            set {
                d3DObj.POrbit = value;
            }
        }

        public float PSpin {
            get {
                return d3DObj.PSpin;
            }
            set {
                d3DObj.PSpin = value;
            }
        }

        public float ROrbit {
            get {
                return d3DObj.ROrbit;
            }
            set {
                d3DObj.ROrbit = value;
            }
        }

        /// <summary>
        /// X scale modifier
        /// </summary>
        public float ScaleX {
            get {
                return d3DObj.ScaleX;
            }
            set {
                d3DObj.ScaleX = value;
            }
        }

        /// <summary>
        /// Y scale modifier
        /// </summary>
        public float ScaleY {
            get {
                return d3DObj.ScaleY;
            }
            set {
                d3DObj.ScaleY = value;
            }
        }

        /// <summary>
        /// Z scale modifier
        /// </summary>
        public float ScaleZ {
            get {
                return d3DObj.ScaleZ;
            }
            set {
                d3DObj.ScaleZ = value;
            }
        }

        public float AnimationPhaseOffset {
            get {
                return d3DObj.AnimationPhaseOffset;
            }
            set {
                d3DObj.AnimationPhaseOffset = value;
            }
        }

        internal DecalD3DObj(D3DObj obj, ACDecalD3D parent) {
            d3DObj = obj;
            Parent = parent;

            Parent.decalD3DObjs.Add(this);
        }

        /// <summary>
        /// Scale this entire object
        /// </summary>
        /// <param name="factor">The factor to scale by</param>
        public void Scale(float factor) {
            d3DObj.Scale(factor);
        }

        /// <summary>
        /// Orient this object to the camera
        /// </summary>
        /// <param name="verticalTilt">Wether to also tilt vertically</param>
        public void OrientToCamera(bool verticalTilt) {
            d3DObj.OrientToCamera(verticalTilt);
        }

        /// <summary>
        /// Orient this object to the specified coordinates
        /// </summary>
        /// <param name="lat">The worldspace latitude to point at</param>
        /// <param name="lng">The worldspace longitude to point at</param>
        /// <param name="z">The worldspace z (altitude) to point at</param>
        /// <param name="verticalTilt">Wether to also tilt vertically</param>
        public void OrientToCoords(float lat, float lng, float z, bool verticalTilt) {
            d3DObj.OrientToCoords(lat, lng, z, verticalTilt);
        }

        /// <summary>
        /// Orient this object to another object.
        /// </summary>
        /// <param name="objectId">The id of the object to orient to</param>
        /// <param name="fractHeight"></param>
        /// <param name="verticalTilt">Wether to also tilt vertically</param>
        public void OrientToObject(uint objectId, float fractHeight, bool verticalTilt) {
            d3DObj.OrientToObject((int)objectId, fractHeight, verticalTilt);
        }

        /// <summary>
        /// Orient this object to the player
        /// </summary>
        /// <param name="verticalTilt">Wether to also tilt vertically</param>
        public void OrientToPlayer(bool verticalTilt) {
            d3DObj.OrientToPlayer(verticalTilt);
        }

        /// <summary>
        /// Anchor this object to the specified coordinates.
        /// </summary>
        /// <param name="lat">The worldspace latitude to point at</param>
        /// <param name="lng">The worldspace longitude to point at</param>
        /// <param name="z">The worldspace z (altitude) to point at</param>
        public void Anchor(float lat, float lng, float z) {
            d3DObj.Anchor(lat, lng, z);
        }

        /// <summary>
        /// Anchor this object to the specified object.
        /// </summary>
        /// <param name="objectId">The id of the object to anchor this to</param>
        /// <param name="height">The height to anchor at</param>
        /// <param name="xOffset">X offset</param>
        /// <param name="yOffset">Y offset</param>
        /// <param name="zOffset">Z offset</param>
        public void Anchor(uint objectId, float height, float xOffset, float yOffset, float zOffset) {
            d3DObj.Anchor((int)objectId, height, xOffset, yOffset, zOffset);
        }

        /// <summary>
        /// Sets the text of this object.
        /// </summary>
        /// <param name="text">The text to set this object to</param>
        public void SetText(string text) {
            SetText(DecalD3DTextType.Text2D, text, "Arial", 0);
        }

        /// <summary>
        /// Sets the text of this object.
        /// </summary>
        /// <param name="text">The text to set this object to</param>
        /// <param name="fontName">The name of the font to use</param>
        public void SetText(string text, string fontName) {
            SetText(DecalD3DTextType.Text2D, text, fontName, 0);
        }

        /// <summary>
        /// Sets the text of this object.
        /// </summary>
        /// <param name="text">The text to set this object to</param>
        /// <param name="fontName">The name of the font to use</param>
        /// <param name="color">The color of the text, #AARRGGBB</param>
        public void SetText(string text, string fontName, uint color) {
            SetText(DecalD3DTextType.Text2D, text, fontName, color);
        }

        /// <summary>
        /// Sets the text of this object.
        /// </summary>
        /// <param name="type">The text type.</param>
        /// <param name="text">The text to set this object to</param>
        /// <param name="fontName">The name of the font to use</param>
        /// <param name="color">The color of the text, #AARRGGBB</param>
        public void SetText(DecalD3DTextType type, string text, string fontName, uint color) {
            d3DObj.SetText((D3DTextType)type, text, fontName, (int)color);
        }

        /// <summary>
        /// Set the icon of this object
        /// </summary>
        /// <param name="icon">The id of the icon to draw</param>
        public void SetIcon(int icon) {
            d3DObj.SetIcon(icon);
        }

        /// <summary>
        /// Set the shape of this object
        /// </summary>
        /// <param name="shape">The shape to set to</param>
        public void SetShape(DecalD3DShape shape) {
            d3DObj.SetShape((D3DShape)shape);
        }

        /// <summary>
        /// Destroy this d3d obj
        /// </summary>
        public void Dispose() {
            Parent.decalD3DObjs.Remove(this);
            d3DObj?.Dispose();
        }
    }
}
