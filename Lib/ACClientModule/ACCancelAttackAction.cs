﻿using AcClient;
using Decal.Adapter;
using System;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;

namespace UtilityBelt.Service.Lib.ACClientModule {

    /// <summary>
    /// Cancel any current Melee / missile attack
    /// </summary>
    public class ACCancelAttackAction : QueueAction {
        public ACCancelAttackAction(ActionOptions options = null) : base(options) {
            
        }

        public override ActionType ActionType => ActionType.Combat;

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 400;
            if (!Options.MaxRetryCount.HasValue)
                Options.MaxRetryCount = 1;
        }

        protected override void UpdatePreconditions() {

        }

        public override bool IsValid() {
            if (Manager.GameState.State != ClientState.In_Game) {
                SetPermanentResult(ActionError.NotLoggedIn);
                return false;
            }
            if (!(Manager.GameState.Character.Weenie.CombatMode == Common.Enums.CombatMode.Melee || Manager.GameState.Character.Weenie.CombatMode == Common.Enums.CombatMode.Missile)) {
                SetPermanentResult(ActionError.InvalidCombatMode, "Must be in melee or missile combat mode.");
                return false;
            }
            return true;
        }

        protected override void Start() {

        }

        protected override void Stop() {

        }

        unsafe protected override bool Execute() {
            if (UBService.IsInGame && ClientCombatSystem.GetCombatSystem() != null) {
                ClientCombatSystem.GetCombatSystem()->AbortAutomaticAttack();
                SetPermanentResult(ActionError.None);
                return true;
            }
            return false;
        }
    }
}