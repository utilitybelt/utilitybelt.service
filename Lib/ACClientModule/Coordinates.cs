﻿using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace UtilityBelt.Service.Lib.ACClientModule {
    /// <summary>
    /// Represents an ingame location
    /// </summary>
    public class Coordinates {
        /// <summary>
        /// North/South coordinate
        /// </summary>
        public float NS {
            get => Geometry.LandblockToNS(LandCell, LocalY);
        }

        /// <summary>
        /// East/West coordinate
        /// </summary>
        public float EW {
            get => Geometry.LandblockToEW(LandCell, LocalX);
        }

        /// <summary>
        /// Z coordinate.
        /// </summary>
        public float Z {
            get => LocalZ;
        }

        public static Coordinates Me {
            get {
                if (UBService.IsInGame) {
                    var me = CoreManager.Current.CharacterFilter.Id;
                    var pos = PhysicsObject.GetPosition(me);
                    return new Coordinates((uint)PhysicsObject.GetLandcell(me), pos.X, pos.Y, pos.Z);
                }
                return null;
            }
        }

        /// <summary>
        /// Landcell this coordinate object represents
        /// </summary>
        public uint LandCell { get; set; }

        /// <summary>
        /// The local X offset within this landcell that this coordinate object represents.
        /// </summary>
        public float LocalX { get; set; }

        /// <summary>
        /// The local Y offset within this landcell that this coordinate object represents.
        /// </summary>
        public float LocalY { get; set; }

        /// <summary>
        /// The local Z offset within this landcell that this coordinate object represents.
        /// </summary>
        public float LocalZ { get; set; }

        public static Regex CoordinateRegex = new Regex(@"(?<NSval>[0-9]{1,3}(?:\.[0-9]{1,3})?)(?<NSchr>(?:[ns]))(?:[,\s]+)?(?<EWval>[0-9]{1,3}(?:\.[0-9]{1,3})?)(?<EWchr>(?:[ew]))?(,?\s*(?<Zval>\-?\d+.?\d+)z)?", RegexOptions.IgnoreCase | RegexOptions.Compiled);

        public Coordinates() {

        }

        public Coordinates(uint landCell, float localX, float localY, float localZ) {
            LandCell = landCell;
            LocalX = localX;
            LocalY = localY;
            LocalZ = localZ;
        }

        public Coordinates(float northSouth, float eastWeast, float z) {
            LandCell = Geometry.GetLandblockFromCoordinates(eastWeast, northSouth);
            LocalX = Geometry.EWToLandblock(LandCell, eastWeast);
            LocalY = Geometry.NSToLandblock(LandCell, northSouth);
            LocalZ = z;
        }

        /*
        public Coordinates(float northSouth, float eastWest, float z = 0) {
            LandCell = Geometry.GetLandblockFromCoordinates(eastWest, northSouth);
            LocalX = Geometry.EWToLandblock(LandCell, eastWest);
            LocalY = Geometry.NSToLandblock(LandCell, northSouth);
            LocalZ = z;
        }

        unsafe public Coordinates(string coordsToParse) {
            float _ns = 0;
            float _ew = 0;

            if (CoordinateRegex.IsMatch(coordsToParse)) {
                var m = CoordinateRegex.Match(coordsToParse);
                if (float.TryParse(m.Groups["NSval"].Value, NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out float ns)) {
                    _ns = ns;
                    if (!string.IsNullOrEmpty(m.Groups["NSchr"].Value.ToLower())) {
                        _ns *= m.Groups["NSchr"].Value.ToLower().Equals("n") ? 1 : -1;
                    }
                }
                if (float.TryParse(m.Groups["EWval"].Value, NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out float ew)) {
                    _ew = ew;
                    if (!string.IsNullOrEmpty(m.Groups["EWchr"].Value.ToLower())) {
                        _ew *= m.Groups["EWchr"].Value.ToLower().Equals("e") ? 1 : -1;
                    }
                }
                if (float.TryParse(m.Groups["Zval"].Value, NumberStyles.Float, CultureInfo.InvariantCulture.NumberFormat, out float z)) {
                    LocalZ = z;
                }



                try {
                    CPhysicsObj* phy = *CPhysicsObj.player_object;
                    if (phy == null)
                        return;

                }
                catch (Exception ex) { UBService.LogException(ex); }
                LandCell = Geometry.GetLandblockFromCoordinates(_ew, _ns);
                LocalY = Geometry.NSToLandblock(LandCell, _ns);
                LocalX = Geometry.EWToLandblock(LandCell, _ew);
            }
        }
        */
        public float HeadingTo(Coordinates end) {
            var deltaY = end.NS - NS;
            var deltaX = end.EW - EW;
            return (float)(360f - (Math.Atan2(deltaY, deltaX) * 180f / Math.PI) + 90f) % 360f;
        }

        public float DistanceTo(Coordinates other) {
            if (other is null) return -1;
            var nsdiff = (((NS * 10) + 1019.5) * 24) - (((other.NS * 10) + 1019.5) * 24);
            var ewdiff = (((EW * 10) + 1019.5) * 24) - (((other.EW * 10) + 1019.5) * 24);
            return (float)Math.Abs(Math.Sqrt(Math.Pow(Math.Abs(nsdiff), 2) + Math.Pow(Math.Abs(ewdiff), 2) + Math.Pow(Math.Abs(Z - other.Z), 2)));
        }

        public float DistanceToFlat(Coordinates other) {
            if (other is null) return -1;
            var nsdiff = (((NS * 10) + 1019.5) * 24) - (((other.NS * 10) + 1019.5) * 24);
            var ewdiff = (((EW * 10) + 1019.5) * 24) - (((other.EW * 10) + 1019.5) * 24);
            return (float)Math.Abs(Math.Sqrt(Math.Pow(Math.Abs(nsdiff), 2) + Math.Pow(Math.Abs(ewdiff), 2)));
        }

        public override string ToString() {
            return $"{Math.Abs(NS).ToString("F2")}{(NS >= 0 ? "N" : "S")}, {Math.Abs(EW).ToString("F2")}{(EW >= 0 ? "E" : "W")}, {(Z / 240).ToString("F2")}Z [0x{LandCell:X8} {LocalX}, {LocalY}, {LocalZ}]";
        }
    }

}
