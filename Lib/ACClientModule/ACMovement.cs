﻿using AcClient;
using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Scripting.Actions;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Events;
using UtilityBelt.Scripting.Interop;
using Position = UtilityBelt.Scripting.Interop.Position;

namespace UtilityBelt.Service.Lib.ACClientModule {
    /// <summary>
    /// A motion command type
    /// </summary>
    public enum ACMotion { Forward = 0x45000005, Backward = 0x45000006, TurnRight = 0x6500000D, TurnLeft = 0x6500000E, StrafeRight = 0x6500000F, StrafeLeft = 0x65000010, Walk = 0x11112222, Point = 0x430000f0 }

    /// <summary>
    /// Movement stuff. This is an experimental api and may change / move in the future.
    /// </summary>
    public class ACMovement : IDisposable {

        private Dictionary<ACMotion, bool> SetMotionStatus = new Dictionary<ACMotion, bool> { { ACMotion.Forward, false }, { ACMotion.Backward, false }, { ACMotion.TurnRight, false }, { ACMotion.TurnLeft, false }, { ACMotion.StrafeRight, false }, { ACMotion.StrafeLeft, false }, { ACMotion.Walk, false }, { ACMotion.Point, false } };

        /// <summary>
        /// Your character's current heading. This uses the phyics position so it should be what the client is showing, but may not line up
        /// exactly with what the server things your heading is.
        /// </summary>
        public double Heading => UBService.IsInGame ? CoreManager.Current.Actions.Heading : 0;

        /// <summary>
        /// Your character's current physics position. This is the client side value, not neccesarily where the server thinks you are.
        /// </summary>
        public Coordinates MyPhysicsCoordinates => Coordinates.Me;

        /// <summary>
        /// Get the current physics coordinates of the specified object id. This is the client side value, not neccesarily where the server thinks the object is.
        /// </summary>
        /// <param name="objectId">The id of the object to get the physics coordinates of</param>
        /// <returns>The current physics coordinates of the specified object id.</returns>
        public Coordinates GetPhysicsCoordinates(uint objectId) {
            if (UBService.IsInGame && UBService.Scripts.GameState?.WorldState?.WeenieExists(objectId) == true) {
                var pos = PhysicsObject.GetPosition((int)objectId);
                return new Coordinates((uint)PhysicsObject.GetLandcell((int)objectId), pos.X, pos.Y, pos.Z);
            }
            return null;
        }

        /// <summary>
        /// Turn your character to the specified heading, in degrees.
        /// </summary>
        /// <param name="degrees">The heading to turn to. 0 is north, 90 is east, 180 is south, 270 is west.</param>
        /// <param name="accuracy">The accuracy within degrees to the specified heading to consider finished</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ACSetHeadingAction SetHeading(double degrees, double accuracy = 2, ActionOptions options = null, Action<ACSetHeadingAction> callback = null) {
            return UBService.Scripts.GameState.Actions.MakeAndEnqueuePromise(new ACSetHeadingAction(degrees, accuracy, options), callback);
        }

        /// <summary>
        /// Run your character in a straight line to the specified position. This will first attempt to set the correct heading,
        /// then run there. It does not do any collision detection or path finding. It just naively moves your character towards the
        /// position specified, until it is within the specified accuracy.
        /// </summary>
        /// <param name="coordinates">The coordinates to run to</param>
        /// <param name="accuracy">The accuracy in units to allow, when detecting finished.</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ACRunStraightToAction RunStraightTo(Coordinates coordinates, double accuracy = 2, ActionOptions options = null, Action<ACRunStraightToAction> callback = null) {
            return UBService.Scripts.GameState.Actions.MakeAndEnqueuePromise(new ACRunStraightToAction(coordinates, accuracy, options), callback);
        }

        /// <summary>
        /// Walk your character in a straight line to the specified position. This will first attempt to set the correct heading,
        /// then walk there. It does not do any collision detection or path finding. It just naively moves your character towards the
        /// position specified, until it is within the specified accuracy.
        /// </summary>
        /// <param name="coordinates">The coordinates to walk to</param>
        /// <param name="accuracy">The accuracy in units to allow, when detecting finished.</param>
        /// <param name="options">Action options</param>
        /// <param name="callback">Called when the action completes</param>
        /// <returns>A promise</returns>
        public ACWalkStraightToAction WalkStraightTo(Coordinates coordinates, double accuracy = 2, ActionOptions options = null, Action<ACWalkStraightToAction> callback = null) {
            return UBService.Scripts.GameState.Actions.MakeAndEnqueuePromise(new ACWalkStraightToAction(coordinates, accuracy, options), callback);
        }

        /// <summary>
        /// Set the specified ACMotion either on or off. This is the same as manually pressing keys to move. You should use other methods like
        /// the SetHeading and RunStraightTo actions if you are able.
        /// </summary>
        /// <param name="motion">The motion to set</param>
        /// <param name="fOn">Wether to turn the motion on / off</param>
        public unsafe void SetMotion(ACMotion motion, bool fOn) {
            if (!UBService.IsInGame)
                return;

            SetMotionStatus.AddOrUpdate(motion, fOn);
            ACClientModuleInternal.SetMotion(motion, fOn);
        }

        /// <summary>
        /// Get the status of the specified ACMotion.
        /// </summary>
        /// <param name="motion">The ACMotion to get the status</param>
        /// <returns>True if set to on, false otherwise.</returns>
        public unsafe bool GetMotion(ACMotion motion) {
            return ACClientModuleInternal.GetMotion(motion);
        }

        /// <summary>
        /// Clear all motions set by this script.
        /// </summary>
        /// <returns></returns>
        public unsafe bool ClearAllMotions() {
            if (UBService.Scripts.GameState.State != ClientState.In_Game)
                return false;

            foreach (var key in SetMotionStatus.Keys.ToArray()) {
                if (SetMotionStatus[key])
                    SetMotion(key, false);
            }
            return true;
        }

        public void Dispose() {
            ClearAllMotions();
        }
    }
}
