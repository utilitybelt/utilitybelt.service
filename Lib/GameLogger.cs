﻿using Decal.Adapter;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

namespace UtilityBelt.Service.Lib {
    public class GameLogger : ILogger {
        private readonly Func<LogLevel> logLevelFunc;
        private readonly Func<LogLevel> printLevelFunc;

        public LogLevel LogLevel => logLevelFunc();
        public LogLevel PrintLevel => printLevelFunc();
        public string LogFile { get; set; } = "ubservice.logs.txt";

        public GameLogger(string logFile, Func<LogLevel> logLevelFunc, Func<LogLevel> printLevelFunc) {
            LogFile = logFile;
            this.logLevelFunc = logLevelFunc;
            this.printLevelFunc = printLevelFunc;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter) {
            Log(formatter(state, exception), logLevel);
        }

        public bool IsEnabled(LogLevel logLevel) {
            return logLevel >= LogLevel || logLevel >= PrintLevel;
        }

        private class LogScope : IDisposable {
            public void Dispose() {
                
            }
        }

        public IDisposable BeginScope<TState>(TState state) where TState : notnull {
            return new LogScope();
        }

        public void Log(string message, LogLevel logLevel = LogLevel.Information) {
            PrintInternal(message, logLevel);
            WriteLog(message, logLevel);
        }

        public void Log(Exception ex) {
            PrintInternal(ex.ToString(), LogLevel.Error);
            WriteLog(ex.ToString(), LogLevel.Error);
        }

        public void Print(string message, LogLevel logLevel = LogLevel.Information) {
            PrintInternal(message, logLevel);
            WriteLog(message, logLevel);
        }

        public void PrintInternal(string message, LogLevel logLevel = LogLevel.Information) {
            try {
                if (UBService.IsInGame && logLevel >= PrintLevel) {
                    LogToChat(message, logLevel);
                }
            }
            catch { }
        }

        public void LogToChat(string message, LogLevel logLevel = LogLevel.Information) {
            try {
                if (UBService.IsInGame) {
                    switch (logLevel) {
                        case LogLevel.Trace:
                            CoreManager.Current?.Actions.AddChatText(message, (int)Common.Enums.ChatMessageType.Emote);
                            break;
                        case LogLevel.Debug:
                            CoreManager.Current?.Actions.AddChatText(message, (int)Common.Enums.ChatMessageType.Speech);
                            break;
                        case LogLevel.Information:
                            CoreManager.Current?.Actions.AddChatText(message, (int)Common.Enums.ChatMessageType.WorldBroadcast);
                            break;
                        case LogLevel.Warning:
                            CoreManager.Current?.Actions.AddChatText(message, (int)Common.Enums.ChatMessageType.AdminTell);
                            break;
                        case LogLevel.Error:
                            CoreManager.Current?.Actions.AddChatText(message, (int)Common.Enums.ChatMessageType.Help);
                            break;
                        default:
                            CoreManager.Current?.Actions.AddChatText(message, (int)Common.Enums.ChatMessageType.WorldBroadcast);
                            break;
                    }
                }
            }
            catch { }
        }

        private void WriteLog(string text, LogLevel level) {
            try {
                if (level >= LogLevel) {
                    var fileInfo = new FileInfo(LogFile);
                    if (fileInfo.Exists && fileInfo.Length > UBService.MAX_LOG_SIZE) {
                        File.Delete(LogFile);
                    }
                    File.AppendAllText(LogFile, $"{DateTime.Now} {level}: {text}\n");
                }
            }
            catch { }
        }
    }
}
