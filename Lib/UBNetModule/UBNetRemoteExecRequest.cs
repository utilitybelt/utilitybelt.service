﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Service.Lib.UBNetModule {
    /// <summary>
    /// Used for executing a lua function on a remote set of clients.
    /// </summary>
    [ProtoContract]
    public class UBNetRemoteExecRequest {
        /// <summary>
        /// The function to execute
        /// </summary>
        [ProtoMember(1)]
        public string Function { get; set; }

        /// <summary>
        /// A serialized table of arguments to be passed to the function
        /// </summary>
        [ProtoMember(2)]
        public string Arguments { get; set; }

        /// <summary>
        /// The name of the script executing this action, if any
        /// </summary>
        [ProtoMember(3)]
        public string ScriptName { get; set; } = "global";

        /// <summary>
        /// Create a new Remote Exec request.
        /// </summary>
        public UBNetRemoteExecRequest() { }

        /// <summary>
        /// Create a new Remote Exec request.
        /// </summary>
        public UBNetRemoteExecRequest(string function, string arguments, string scriptName) {
            Function = function;
            Arguments = arguments;
            ScriptName = scriptName;
        }
    }
}
