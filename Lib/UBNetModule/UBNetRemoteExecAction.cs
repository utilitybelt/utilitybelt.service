﻿using AcClient;
using Microsoft.DirectX.Direct3D;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UtilityBelt.Networking;
using UtilityBelt.Networking.Messages;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using WattleScript.Interpreter;

namespace UtilityBelt.Service.Lib.UBNetModule {
    /// <summary>
    /// Execute a function on the specified remote clients, and get the results.
    /// </summary>
    public class UBNetRemoteExecAction : QueueAction {
        private Task<List<UBNetRemoteExecResponse>> _task;

        // private Task<MultiRemoteExecResponse> _task;

        /// <summary>
        /// A filter to apply, that dictates which clients this is broadcast to. If left blank, this will be broadcast
        /// to all connected clients.
        /// </summary>
        public ClientFilter Filter { get; }

        /// <summary>
        /// The function to execute
        /// </summary>
        public string Function { get; }

        /// <summary>
        /// A serialized table of arguments to be passed to the function
        /// </summary>
        public string Arguments { get; }

        /// <summary>
        /// The name of the script executing this action, if any
        /// </summary>
        public string ScriptName { get; } = "global";

        public override ActionType ActionType => ActionType.Immediate;

        /// <summary>
        /// The results from the remote clients.
        /// </summary>
        public List<UBNetRemoteExecResponse> Results { get; private set; }

        public UBNetRemoteExecAction(Closure closure, Table arguments, ClientFilter filter = null, ActionOptions options = null) : base(options) {
            // TODO: we might want to check for multiple source chunks and combine all code from GetSourceCode(chunk_id) ?
            var functionString = closure.Function.GetSourceFragment(closure.OwnerScript.GetSourceCode(0).Code);
            // there's some bugs in GetSourceFragment above, so we have to massage the data a little when the function is not named..
            // for named functions we also need to strip the name
            if (!functionString.Trim().StartsWith("function")) {
                functionString = $"function {functionString}";
            }
            else {
                functionString = Regex.Replace(functionString, @"^\s*function\s+[^\(]+", "function ");
            }
            Function = functionString;
            Filter = filter;
            Arguments = WattleScript.Interpreter.Serialization.Json.JsonTableConverter.TableToJson(arguments);
            ScriptName = Manager.GetScript(closure.OwnerScript).Name;
            Manager.Resolve<ILogger>()?.LogTrace($"{this} Function string is: {functionString}\n - with args: {Arguments}");
        }

        protected override void UpdateDefaultOptions() {
            if (!Options.TimeoutMilliseconds.HasValue)
                Options.TimeoutMilliseconds = 30000;
        }

        protected override void UpdatePreconditions() {

        }

        public override bool IsValid() {
            if (UBService.UBNet?.UBNetClient?.IsConnected != true) {
                SetPermanentResult(ActionError.Exception, $"Not connected to UBNet.");
                return false;
            }
            return true;
        }

        protected override void Start() {
            Manager.GameState.OnRender2D += GameState_OnRender2D;
        }

        private void GameState_OnRender2D(object sender, System.EventArgs e) {
            if (_task.IsCompleted) {
                Results = _task.Result.ToList();
                SetPermanentResult(ActionError.None);
            }
            else if (_task.IsCanceled || _task.IsFaulted) {
                SetPermanentResult(ActionError.Exception, $"Remote exec failed... {_task.Exception}");
            }
        }

        protected override void Stop() {
            Manager.GameState.OnRender2D -= GameState_OnRender2D;
        }

        unsafe protected override bool Execute() {
            if (UBService.UBNet?.UBNetClient?.IsConnected == true) {
                var req = new UBNetRemoteExecRequest(Function, Arguments, ScriptName);
                _task = UBService.UBNet.UBNetClient.SendBroadcastRequest<UBNetRemoteExecResponse, UBNetRemoteExecRequest>(req, Filter, (senderId, totalResCount, currentResCount, success, res) => {
                    if (senderId > 0) {
                        var client = UBService.UBNet.UBNetClient.Clients.Select(c => c.Value).FirstOrDefault(c => c.Id == senderId);
                        if (client != null) {
                            res.Sender = new UBNetClientInfo(client.Id, client.Name, client.Type, client.Tags, client.Properties);
                        }
                        Manager.Resolve<ILogger>().LogTrace($"{this}: Got RemoteExecResponse from client: Sender:{res.Sender} Result ({currentResCount}/{totalResCount}|{(success ? "success" : "error")}): {(success ? res.Response : res.Error)}");
                    }
                    else {
                        Manager.Resolve<ILogger>().LogTrace($"{this}: Got RemoteExecResponse ack from server. Waiting for results ({currentResCount}/{totalResCount}).");
                    }
                });
                return true;
            }

            SetPermanentResult(ActionError.Exception, $"Not connected to UBNet.");
            return false;
        }
    }
}