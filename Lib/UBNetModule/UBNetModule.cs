﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UtilityBelt.Networking;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Messages;
using UtilityBelt.Scripting.Interop;
using WattleScript.Interpreter;

namespace UtilityBelt.Service.Lib.UBNetModule {
    /// <summary>
    /// Access to UBNet. This allows you to talk to clients on the same computer / ubnet instance.
    /// </summary>
    public class UBNetModule : IDisposable {
        private Dictionary<string, List<Action<string, string>>> _channelHandlers = new Dictionary<string, List<Action<string, string>>>();

        /// <summary>
        /// Wether you are currently connected to the UBNet server.
        /// </summary>
        public bool IsConnected => UBService.UBNet?.UBNetClient?.IsConnected == true;

        public UBNetModule() {
            UBService.UBNet.UBNetClient.OnChannelMessage += UBNetClient_OnChannelMessage;
        }

        private void UBNetClient_OnChannelMessage(object sender, ChannelMessageReceivedEventArgs e) {
            if (_channelHandlers.TryGetValue(e.ChannelBroadcast.Channel, out var actions)) {
                foreach (var action in actions) {
                    action?.Invoke(e.ChannelBroadcast.Channel, e.ChannelBroadcast.Message);
                }
            }
        }

        /// <summary>
        /// Get a list of all connected ubnet clients. This does not include yourself.
        /// </summary>
        /// <returns>A list of all ClientInfos</returns>
        public IList<UBNetClientInfo> GetClients() {
            if (!IsConnected) return new List<UBNetClientInfo>();
            return UBService.UBNet.UBNetClient.Clients.Select(kv => {
                return new UBNetClientInfo(kv.Value.Id, kv.Value.Name, kv.Value.Type, kv.Value.Tags, kv.Value.Properties);
            }).ToList();
        }

        /// <summary>
        /// Get a list of all connected ubnet clients of the specified type. This does not include yourself.
        /// </summary>
        /// <returns>A list of matching ClientInfos</returns>
        public IList<UBNetClientInfo> GetClients(ClientType type) {
            return GetClients().Where(c => c.Type == type).ToList();
        }

        /// <summary>
        /// Get a list of all connected ubnet clients that have the specified tag. This does not include yourself.
        /// </summary>
        /// <returns>A list of matching ClientInfos</returns>
        public IList<UBNetClientInfo> GetClients(string tag) {
            return GetClients().Where(c => c.Tags.Contains(tag)).ToList();
        }

        /// <summary>
        /// Get a list of all connected ubnet clients that match the specified function filter. This does not include yourself.
        /// </summary>
        /// <returns>A list of matching ClientInfos</returns>
        public IList<UBNetClientInfo> GetClients(Func<UBNetClientInfo, bool> filterFunction) {
            return GetClients().Where(filterFunction).ToList();
        }

        /// <summary>
        /// Subscribe to the specified channel, and run the specified handler when a message is received.
        /// </summary>
        /// <param name="channel">The channel to subscribe to.</param>
        /// <param name="handler"></param>
        public void SubscribeToChannel(string channel, Action<string, string> handler) {
            UBService.UBNet?.UBNetClient?.SubscribeToChannel(channel);
            if (!_channelHandlers.ContainsKey(channel)) {
                _channelHandlers.Add(channel, new List<Action<string, string>>());
            }

            _channelHandlers[channel].Add(handler);
        }

        /// <summary>
        /// Publish a message on a channel.
        /// </summary>
        /// <param name="channel">The channel to publish the message on.</param>
        /// <param name="message">The message to publish.</param>
        public void PublishToChannel(string channel, string message) {
            UBService.UBNet?.UBNetClient?.PublishToChannel(channel, message);
        }

        /// <summary>
        /// Unsubscribe from the specified channel / handler combo.
        /// </summary>
        /// <param name="channel">The channel to unsubscribe from.</param>
        /// <param name="handler"></param>
        public void UnsubscribeFromChannel(string channel, Action<string, string> handler) {
            UBService.UBNet?.UBNetClient?.UnsubscribeFromChannel(channel);
            if (_channelHandlers.ContainsKey(channel)) {
                _channelHandlers[channel].Remove(handler);
            }
        }

        /// <summary>
        /// Run a remote lua function on the specified clients, and await the results.
        /// </summary>
        /// <param name="closure">The function to run. Cannot use upvalues here.</param>
        /// <param name="arguments">A list of arguments to pass to the remote function. This is a table that gets serialized and sent along with the remote function.</param>
        /// <param name="filter"></param>
        /// <param name="options"></param>
        /// <param name="callback"></param>
        /// <returns></returns>
        public UBNetRemoteExecAction RemoteExec(Closure closure, Table arguments = null, ClientFilter filter = null, ActionOptions options = null, Action<UBNetRemoteExecAction> callback = null) {
            return UBService.Scripts.GameState.Actions.MakeAndEnqueuePromise(new UBNetRemoteExecAction(closure, arguments, filter, options), callback);
        }

        public void Dispose() {
            UBService.UBNet.UBNetClient.OnChannelMessage -= UBNetClient_OnChannelMessage;
        }
    }
}
