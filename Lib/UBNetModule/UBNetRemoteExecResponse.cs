﻿using ProtoBuf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WattleScript.Interpreter;

namespace UtilityBelt.Service.Lib.UBNetModule {
    /// <summary>
    /// Response from a remote client replying to a UBNetRemoteExecRequest
    /// </summary>
    [ProtoContract]
    public class UBNetRemoteExecResponse {
        /// <summary>
        /// The UBNetClientInfo of the client that sent this message
        /// </summary>
        [ProtoIgnore]
        public UBNetClientInfo Sender { get; set; }

        /// <summary>
        /// The response from the remote client, converted to a native lua value.
        /// </summary>
        [ProtoIgnore]
        public DynValue Value { get; set; }

        /// <summary>
        /// The ubnet client id of the client that sent this message
        /// </summary>
        [ProtoMember(1)]
        public uint SenderId { get; set; }

        /// <summary>
        /// Wether the remote client was able to execute the function without any errors.
        /// </summary>
        [ProtoMember(2)]
        public bool Success { get; set; }

        /// <summary>
        /// The response from the remote client. This is the raw string value without attempting to be
        /// converted to a DynValue. Only valid when Success == true.
        /// </summary>
        [ProtoMember(3)]
        public string Response { get; set; }

        /// <summary>
        /// The error, if any. This is only valid when Success == false.
        /// </summary>
        [ProtoMember(4)]
        public string Error { get; set; }

        /// <summary>
        /// Create a new response object
        /// </summary>
        public UBNetRemoteExecResponse() { }

        /// <summary>
        /// Create a new response object
        /// </summary>
        /// <param name="success"></param>
        /// <param name="response"></param>
        /// <param name="error"></param>
        public UBNetRemoteExecResponse(bool success, string response, string error) {
            Success = success;
            Response = response;
            Error = error;
        }
    }
}
