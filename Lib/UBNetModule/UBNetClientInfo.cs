﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Networking.Lib;
using UtilityBelt.Networking.Messages;

namespace UtilityBelt.Service.Lib.UBNetModule {

    /// <summary>
    /// Information about a client
    /// </summary>
    public class UBNetClientInfo {

        /// <summary>
        /// The id of the client. This is set by the server
        /// </summary>
        public uint Id { get; set; }

        /// <summary>
        /// The name of the client. This was set by the client itself and can be anything.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The type of client this is
        /// </summary>
        public ClientType Type { get; set; }

        /// <summary>
        /// Client tags. Used for grouping clients in order to broadcast to a subset of clients.
        /// </summary>
        public List<string> Tags { get; set; }

        /// <summary>
        /// Client properties. Custom string/value pairs
        /// </summary>
        public Dictionary<string, string> Properties { get; set; }

        internal UBNetClientInfo(uint id, string name, ClientType type, List<string> tags, Dictionary<string, string> properties) {
            Id = id;
            Name = name;
            Type = type;
            Tags = tags;
            Properties = properties;
        }

        public override string ToString() {
            return $"ClientInfo<{Id}:{Name}:{Type}>[{string.Join(", ", Tags)}]";
        }
    }
}
