﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Views;

namespace UtilityBelt.Service.Lib {
    public class HudAddedEventArgs : EventArgs {
        public Hud Hud { get; }

        public HudAddedEventArgs(Hud hud) {
            Hud = hud;
        }
    }
}
