﻿using ACE.DatLoader;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Scripting;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service.Lib.ScriptInterface;
using UtilityBelt.Service.Views;

namespace UtilityBelt.Service.Lib {
    internal class ScriptHost : IDisposable {
        public Scripting.ScriptManager Scripts { get; private set; }

        internal ScriptManagerView ScriptManagerView { get; private set; }

        internal ScriptSettings Settings => UBService.ScriptSettings;

        internal void Init() {
            var options = new ScriptManagerOptions() {
                ScriptDirectory = Settings.ScriptDirectory,
                StartVSCodeDebugServer = false,
                ClientCapabilities = ClientCapability.ImGui,
                ScriptDataDirectory = Settings.ScriptDataDirectory
            };

            var gameLogger = new GameLogger(Path.Combine(UBService.AssemblyDirectory, "ubscripts.logs.txt"), () => Settings.LogLevel.Value, () => Settings.PrintLevel.Value);

            Scripts = new UtilityBelt.Scripting.ScriptManager(options);

            Scripts.RegisterComponent(typeof(ILogger), typeof(GameLogger), true, gameLogger);
            Scripts.RegisterComponent(typeof(IClientActionsRaw), typeof(ACClientActions), true, new ACClientActions(gameLogger));
            Scripts.RegisterComponent(typeof(PortalDatDatabase), typeof(PortalDatDatabase), true, UBService.PortalDat);
            Scripts.RegisterComponent(typeof(CellDatDatabase), typeof(CellDatDatabase), true, UBService.CellDat);
            Scripts.RegisterComponent(typeof(LanguageDatDatabase), typeof(LanguageDatDatabase), true, UBService.LanguageDat);

            Scripts.OnDirectoryAccessRequest += Scripts_OnDirectoryAccessRequest;

            Scripts.Initialize((s) => {
                UBService.WriteLog(s, LogLevel.Debug);
            });
        }

        internal void TryInitScriptManagerUI() {
            if (Settings.Enable && UBService.didInit && ScriptManagerView == null) {
                ScriptManagerView = new ScriptManagerView();
            }
        }


        private void Scripts_OnDirectoryAccessRequest(object sender, Scripting.Events.DirectoryAccessRequestEventArgs e) {
            var body = $"{e.ScriptName} is requesting filesystem access to the following directory:";
            var directory = e.Directory;
            var rememberMyChoice = false;

            var access = UBService.ScriptSettings.GetScriptFileSystemAccess(e.ScriptName, e.Directory);
            if (access != null) {
                Timer.Once(TimeSpan.FromMilliseconds(1), () => {
                    if (access.Allowed) {
                        e.Callback(true, access.ProvidedPath);
                    }
                    else {
                        e.Callback(false, e.Directory);
                    }
                });
                return;
            }

            Settings.RemoveScriptFileSystemAccess(e.ScriptName, e.Directory);

            var popup = new PopupModal($"{e.ScriptName} is requesting FileSystem access.", body, new Dictionary<string, Action<string>>() {
                { "Allow Access", (res) => {
                    if (rememberMyChoice) {
                        Settings.SetScriptFileSystemAccess(e.ScriptName, e.Directory, directory, e.Reason, true);
                    }
                    e.Callback(true, directory);
                } },
                { "Deny Access", (res) => {
                    if (rememberMyChoice) {
                        Settings.SetScriptFileSystemAccess(e.ScriptName, e.Directory, e.Directory, e.Reason, false);
                    }
                    e.Callback(false, "NO_ACCESS");
                } }
            });

            popup.OnRender += (s, ev) => {
                ImGuiNET.ImGui.Spacing();
                ImGuiNET.ImGui.Spacing();
                ImGuiNET.ImGui.InputText("##input", ref directory, 2048);

                if (!string.IsNullOrEmpty(e.Reason)) {
                    ImGuiNET.ImGui.Spacing();
                    ImGuiNET.ImGui.Spacing();
                    ImGuiNET.ImGui.Text("The reason given by the script is:");
                    ImGuiNET.ImGui.Spacing();
                    ImGuiNET.ImGui.Spacing();
                    ImGuiNET.ImGui.TextColored(new System.Numerics.Vector4(1, 0, 0, 1), e.Reason);
                }

                ImGuiNET.ImGui.Spacing();
                ImGuiNET.ImGui.Spacing();
                ImGuiNET.ImGui.Text($"Do you want to allow access? You can customize the above directory to suit your needs.");

                ImGuiNET.ImGui.Spacing();
                ImGuiNET.ImGui.Spacing();
                ImGuiNET.ImGui.TextColored(new System.Numerics.Vector4(1, 0, 0, 1), "Only allow access to scripts you trust!");
                ImGuiNET.ImGui.Spacing();
                ImGuiNET.ImGui.Spacing();

                ImGuiNET.ImGui.Checkbox("Remember my choice", ref rememberMyChoice);
            };
        }


        public void Dispose() {
            
        }

    }
}

    