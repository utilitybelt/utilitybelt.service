- Switch logging over to ILogger
- Fix issue with dictionary settings serialization, when the key was an enum

Scripting Updates:

- Add ubnet module for communicating with other clients
- Fix bug where busy queue actions were not timing out
- Switch logging over to ILogger
- Fix bug where dictionaries with enum keys were not properly indexable
