- Add assertions to imgui, so it complains before crashing the client
- Fix some issues around hud disposal
- Add ImPlot and expose to lua
**Scripting Changes:**
 - Fix issue where adding an item to the salvage panel would clear the salvage panel beforehand