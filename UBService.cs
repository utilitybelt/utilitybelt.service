﻿using System;
using Decal.Interop.Core;
using System.Runtime.InteropServices;
using System.IO;
using UtilityBelt.Service.Views;
using UtilityBelt.Service.Lib;
using Decal.Adapter;
using UtilityBelt.Service.Lib.Settings;
using UtilityBelt.Service.Views.SettingsEditor;
using System.Reflection;
using System.Diagnostics;
using ACE.DatLoader;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Scripting;
using UtilityBelt.Scripting.Interop;
using UtilityBelt.Service.Lib.ScriptInterface;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text.RegularExpressions;
using ImGuiNET;
using WattleScript.Interpreter;
using System.Threading.Tasks;
using UtilityBelt.Networking;
using UtilityBelt.Networking.Lib;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;
using Microsoft.Extensions.Logging;
using UtilityBelt.Common.Enums;
using ACE.DatLoader.FileTypes;
using System.Text;

namespace UtilityBelt.Service {
    /// <summary>
    /// UB Service
    /// </summary>
    [ClassInterface(ClassInterfaceType.None)]
    [Guid("8adc5729-db1a-4e28-9475-c4eafae1e6e7")]
    [ProgId("UBService")]
    [ComVisible(true)]
    [ComDefaultInterface(typeof(IDecalService))]
    public sealed class UBService : MarshalByRefObject, IDecalService, IDecalRender, IDecalWindowsMessageSink {
        internal static DecalCore iDecal;
        internal static bool didInit = false;
        internal static Settings Settings { get; set; }
        public static GameLogger Logger { get; private set; }
        internal static Settings ViewsSettings { get; set; }
        internal static Settings CharacterSettings { get; set; }
        internal static Settings AccountSettings { get; set; }
        public static string AssemblyDirectory => Path.GetDirectoryName(Assembly.GetAssembly(typeof(UBService)).Location);
        public static string SettingsDirectory => Path.Combine(AssemblyDirectory, "settings");

        public static bool IsInGame { get; internal set; }
        public static CellDatDatabase CellDat { get; private set; }
        public static PortalDatDatabase PortalDat { get; private set; }
        public static LanguageDatDatabase LanguageDat { get; private set; }

        public static ScriptManager Scripts => ScriptHost?.Scripts;

        public static event EventHandler<EventArgs> OnTick;

        internal static ScriptHost ScriptHost;

        public static UBNet UBNet;

        #region Service Settings
        public class ServiceSettings : ISetting {
            [Summary("Prevent client idle timeout")]
            public Setting<bool> PreventIdleTimeout = new Global<bool>(true);

            [Summary("Log Level")]
            public Setting<LogLevel> LogLevel = new Global<LogLevel>(Microsoft.Extensions.Logging.LogLevel.Error);

            [Summary("Print Level (This is the level of logging to print to chat)")]
            public Setting<LogLevel> PrintLevel = new Global<LogLevel>(Microsoft.Extensions.Logging.LogLevel.Warning);
        }
        public static ServiceSettings Service = new ServiceSettings();
        #endregion // Service Settings

        public static HudManager Huds = null;

        public static ScriptSettings ScriptSettings = new ScriptSettings();

        private bool _hasServices = false;
        private bool _hasFilters = false;
        private bool needsUserScriptLoad;
        internal static readonly long MAX_LOG_SIZE = 1024 * 1024 * 10; // 10MB

        /// <summary>
        /// Returns true if the client currently has focus.
        /// </summary>
        unsafe public static bool ClientIsFocused => *(byte*)0x00838197 == 1; // Device__m_bIsActiveApp

        /// <summary>
        /// Returns true if the mouse is currently over the client (regardless of focus)
        /// </summary>
        unsafe public static bool MouseIsOverClient => *(byte*)0x0083819A == 1; // Device__m_bTrackLeaveCalled

        async void IDecalService.Initialize(DecalCore pDecal) {
            try {
                iDecal = pDecal;

                AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(CurrentDomain_AssemblyResolve);

                if (File.Exists(System.IO.Path.Combine(AssemblyDirectory, @"ubservice.exceptions.txt"))) {
                    File.Delete(System.IO.Path.Combine(AssemblyDirectory, @"ubservice.exceptions.txt"));
                }

                Huds = new HudManager();

                if (!Directory.Exists(Huds.profilesDir))
                    Directory.CreateDirectory(Huds.profilesDir);
                if (!Directory.Exists(SettingsDirectory))
                    Directory.CreateDirectory(SettingsDirectory);

                UBNet = new UBNet(new GameLogger(Path.Combine(AssemblyDirectory, "ubnet.logs.txt"), () => UBNet.LogLevel.Value, () => UBNet.PrintLevel.Value));

                Settings = new Settings(this, System.IO.Path.Combine(SettingsDirectory, "ubservice.settings.json"), (t) => {
                    return (t.SettingType == SettingType.Global);
                });
                Settings.Load();

                Logger = new GameLogger(Path.Combine(AssemblyDirectory, "ubservice.logs.txt"), () => Service.LogLevel.Value, () => Service.PrintLevel.Value);
                WriteLog($"IDecalService.Initialize", LogLevel.Debug);

                ViewsSettings = new Settings(this, Huds.CurrentProfilePath, (t) => {
                    return (t.SettingType == SettingType.Views);
                });
                ViewsSettings.Load();

                Huds.Profile.Changed += Profile_Changed;

                UBHelper.Core.FilterStartup(AssemblyDirectory, AssemblyDirectory);

                Lib.ACClientModule.ACClientModuleInternal.Init();

                iDecal.InitializeComplete += IDecal_InitializeComplete;

                LoadDats();
            }
            catch (Exception ex) { LogException(ex); }
        }

        private Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args) {
            var name = new AssemblyName(args.Name);
            if (name.Name == "System.Runtime.CompilerServices.Unsafe") {
                return typeof(System.Runtime.CompilerServices.Unsafe).Assembly;
            }
            return null;
        }

        private async Task LoadDats() {
            //await Task.Run(() => {
            var datDirectory = "";
            var cellDatPath = System.IO.Path.Combine(datDirectory, "client_cell_1.dat");
            var portalDatPath = System.IO.Path.Combine(datDirectory, "client_portal.dat");
            var languageDatPath = System.IO.Path.Combine(datDirectory, "client_local_English.dat");

            if (!System.IO.File.Exists(cellDatPath)) {
                WriteLog($"Unable to load cellDat: {cellDatPath}", LogLevel.Error);
                return;
            }
            if (!System.IO.File.Exists(portalDatPath)) {
                WriteLog($"Unable to load portalDat: {portalDatPath}", LogLevel.Error);
                return;
            }
            if (!System.IO.File.Exists(languageDatPath)) {
                WriteLog($"Unable to load languageDat: {portalDatPath}", LogLevel.Error);
                return;
            }

            CellDat = new CellDatDatabase(cellDatPath, true);
            PortalDat = new PortalDatDatabase(portalDatPath, true);
            LanguageDat = new LanguageDatDatabase(languageDatPath, true);
            //});
        }

        private void IDecal_InitializeComplete(eDecalComponentType Type) {
            try {
                WriteLog($"IDecal_InitializeComplete {Type}", LogLevel.Debug);
                if (Type == eDecalComponentType.eNetworkFilter) {
                    _hasFilters = true;
                }
                if (Type == eDecalComponentType.eService) {
                    _hasServices = true;
                }
                if (_hasServices && _hasFilters) {
                    iDecal.InitializeComplete -= IDecal_InitializeComplete;
                    WriteLog($"Decal Services/Filters ready", LogLevel.Debug);
                    CoreManager.Current.EchoFilter.ClientDispatch += EchoFilter_ClientDispatch;
                    CoreManager.Current.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
                    CoreManager.Current.MessageProcessed += Current_MessageProcessed;

                    if (ScriptSettings.Enable) {
                        ScriptHost = new ScriptHost();
                        ScriptHost.Init();
                        ScriptSettings.Init();
                    }
                }

            }
            catch (Exception ex) { LogException(ex); }
        }

        private void Current_MessageProcessed(object sender, MessageProcessedEventArgs e) {
            try {
                if (ScriptSettings.Enable) {
                    Scripts.HandleIncoming(e.Message.RawData);
                }
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }

        // CoreManager.Current.EchoFilter.ServerDispatch += EchoFilter_ServerDispatch;
        private void EchoFilter_ServerDispatch(object sender, NetworkMessageEventArgs e) {
            try {
                if (e.Message.Type == 0xF7E1) {
                    var serverName = e.Message.Value<string>("server");
                    if (AccountSettings == null) {
                        if (!Directory.Exists(Path.Combine(SettingsDirectory, serverName)))
                            Directory.CreateDirectory(Path.Combine(SettingsDirectory, serverName));

                        var accountSettingsPath = Path.Combine(SettingsDirectory, serverName, $"__{UBHelper.Core.UserName.Split(':').First()}.json");
                        AccountSettings = new Settings(this, accountSettingsPath, (t) => {
                            return (t.SettingType == SettingType.Account);
                        });
                        AccountSettings.Load();

                        UBNet.Init($"{UBHelper.Core.UserName.Split(':').First()}");

                        ScriptSettings.StartScripts(ScriptSettings.ScriptRunType.Global);
                        ScriptSettings.StartScripts(ScriptSettings.ScriptRunType.Account);
                    }
                }
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }

        private void EchoFilter_ClientDispatch(object sender, NetworkMessageEventArgs e) {
            if (ScriptSettings.Enable) {
                try {
                    Scripts.HandleOutgoing(e.Message.RawData);
                }
                catch (Exception ex) {
                    LogException(ex);
                }
            }
        }

        private void Profile_Changed(object sender, SettingChangedEventArgs e) {
            try {
                ViewsSettings.SettingsPath = Huds.CurrentProfilePath;
            }
            catch (Exception ex) { LogException(ex); }
        }

        /// <summary>
        /// Handle window messages
        /// </summary>
        /// <param name="HWND"></param>
        /// <param name="uMsg"></param>
        /// <param name="wParam"></param>
        /// <param name="lParam"></param>
        /// <returns></returns>
        unsafe public bool WindowMessage(int HWND, short uMsg, int wParam, int lParam) {
            var eat = false;
            try {
                if (uMsg == 0x20/*setcursor*/ || uMsg == 0x84/*WM_NCHITTEST*/) {
                    return eat;
                }

                if (ScriptHost?.Scripts?.GameState != null) {
                    eat = ScriptHost.Scripts.GameState.HandleWindowMessage(HWND, uMsg, wParam, lParam);
                }
                if (!eat) {
                    eat = Huds.WindowMessage(HWND, uMsg, wParam, lParam);
                }
            }
            catch (Exception ex) { LogException(ex); }
            return eat;
        }

        void IDecalService.BeforePlugins() {
            try {
                WriteLog($"IDecalService.BeforePlugins", LogLevel.Debug);
                CoreManager.Current.CharacterFilter.Login += CharacterFilter_Login;
                CoreManager.Current.ChatBoxMessage += Current_ChatBoxMessage;
                CoreManager.Current.CommandLineText += Current_CommandLineText;
                CoreManager.Current.ChatNameClicked += Current_ChatNameClicked;
                CoreManager.Current.ItemSelected += Current_ItemSelected;

                if (Service.PreventIdleTimeout) {
                    CoreManager.Current.Actions.SetIdleTime(double.MaxValue);
                }
                ScriptSettings.TryCreateInspector();
            }
            catch (Exception ex) { LogException(ex); }
        }

        void IDecalService.AfterPlugins() {
            try {
                WriteLog($"IDecalService.AfterPlugins", LogLevel.Debug);
                ScriptSettings.StopScripts(ScriptSettings.ScriptRunType.Character);
                ScriptHost.Scripts.HandlePluginsUnloaded();
                CoreManager.Current.CharacterFilter.Login -= CharacterFilter_Login;
                CoreManager.Current.ChatBoxMessage -= Current_ChatBoxMessage;
                CoreManager.Current.CommandLineText -= Current_CommandLineText;
                CoreManager.Current.ChatNameClicked -= Current_ChatNameClicked;
                CoreManager.Current.ItemSelected -= Current_ItemSelected;
                IsInGame = false;
                ViewsSettings.SettingsPath = Huds.CurrentProfilePath;
                if (CharacterSettings != null && CharacterSettings.NeedsSave)
                    CharacterSettings.Save();
                CharacterSettings = null;

                UBNet.UBNetClient.SetName(UBHelper.Core.UserName.Split(':').First());
                UBNet.UBNetClient.RemoveProperties(new List<string>() {
                    "LoggedIn",
                    "CharacterName",
                    "PlayerId"
                });
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }

        private void Current_ItemSelected(object sender, ItemSelectedEventArgs e) {
            try {
                Scripts?.GameState?.WorldState?.HandleObjectSelected((uint)e.ItemGuid);
            }
            catch (Exception ex) { LogException(ex); }
        }

        private void Current_ChatNameClicked(object sender, ChatClickInterceptEventArgs e) {
            try {
                if (Scripts?.GameState?.WorldState?.HandleChatNameClicked((uint)e.Id, e.Text) == true) {
                    e.Eat = true;
                }
            }
            catch (Exception ex) { LogException(ex); }
        }

        private readonly Regex slashUBS = new Regex(@"^[/@]+ubs( |$)");
        private void Current_CommandLineText(object sender, ChatParserInterceptEventArgs e) {
            try {
                if (slashUBS.IsMatch(e.Text)) {
                    e.Eat = true;
                    var logger = Scripts.Resolve<ILogger>();

                    var parts = e.Text.Split(' ');
                    var verb = parts.Length > 1 ? parts[1] : "";
                    var args = parts.Length > 2 ? string.Join(" ", parts.Skip(2)) : "";

                    switch (verb) {
                        case "lexec":
                            if (string.IsNullOrEmpty(args)) {
                                Logger?.LogToChat("Usage: /ubs lexec <lua code>", LogLevel.Error);
                                return;
                            }
                            var watchLexec = new System.Diagnostics.Stopwatch();
                            Logger?.LogToChat($"Evaluating script (context `Global`): \"{args}\"");
                            watchLexec.Start();
                            DoAsyncScript(async () => {
                                var lexecRes = await Scripts.GlobalScriptContext.RunTextAsync(args);
                                watchLexec.Stop();
                                Logger?.LogToChat($"Result: {PrettyPrint(lexecRes)} ({1000.0 * (double)watchLexec.ElapsedTicks / Stopwatch.Frequency:N3}ms)");
                            });
                            break;

                        case "lexecs":
                            if (parts.Length < 4) {
                                Logger?.LogToChat("Usage: /ubs lexecs <scriptname> <lua code>", LogLevel.Error);
                                return;
                            }
                            var name = parts[2];
                            var script = Scripts.GetScript(name);
                            if (script == null) {
                                var loadedScripts = string.Join(", ", Scripts.GetAll().Select(s => s.Name).ToArray());
                                Logger?.LogToChat($"Unable to find script named `{name}`. Loaded scripts: {loadedScripts}", LogLevel.Error);
                                return;
                            }
                            var luaCode = string.Join(" ", parts.Skip(3));
                            var watchLexecs = new System.Diagnostics.Stopwatch();
                            Logger?.LogToChat($"Evaluating script (context `{name}`): \"{luaCode}\"");
                            watchLexecs.Start();
                            DoAsyncScript(async () => {
                                var res = await script.RunTextAsync(luaCode);
                                watchLexecs.Stop();
                                Logger?.LogToChat($"Result: [{(res.IsNil() ? "null" : res.GetType().ToString())}] {res} ({Math.Round(watchLexecs.ElapsedTicks / 10000.0, 3)}ms)");
                            });
                            break;

                        case "script":
                            if (parts.Length < 4) {
                                Logger?.LogToChat("Usage: /ubs script <start|stop> <scriptname>", LogLevel.Error);
                                return;
                            }
                            var command = parts[2];
                            var scriptname = parts[3];
                            switch (command.ToLower()) {
                                case "start":
                                    Scripts.StartScript(scriptname);
                                    break;
                                case "stop":
                                    Scripts.StopScript(scriptname);
                                    break;
                            }
                            break;
                    }

                }

                if (Scripts?.GameState?.WorldState?.HandleChatInputText(e.Text) == true) {
                    e.Eat = true;
                }
            }
            catch (Exception ex) { LogException(ex); }
        }

        private void DoAsyncScript(Func<Task> value) {
            value();
        }

        private string PrettyPrint(object lexecRes) {
            if (lexecRes == null)
                return "null";
            return $"({lexecRes.GetType().Name}) {lexecRes}";
        }

        private void Current_ChatBoxMessage(object sender, ChatTextInterceptEventArgs e) {
            try {
                if (Scripts?.GameState?.WorldState?.EatNextChatMessage == true) {
                    e.Eat = true;
                    Scripts.GameState.WorldState.EatNextChatMessage = false;
                }
            }
            catch (Exception ex) { LogException(ex); }
        }

        private void CharacterFilter_Login(object sender, Decal.Adapter.Wrappers.LoginEventArgs e) {
            try {
                WriteLog($"CharacterFilter_Login", LogLevel.Debug);
                IsInGame = true;

                var charSettingsPath = Path.Combine(SettingsDirectory, UBHelper.Core.WorldName, $"__{CoreManager.Current.CharacterFilter.AccountName}_{CoreManager.Current.CharacterFilter.Name}.json");
                CharacterSettings = new Settings(this, charSettingsPath, (t) => {
                    return (t.SettingType == SettingType.CharacterSettings);
                });
                CharacterSettings.Load();

                ViewsSettings.SettingsPath = Huds.CurrentProfilePath;
                CoreManager.Current.CharacterFilter.Login -= CharacterFilter_Login;

                UBHelper.Core.Startup(CoreManager.Current.CharacterFilter.Name);

                UBNet.UBNetClient.SetName($"{CoreManager.Current.CharacterFilter.Server}/{CoreManager.Current.CharacterFilter.Name}");
                UBNet.UBNetClient.SetProperties(new Dictionary<string, string>() {
                    { "LoggedIn", "true" },
                    { "CharacterName", CoreManager.Current.CharacterFilter.Name },
                    { "PlayerId", CoreManager.Current.CharacterFilter.Id.ToString() }
                });

                needsUserScriptLoad = true;

                CoreManager.Current.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
            }
            catch (Exception ex) { LogException(ex); }
        }

        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            try {
                CoreManager.Current.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
            }
            catch (Exception ex) { LogException(ex); }
        }

        void IDecalService.Terminate() {
            try {
                WriteLog($"IDecalService.Terminate", LogLevel.Debug);
                ScriptSettings.StopScripts(ScriptSettings.ScriptRunType.Account);
                ScriptSettings.StopScripts(ScriptSettings.ScriptRunType.Global);

                ScriptHost.ScriptManagerView?.Dispose();

                Huds.Profile.Changed -= Profile_Changed;
                if (AccountSettings != null && AccountSettings.NeedsSave)
                    AccountSettings.Save();
                if (Settings != null && Settings.NeedsSave)
                    Settings.Save();
                if (ViewsSettings != null && ViewsSettings.NeedsSave)
                    ViewsSettings.Save();

                Scripts?.Dispose();
                Lib.ACClientModule.ACClientModuleInternal.Dispose();

                UBNet?.Shutdown();
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }


#pragma warning disable 1591
        public unsafe void ChangeDirectX() {
            try {
                WriteLog($"ChangeDirectX", LogLevel.Debug);
                if (!didInit) {
                    Huds.Init();
                    didInit = true;
                    ScriptHost.TryInitScriptManagerUI();
                }
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }

#pragma warning disable 1591
        public void ChangeHWND() {
            try {
                WriteLog($"ChangeHWND", LogLevel.Debug);
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }

#pragma warning disable 1591
        public void PostReset() {
            try {
                WriteLog($"PostReset", LogLevel.Debug);
                Huds.PostReset();
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }

#pragma warning disable 1591
        public void PreReset() {
            try {
                WriteLog($"PreReset", LogLevel.Debug);
                Huds.PreReset();
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }

#pragma warning disable 1591
        public void Render2D() {
            try {
                if (needsUserScriptLoad) {
                    needsUserScriptLoad = false;

                    ScriptHost.Scripts.HandlePluginsLoaded();

                    ScriptSettings.StartScripts(ScriptSettings.ScriptRunType.Character);
                }

                Scripts?.Tick();
                Scripts?.Render2D();
                Huds.DoRender();
                OnTick?.InvokeSafely(this, EventArgs.Empty);

                if (Settings?.NeedsLoad == true) {
                    TryLoadSettings(Settings);
                }
                if (AccountSettings?.NeedsLoad == true) {
                    TryLoadSettings(AccountSettings);
                }
                if (ViewsSettings?.NeedsLoad == true) {
                    TryLoadSettings(ViewsSettings);
                }
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }

        private void TryLoadSettings(Settings settings) {
            try {
                settings.DisableSaving();
                settings.LoadSettings();
            }
            catch (Exception ex) {
                UBService.LogException(ex);
            }
            finally {
                settings.EnableSaving();
            }
        }

#pragma warning disable 1591
        public void Render3D() {
            try {
                var timers = Timer.RunningTimers.ToArray();
                foreach (var timer in timers) {
                    timer.TryTick();
                }
                Scripts?.Render3D();
            }
            catch (Exception ex) {
                LogException(ex);
            }
        }

        public static void LogException(Exception ex) {
            WriteLog(ex.ToString(), LogLevel.Error);
        }

        public static void WriteLog(string text, LogLevel level) {
            try {
                Logger?.Log(text, level);
            }
            catch { }
        }
    }
}
