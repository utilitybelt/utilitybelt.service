﻿using AcClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;

namespace UtilityBelt.Service.Views {


    [StructLayout(LayoutKind.Sequential)]
    public struct DragDropPayload {
        public uint SpellId;
        public uint ItemId;
        public DropItemFlags Flags;

        public ManagedTexture? MakeIcon() {
            var game = UBService.ScriptHost.Scripts.GlobalScriptContext.Context.GetGame();
            if (ItemId != 0) {
                if (game?.World?.TryGet(ItemId, out var wo) == true) {
                    return new ManagedTexture(wo.Value(DataId.Icon));
                }
            }

            if (game?.Character?.SpellBook?.TryGet(SpellId, out var spell) == true) {
                return new ManagedTexture(spell.Icon);
            }

            return null;
        }

        public unsafe override string ToString() {
            var game = UBService.ScriptHost.Scripts.GlobalScriptContext.Context.GetGame();
            if (ItemId != 0) {
                if (game?.World?.TryGet(ItemId, out var wo) == true) {
                    return wo.Name;
                }

                return $"Item: 0x{ItemId:X8}";
            }

            if (game?.Character?.SpellBook?.TryGet(SpellId, out var spell) == true) {
                return $"{spell.Name}";
            }

            return $"Spell: 0x{SpellId:X8}";
        }
    }
}
