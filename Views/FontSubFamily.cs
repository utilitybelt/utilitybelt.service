﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityBelt.Service.Views {
    public enum FontSubFamily {
        Regular,
        Italic,
        Bold,
        BoldItalic
    }
}
