﻿using ImGuiNET;
using Microsoft.DirectX.Direct3D;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using UtilityBelt.Service.Lib;

namespace UtilityBelt.Service.Views {
    /// <summary>
    /// Window hud
    /// </summary>
    public class Hud : IDisposable {
        private bool windowIsOpen = false;
        internal Texture iconTexture = null;
        internal Bitmap iconBitmap;
        private bool _lastVisible = false;
        private bool _needsWindowPositionUpdate = false;
        internal bool isDisposed = false;

        internal event EventHandler OnDisposed;

        /// <summary>
        /// Fired before the device is reset, you should unload any textures you have, and recreate them
        /// inside the CreateTextures event.
        /// </summary>
        public event EventHandler OnDestroyTextures;

        /// <summary>
        /// Fired after the device is reset. You should recreate your textures here.
        /// </summary>
        public event EventHandler OnCreateTextures;

        /// <summary>
        /// Use this to set state for the next window, like with `ImGui::SetNextWindowPos`
        /// </summary>
        public event EventHandler OnPreRender;

        /// <summary>
        /// When this is raised, redraw your hud
        /// </summary>
        public event EventHandler OnRender;

        /// <summary>
        /// Called after Rendering the imgui window
        /// </summary>
        public event EventHandler OnPostRender;

        /// <summary>
        /// When this is raised, show your hud if you are managing your own windows
        /// </summary>
        public event EventHandler OnShow;

        /// <summary>
        /// When this is raised, hide your hud if you are managing your own windows
        /// </summary>
        public event EventHandler OnHide;

        /// <summary>
        /// Wether to show this hud in the bar
        /// </summary>
        public bool ShowInBar { get; set; } = true;

        /// <summary>
        /// If this is true, you manage showing/hiding/drawing your own window. Use ShouldShow / ShouldHide events
        /// </summary>
        public bool DontDrawDefaultWindow { get; set; } = false;

        /// <summary>
        /// Wether the window is visible or not.  If CustomWindowDrawing is true, this only reflects what the bar thinks.
        /// It's up to you to use ShouldShow/ShouldHide to manage its actual state.
        /// </summary>
        public bool Visible {
            get => windowIsOpen;
            set {
                try {
                    if (_lastVisible != value) {
                        _lastVisible = value;
                        windowIsOpen = value;
                        if (windowIsOpen) {
                            OnShow?.Invoke(this, EventArgs.Empty);
                        }
                        else {
                            OnHide?.Invoke(this, EventArgs.Empty);
                        }
                    }
                }
                finally { }
            }
        }

        /// <summary>
        /// Window settings flags
        /// </summary>
        public ImGuiWindowFlags WindowSettings { get; set; } = ImGuiWindowFlags.None;

        /// <summary>
        /// Name of the hud. This should be unique to your window. Two windows with the same name will share
        /// state.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Window title if not using CustomWindowDrawing
        /// </summary>
        public string Title { get; set; }

        private uint _windowX = 0;
        private uint _windowY = 0;
        public uint WindowX {
            get => _windowX;
            set {
                if (value != _windowX) {
                    _windowX = value;
                    _needsWindowPositionUpdate = true;
                }
            }
        }
        public uint WindowY { get; set; }
        public ImGuiViewportPtr WindowViewport { get; private set; }

        public Vector2 WindowPositionRelative {
            get {
                var isMainViewport = (WindowViewport.ID == ImGui.GetWindowViewport().ID);
                var x = isMainViewport ? WindowX : WindowViewport.Pos.X;
                return new Vector2(WindowX, WindowY);
            }
        }

        /// <summary>
        /// The time it took to run the most recent PreRender event for this hud, in milliseconds.
        /// </summary>
        public double LastPreRenderTimeMS { get; private set; }

        /// <summary>
        /// The time it took to run the most recent Render event for this hud, in milliseconds.
        /// </summary>
        public double LastRenderTimeMS { get; private set; }

        /// <summary>
        /// The time it took to run the most recent PostRender event for this hud, in milliseconds.
        /// </summary>
        public double LastPostRenderTimeMS { get; private set; }

        internal Hud() {
            
        }

        internal Hud(string name, Bitmap icon) {
            UBService.WriteLog($"Hud: {Name}: Create", LogLevel.Debug);
            if (icon != null) {
                iconBitmap = new Bitmap(icon);
            }
            Name = name;
            Title = name;
#if NET35_OR_GREATER
            if (iconTexture == null && iconBitmap != null) {
                iconTexture = new Texture(UBService.Huds.D3Ddevice, iconBitmap, Usage.Dynamic, Pool.Default);
            }
#endif
        }

        internal void CallRender() {
            bool startedWindow = false;
            bool isCollapsed = false;
            try {
                Visible = windowIsOpen;
                LastPreRenderTimeMS = 0;
                LastRenderTimeMS = 0;

                if (!windowIsOpen)
                    return;

                //if (!DontDrawDefaultWindow && Visible && _needsWindowPositionUpdate) {
                //    ImGui.SetNextWindowPos(WindowPositionRelative);
                //    _needsWindowPositionUpdate = false;
                //}

                var sw = new System.Diagnostics.Stopwatch();
                sw.Start();
                OnPreRender?.Invoke(this, EventArgs.Empty);
                sw.Stop();
                LastPreRenderTimeMS = ((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0;
                sw.Restart();

                sw.Start();
                if (!DontDrawDefaultWindow && Visible) {
                    startedWindow = true;
                    if (ImGui.Begin($"{Title}###{Name}", ref windowIsOpen, WindowSettings)) {
                        // turns out exiting early can mess with hud logic that is inside Render event,
                        // so we call Render event if the window is collapsed. NonVisible windows do
                        // not get Render events called, since they shouldn't be doing anything.
                        // return; // early exit if window is collapsed
                        OnRender?.Invoke(this, EventArgs.Empty);
                    }
                    else {
                        isCollapsed = true;
                    }
                }
                else if (DontDrawDefaultWindow && Visible) {
                    OnRender?.Invoke(this, EventArgs.Empty);
                }
                sw.Stop();
                LastRenderTimeMS = ((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0;
                sw.Restart();

                sw.Start();
                OnPostRender?.Invoke(this, EventArgs.Empty);
                sw.Stop();
                LastPostRenderTimeMS = ((double)sw.ElapsedTicks / Stopwatch.Frequency) * 1000.0;
            }
            catch (Exception ex) { UBService.LogException(ex); }
            finally {
                if (startedWindow) {
                    ImGui.End();
                    WindowX = (uint)ImGui.GetWindowPos().X;
                    WindowY = (uint)ImGui.GetWindowPos().Y;
                    WindowViewport = ImGui.GetWindowViewport();
                }
            }
        }

        internal void CallDestroyTextures() {
            UBService.WriteLog($"Hud: {Name}: CallDestroyTextures", LogLevel.Debug);
            // need to recreate textures when the device is reset
            if (iconTexture != null) {
                iconTexture.Dispose();
                iconTexture = null;
            }
            OnDestroyTextures?.Invoke(this, EventArgs.Empty);
        }

        internal void CallCreateTextures() {
            UBService.WriteLog($"Hud: {Name}: CallCreateTextures", LogLevel.Debug);
#if NET35_OR_GREATER
            if (iconTexture == null && iconBitmap != null) {
                try {
                    iconTexture = new Texture(UBService.Huds.D3Ddevice, iconBitmap, Usage.Dynamic, Pool.Default);
                }
                catch (Exception ex) { UBService.LogException(ex); }
            }
#endif
            OnCreateTextures?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Destroy the hud
        /// </summary>
        public void Dispose() {
            UBService.WriteLog($"Hud: {Name}: Dispose", LogLevel.Debug);
            if (isDisposed)
                return;
            try {
                OnDisposed?.Invoke(this, EventArgs.Empty);
                iconTexture?.Dispose();
                iconBitmap?.Dispose();
                isDisposed = true;
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }
    }
}
