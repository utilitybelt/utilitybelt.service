﻿using Decal.Adapter.Wrappers;
using ImGuiNET;
using WattleScript.Interpreter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Lib;
using UtilityBelt.Scripting.Enums;
using UtilityBelt.Service.Lib;
using Microsoft.Extensions.Logging;

namespace UtilityBelt.Service.Views {
    internal class ScriptManagerView : IDisposable {
        private Hud hud;
        private UtilityBelt.Scripting.UBScript currentScriptInstance;
        private string _consoleInput = "";
        private List<string> _scripts = new List<string>();
        private string selectedScript = "";

        private ManagedTexture deleteIcon = null;
        private ManagedTexture editIcon = null;

        private List<string> suggestions = new List<string>();
        private bool _showSuggestions = false;
        private Vector2 intellisensePos = new Vector2(0,0);
        private string currentConsoleInput = "";

        private bool useMultiLineInput = false;

        private int lastEditorCursorPos = 0;
        private int selectedIndex = -1;
        private string setNext = null;

        private bool showSuggestions {
            get => _showSuggestions;
            set {
                _showSuggestions = value;
                //if (intellisensePopup != null)
                //    intellisensePopup.Visible = value;
            }
        }

        private Dictionary<string, FileSystemWatcher> _scriptWatchers = new Dictionary<string, FileSystemWatcher>();
        private List<string> _reloadingScripts = new List<string>();
        //private Dictionary<string, Inspector> _scriptInspector = new Dictionary<string, Inspector>();

        public bool ScrollToBottom { get; private set; }

        private class InputHistory {
            public List<string> History { get; } = new List<string>();
            public int HistoryIndex = 0;
        }
        private Dictionary<string, InputHistory> _inputHistories = new Dictionary<string, InputHistory>();

        public ScriptManagerView() {
            if (!UBService.ScriptSettings.Enable)
                return;

            Bitmap bmp = null;
            using (Stream manifestResourceStream = GetType().Assembly.GetManifestResourceStream("UtilityBelt.Service.Resources.icons.scriptsmanager.png")) {
                bmp = new Bitmap(manifestResourceStream);
            }


            using (Stream manifestResourceStream = GetType().Assembly.GetManifestResourceStream("UtilityBelt.Service.Resources.icons.delete.png")) {
                using (var dbmp = new Bitmap(manifestResourceStream)) {
                    deleteIcon = new ManagedTexture(dbmp);
                }
            }


            using (Stream manifestResourceStream = GetType().Assembly.GetManifestResourceStream("UtilityBelt.Service.Resources.icons.edit.png")) {
                using (var ebmp = new Bitmap(manifestResourceStream)) {
                    editIcon = new ManagedTexture(ebmp);
                }
            }

            hud = UtilityBelt.Service.UBService.Huds.CreateHud("UtilityBelt Scripts Manager", bmp);

            hud.ShowInBar = true;
            hud.OnRender += Hud_Render;
            hud.OnPreRender += Hud_PreRender;

            UBService.Scripts.OnScriptStarted += Scripts_OnScriptStarted;
            UBService.Scripts.OnScriptStopped += Scripts_OnScriptStopped;
        }

        private void Scripts_OnScriptStopped(object sender, Scripting.Events.ScriptEventArgs e) {
            
        }

        private void Scripts_OnScriptStarted(object sender, Scripting.Events.ScriptEventArgs e) {
            if (UBService.ScriptHost.Settings.ReloadScriptsOnFileChanges) {
                WatchScriptForChanges(e.Script.Name);
            }
        }

        private void WatchScriptForChanges(string scriptName) {
            var scriptPath = UBService.Scripts.GetScriptDirectory(scriptName);
            if (!string.IsNullOrEmpty(scriptPath) && !_scriptWatchers.ContainsKey(scriptName)) {
                var watcher = new FileSystemWatcher();
                watcher.Path = scriptPath;
                watcher.IncludeSubdirectories = true;
                watcher.Filter = "*.*";
                watcher.Changed += (s, e) => {
                    if (_reloadingScripts.Contains(scriptName)) {
                        return;
                    }

                    _reloadingScripts.Add(scriptName);
                    UBService.WriteLog($"Script files changed, restarting: {scriptName}", LogLevel.Debug);
                    Timer.Once(TimeSpan.FromMilliseconds(100), () => {
                        _reloadingScripts.Remove(scriptName);
                        var runningScript = UBService.Scripts.GetScript(scriptName);
                        if (runningScript != null) {
                            UBService.Scripts.RestartScript(scriptName);
                        }
                    });
                };
                watcher.EnableRaisingEvents = true;

                UBService.WriteLog($"Adding fs watcher for: {scriptPath}", LogLevel.Debug);

                _scriptWatchers.Add(scriptName, watcher);
            }
        }

        private void StopWatchingScriptForChanges(string scriptName) {
            if (_scriptWatchers.TryGetValue(scriptName, out var watcher)) {
                watcher.Dispose();
                _scriptWatchers.Remove(scriptName);
            }
        }

        private void Hud_PreRender(object sender, EventArgs e) {
            ImGui.SetNextWindowSizeConstraints(new Vector2(500, 250), new Vector2(float.MaxValue, float.MaxValue));
            ImGui.SetNextWindowSize(new Vector2(600, 300), ImGuiCond.FirstUseEver);
        }

        unsafe private void Hud_Render(object sender, EventArgs e) {
            var pad = 15;
            ImGui.BeginTable("ScriptsTable", 2, ImGuiTableFlags.Resizable | ImGuiTableFlags.BordersInnerV | ImGuiTableFlags.NoSavedSettings);
            {
                ImGui.TableSetupColumn("ObjectTree", ImGuiTableColumnFlags.WidthFixed, 200);
                ImGui.TableNextRow();
                ImGui.TableSetColumnIndex(0);
                ImGui.BeginChild("Object Tree", new Vector2(-1, ImGui.GetContentRegionAvail().Y - 4));
                {
                    var listedScripts = new List<string>();
                    var runningScripts = UtilityBelt.Service.UBService.Scripts.GetAll();

                    if (ImGui.TreeNodeEx("Running Scripts", runningScripts.Count() > 0 ? ImGuiTreeNodeFlags.DefaultOpen : ImGuiTreeNodeFlags.Leaf)) {
                        foreach (var script in runningScripts) {
                            var flags = ImGuiTreeNodeFlags.Leaf;
                            if (selectedScript.ToLower().Equals(script.Name.ToLower())) {
                                flags |= ImGuiTreeNodeFlags.Selected;
                            }

                            ImGui.TreeNodeEx(script.Name, flags);
                            if (ImGui.IsItemClicked()) {
                                selectedScript = script.Name;
                                SelectScript(script.Name);
                            }
                            ImGui.TreePop();
                            listedScripts.Add(script.Name.ToLower());
                        }
                        ImGui.TreePop(); // Running Scripts
                    }

                    var availableScripts = UtilityBelt.Service.UBService.Scripts.GetAvailable().Where(s => !listedScripts.Contains(s.ToLower()));
                    if (ImGui.TreeNodeEx("Available Scripts", availableScripts.Count() > 0 ? ImGuiTreeNodeFlags.DefaultOpen : ImGuiTreeNodeFlags.Leaf)) {
                        foreach (var script in availableScripts) {
                            if (listedScripts.Contains(script))
                                continue;
                            var flags = ImGuiTreeNodeFlags.Leaf;
                            if (selectedScript.ToLower().Equals(script.ToLower())) {
                                flags |= ImGuiTreeNodeFlags.Selected;
                            }
                            ImGui.TreeNodeEx(script, flags);
                            if (ImGui.IsItemClicked()) {
                                selectedScript = script;
                                SelectScript(script);
                            }
                            ImGui.TreePop();

                            listedScripts.Add(script.ToLower());
                        }
                        ImGui.TreePop(); // Available Scripts
                    }
                }
                ImGui.EndChild(); // object tree

                ImGui.Indent(10);
                ImGui.Unindent(-10);

                ImGui.TableSetColumnIndex(1);
                ImGui.BeginChild("Object Info", new Vector2(-1, ImGui.GetContentRegionAvail().Y - 4));
                {
                    if (string.IsNullOrEmpty(selectedScript)) {
                        ImGui.TextWrapped("Select a script using the menu to the left.");
                    }
                    else {
                        var isRunning = UBService.Scripts.GetAll().Any(s => s.Name.ToLower().Equals(selectedScript.ToLower()));

                        ImGui.Text($"Script: {selectedScript}");
                        ImGui.BeginTabBar(selectedScript);

                        if (ImGui.BeginTabItem("Controls")) {
                            if (ImGui.Button(isRunning ? "Restart" : "Start")) {
                                UBService.Scripts.RestartScript(selectedScript);
                            }
                            if (isRunning) {
                                ImGui.SameLine(0, 10);
                                if (ImGui.Button("Stop")) {
                                    UBService.Scripts.StopScript(selectedScript);
                                }
                            }
                            ImGui.EndTabItem();
                        }
                        if (ImGui.BeginTabItem("Console")) {
                            // Reserve enough left-over height for 1 separator + 1 input text
                            float footer_height_to_reserve = ImGui.GetStyle().ItemSpacing.Y + ImGui.GetFrameHeightWithSpacing();
                            if (useMultiLineInput) {
                                footer_height_to_reserve += 60;
                            }
                            ImGui.BeginChild("ScrollingRegion", new Vector2(0, -footer_height_to_reserve), false, ImGuiWindowFlags.HorizontalScrollbar);

                            ImGui.PushStyleVar(ImGuiStyleVar.ItemSpacing, new Vector2(4, 1)); // Tighten spacing

                            foreach (var line in UBService.Scripts.GetScript(selectedScript)?.Logs ?? new List<string>()) {
                                ImGui.TextUnformatted(line);
                            }

                            if (ScrollToBottom)
                                ImGui.SetScrollHereY(1.0f);

                            ImGui.PopStyleVar();
                            ImGui.EndChild();
                            if (ImGui.BeginPopupContextItem("Context")) {
                                if (ImGui.MenuItem("Clear logs")) {
                                    UBService.Scripts.GetScript(selectedScript)?.Logs.Clear();
                                }
                                ImGui.EndPopup();
                            }
                            ImGui.Separator();

                            if (isRunning) {
                                // Command-line
                                bool reclaim_focus = false;
                                ImGuiInputTextFlags input_text_flags = ImGuiInputTextFlags.EnterReturnsTrue | ImGuiInputTextFlags.CallbackHistory | ImGuiInputTextFlags.CallbackEdit | ImGuiInputTextFlags.CallbackAlways;

                                var callback = (ImGuiInputTextCallback)((data) => {
                                    try {
                                        var dataPtr = new ImGuiInputTextCallbackDataPtr(data);

                                        if (showSuggestions) {
                                            if (dataPtr.EventKey == ImGuiKey.UpArrow) {
                                                dataPtr.CursorPos = lastEditorCursorPos;
                                                selectedIndex--;
                                                return 0;
                                            }
                                            else if (dataPtr.EventKey == ImGuiKey.DownArrow) {
                                                dataPtr.CursorPos = lastEditorCursorPos;
                                                selectedIndex++;
                                                return 0;
                                            }
                                        }

                                        selectedIndex = Math.Max(Math.Min(selectedIndex, suggestions.Count - 1), -1);
                                        lastEditorCursorPos = dataPtr.CursorPos;

                                        if (dataPtr.BufTextLen == 0) {
                                            showSuggestions = false;
                                            suggestions.Clear();
                                            selectedIndex = -1;
                                        }

                                        switch (dataPtr.EventFlag) {
                                            case ImGuiInputTextFlags.CallbackEdit:
                                                if (false && dataPtr.BufTextLen > 0) {
                                                    currentConsoleInput = Encoding.UTF8.GetString((byte*)dataPtr.Buf, dataPtr.BufTextLen);
                                                    var inp = currentConsoleInput;
                                                    inp = inp.Split('\n').LastOrDefault() ?? "";
                                                    inp = inp.Replace("return ", "").Trim();

                                                    if (selectedIndex >= 0 && useMultiLineInput && currentConsoleInput.Split('\n').LastOrDefault() == "") {
                                                        var suggestion = suggestions[selectedIndex];
                                                        var pre = string.Join("\n", currentConsoleInput.Split('\n').Take(currentConsoleInput.Split('\n').Length - 1));
                                                        if (_consoleInput.Contains("game.")) {
                                                            setNext = pre + "game." + suggestion.Split(' ').First();
                                                        }
                                                        else {
                                                            setNext = pre + suggestion.Split(' ').First();
                                                        }
                                                        return 0;
                                                    }

                                                    selectedIndex = -1;
                                                    suggestions.Clear();

                                                    if (currentScriptInstance != null && !string.IsNullOrEmpty(inp)) {
                                                        if (inp.StartsWith("game.")) {
                                                            inp = inp.Replace("game.", "");
                                                            var game = currentScriptInstance.Context.Globals["game"].ToObject<Scripting.Interop.Game>();
                                                            foreach (var x in game.GetType().GetMembers()) {
                                                                if (!x.Name.ToLower().StartsWith(inp.ToLower())) {
                                                                    continue;
                                                                }

                                                                if (x.DeclaringType != typeof(Scripting.Interop.Game))
                                                                    continue;

                                                                if (x is PropertyInfo propInfo) {
                                                                    suggestions.Add($"{x.Name} ({propInfo.PropertyType.Name})");
                                                                }
                                                                else if (x is FieldInfo fieldInfo) {
                                                                    suggestions.Add($"{x.Name} ({fieldInfo.FieldType.Name})");
                                                                }
                                                                else if (x is MethodInfo methodInfo) {
                                                                    suggestions.Add($"{x.Name}() (function)");
                                                                }
                                                            }
                                                        }
                                                        else {
                                                            var globals = currentScriptInstance.WattleScript.Globals;
                                                            foreach (var key in globals.Keys) {
                                                                if (key.String?.ToLower().StartsWith(inp.ToLower()) == true) {
                                                                    suggestions.Add($"{key.String} ({globals[key].GetType().Name})");
                                                                }
                                                            }
                                                        }
                                                        suggestions.Sort();
                                                    }
                                                }
                                                break;
                                            case ImGuiInputTextFlags.CallbackHistory:
                                                int historyPosition = -1;
                                                int prevHistoryPosition = -1;
                                                int historyLength = 0;
                                                if (_inputHistories.TryGetValue(selectedScript, out InputHistory history)) {
                                                    prevHistoryPosition = history.HistoryIndex;
                                                    historyPosition = history.HistoryIndex;
                                                    historyLength = history.History.Count;
                                                }
                                                if (dataPtr.EventKey == ImGuiKey.UpArrow && historyLength > 0) {
                                                    historyPosition = historyPosition + 1;
                                                    if (historyPosition > historyLength - 1)
                                                        historyPosition = -1;
                                                }
                                                else if (dataPtr.EventKey == ImGuiKey.DownArrow && historyLength > 0) {
                                                    historyPosition = historyPosition - 1;
                                                    if (historyPosition < -1)
                                                        historyPosition = historyLength - 1;
                                                }

                                                // A better implementation would preserve the data on the current input line along with cursor position.
                                                if (prevHistoryPosition != historyPosition && history != null) {
                                                    history.HistoryIndex = historyPosition;
                                                    var newValue = (historyPosition >= 0) ? history.History[historyPosition] : "";
                                                    dataPtr.DeleteChars(0, dataPtr.BufTextLen);
                                                    dataPtr.InsertChars(0, newValue);
                                                }
                                                break;
                                        }
                                    }
                                    catch (Exception ex) { UBService.LogException(ex); }
                                    return 0;
                                });

                                ImGui.Checkbox("M", ref useMultiLineInput);
                                ImGui.SameLine();
                                //var cursor = (UBService.Huds.Viewports ? ImGui.GetCursorScreenPos() : ImGui.GetCursorPos());
                                var cursor = ImGui.GetCursorPos();

                                if (setNext != null) {
                                    _consoleInput = setNext;
                                    setNext = null;
                                }

                                if (useMultiLineInput) {
                                    var multi_input_flags = ImGuiInputTextFlags.EnterReturnsTrue | ImGuiInputTextFlags.CallbackEdit | ImGuiInputTextFlags.CallbackAlways;
                                    if (ImGui.InputTextMultiline("Input", ref _consoleInput, 2048, ImGui.GetContentRegionAvail() - new Vector2(0,24), multi_input_flags, callback)) {

                                    }

                                    // Auto-focus on window apparition
                                    ImGui.SetItemDefaultFocus();
                                    if (reclaim_focus)
                                        ImGui.SetKeyboardFocusHere(-1); // Auto focus previous widget

                                    showSuggestions = ImGui.IsItemFocused() && suggestions.Count > 0;
                                    if (false &&showSuggestions) {
                                        intellisensePos = cursor + ImGui.CalcTextSize(currentConsoleInput) + new Vector2(0, 5) + new Vector2(0, ImGui.CalcTextSize("a").Y * (_consoleInput.Split('\n').Length - 1) + (_consoleInput.EndsWith("\n") ? 0 : -ImGui.CalcTextSize("a").Y));

                                        if (useMultiLineInput && !_consoleInput.Contains("\n")) {
                                            intellisensePos += new Vector2(0, ImGui.CalcTextSize("a").Y);
                                        }
                                    }
                                }
                                else {
                                    if (ImGui.InputText("Input", ref _consoleInput, 2048, input_text_flags, callback)) {
                                        UBService.Scripts.GetScript(selectedScript)?.Logs.Add($"> {_consoleInput}");
                                        RunInput(_consoleInput);
                                        if (!_inputHistories.ContainsKey(selectedScript)) {
                                            _inputHistories.Add(selectedScript, new InputHistory());
                                        }
                                        _inputHistories[selectedScript].History.Insert(0, _consoleInput);
                                        _inputHistories[selectedScript].HistoryIndex = -1;
                                        _consoleInput = "";
                                        suggestions.Clear();
                                        showSuggestions = false;
                                        selectedIndex = -1;
                                        reclaim_focus = true;
                                    }

                                    // Auto-focus on window apparition
                                    ImGui.SetItemDefaultFocus();
                                    if (reclaim_focus)
                                        ImGui.SetKeyboardFocusHere(-1); // Auto focus previous widget

                                    showSuggestions = ImGui.IsItemFocused() && suggestions.Count > 0;
                                    if (showSuggestions) {
                                        intellisensePos = cursor + ImGui.CalcTextSize(currentConsoleInput) + new Vector2(0, 5);
                                    }
                                }

                                if (false && showSuggestions) {
                                    var restoreCursor = ImGui.GetCursorPos();
                                    ImGui.SetNextWindowPos(intellisensePos);
                                    ImGui.BeginTooltip();
                                    ImGui.BeginChild("a", new Vector2(250, Math.Min(150, suggestions.Count * ImGui.CalcTextSize("a").Y)));
                                    if (showSuggestions && suggestions.Count > 0) {
                                        var i = 0;
                                        foreach (var suggestion in suggestions) {
                                            bool is_selected = (i == selectedIndex);
                                            if (ImGui.Selectable(suggestion, is_selected)) {
                                            }
                                            i++;
                                        }
                                    }
                                    else {
                                        ImGui.Text($"No suggestions!");
                                    }
                                    ImGui.EndChild();
                                    ImGui.EndTooltip();
                                }

                                if (useMultiLineInput && ImGui.Button("Execute")) {
                                    UBService.Scripts.GetScript(selectedScript)?.Logs?.Add($"> {_consoleInput}");
                                    RunInput(_consoleInput);
                                    if (!_inputHistories.ContainsKey(selectedScript)) {
                                        _inputHistories.Add(selectedScript, new InputHistory());
                                    }
                                    _inputHistories[selectedScript].History.Insert(0, _consoleInput);
                                    _inputHistories[selectedScript].HistoryIndex = -1;
                                    _consoleInput = "";
                                    reclaim_focus = true;
                                }
                            }
                            ImGui.EndTabItem();
                        }
                        if (ImGui.BeginTabItem("Settings")) {
                            var runType = ScriptSettings.GetScriptRunType(selectedScript);
                            var runTypes = new List<ScriptSettings.ScriptRunType>() { ScriptSettings.ScriptRunType.None, ScriptSettings.ScriptRunType.Global, ScriptSettings.ScriptRunType.Account };
                            if (UBService.IsInGame) {
                                runTypes.Add(ScriptSettings.ScriptRunType.Character);
                            }
                            var currentRunType = runTypes.IndexOf(runType);

                            if (ImGui.Combo("AutoLoad", ref currentRunType, runTypes.Select(r => r.ToString()).ToArray(), runTypes.Count)) {
                                var newRuntype = runTypes[currentRunType];
                                UBService.ScriptSettings.GlobalScripts.Value.Remove(selectedScript);
                                UBService.ScriptSettings.AccountScripts.Value.Remove(selectedScript);
                                UBService.ScriptSettings.CharacterScripts.Value.Remove(selectedScript);
                                switch (newRuntype) {
                                    case ScriptSettings.ScriptRunType.Global:
                                        UBService.ScriptSettings.GlobalScripts.Value.Add(selectedScript);
                                        break;
                                    case ScriptSettings.ScriptRunType.Account:
                                        UBService.ScriptSettings.AccountScripts.Value.Add(selectedScript);
                                        break;
                                    case ScriptSettings.ScriptRunType.Character:
                                        UBService.ScriptSettings.CharacterScripts.Value.Add(selectedScript);
                                        break;
                                }
                            }

                            ImGui.Spacing();

                            var hotReloadEnabled = _scriptWatchers.ContainsKey(selectedScript);
                            if (ImGui.Checkbox("Reload script on file change", ref hotReloadEnabled)) {
                                UBService.WriteLog($"hotReloadEnabled = {hotReloadEnabled}", LogLevel.Warning);
                                if (hotReloadEnabled) {
                                    WatchScriptForChanges(selectedScript);
                                }
                                else {
                                    StopWatchingScriptForChanges(selectedScript);
                                }
                            }
                            if (ImGui.IsItemHovered()) {
                                ImGui.SetTooltip("Enabling this will monitor the script directory for file changes and reload when a change is detected.\nThis setting does not persist and will reset when the client is restarted.");
                            }

                            ImGui.Spacing();
                            ImGui.Text("File System Sandboxes:");
                            if (ImGui.BeginTable("FileSystemSandboxes", 5, ImGuiTableFlags.Resizable)) {
                                ImGui.TableSetupColumn("Requested");
                                ImGui.TableSetupColumn("Provided");
                                ImGui.TableSetupColumn("Reason");
                                ImGui.TableSetupColumn("Allowed", ImGuiTableColumnFlags.WidthFixed, 60);
                                ImGui.TableSetupColumn("Actions", ImGuiTableColumnFlags.WidthFixed, 50);
                                ImGui.TableHeadersRow();

                                if (UBService.ScriptSettings.ScriptFileSystemAccess.Value.ContainsKey(selectedScript)) {
                                    var i = 0;
                                    foreach (var x in UBService.ScriptSettings.ScriptFileSystemAccess.Value[selectedScript]) {
                                        i++;
                                        ImGui.TableNextColumn();
                                        ImGui.Text(x.RequestedPath);
                                        ImGui.TableNextColumn();
                                        ImGui.Text(x.ProvidedPath);
                                        ImGui.TableNextColumn();
                                        ImGui.Text(x.Reason);
                                        ImGui.TableNextColumn();
                                        ImGui.TextColored(x.Allowed ? new Vector4(0, 1, 0, 1) : new Vector4(1, 0, 0, 1), x.Allowed.ToString());
                                        ImGui.TableNextColumn();
                                        ImGui.TextureButton($"Edit-{i}", editIcon, new Vector2(18, 18));
                                        ImGui.SameLine(0, 5);
                                        ImGui.TextureButton($"Delete-{i}", deleteIcon, new Vector2(18, 18));
                                    }
                                }

                                ImGui.EndTable();
                            }

                            ImGui.EndTabItem();
                        }

                        ImGui.EndTabBar();
                    }
                }
                ImGui.EndChild(); // Object Info
            }
            ImGui.EndTable();
        }

        async private void RunInput(string consoleInput) {
            var watch = new System.Diagnostics.Stopwatch();
            watch.Start();
            object res = null;
            try {
                res = currentScriptInstance?.RunTextNoCatch(_consoleInput);
            }
            catch (ScriptRuntimeException ex) {
                currentScriptInstance?.Logs.Add($"An error occured! {ex.DecoratedMessage}");
            }
            catch (SyntaxErrorException ex) {
                currentScriptInstance?.Logs.Add($"A syntax error occured! {ex.DecoratedMessage}");
            }
            catch (Exception ex) {
                currentScriptInstance?.Logs.Add(ex.ToString());
            }
            watch.Stop();
            currentScriptInstance?.Logs.Add($"{PrettyPrint(res)} ({Math.Round(watch.ElapsedTicks / 10000.0, 3)}ms)");
        }

        private void SelectScript(string script) {
            selectedScript = script;

            if (currentScriptInstance != null) {
                currentScriptInstance = null;
            }

            var scriptInstance = UtilityBelt.Service.UBService.Scripts.GetScript(script);
            if (scriptInstance != null) {
                currentScriptInstance = scriptInstance;
            }
        }

        public static string PrettyPrint(object res) {
            if (res == null)
                return "null";

            return $"({res.GetType().AssemblyQualifiedName}) {res}";
        }

        public void Dispose() {
            hud.OnRender -= Hud_Render;
            hud.OnPreRender -= Hud_PreRender;
            hud.Dispose();

            UBService.Scripts.OnScriptStarted -= Scripts_OnScriptStarted;
            UBService.Scripts.OnScriptStopped -= Scripts_OnScriptStopped;

            foreach (var script in _scripts) {
                UtilityBelt.Service.UBService.Scripts.StopScript(script);
            }
            _scripts.Clear();
        }
    }
}
