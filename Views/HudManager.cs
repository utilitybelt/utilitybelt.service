﻿using Decal.Adapter;
using Decal.Adapter.Wrappers;
using ImGuiNET;
using Microsoft.DirectX.Direct3D;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using UtilityBelt.Service.Lib;
using UtilityBelt.Service.Lib.Settings;
using UtilityBelt.Service.Views;
using UtilityBelt.Service;
using UtilityBelt.Service.Lib.Settings.Serializers;
using Microsoft.Extensions.Logging;
using ImPlotNET;
using RoyT.TrueType;
using ImGuizmoNET;
using AcClient;
using ACE.DatLoader.FileTypes;

namespace UtilityBelt.Service.Views {

    /// <summary>
    /// Manages ImGui huds
    /// </summary>
    public class HudManager : ISetting, IDisposable {
        private object _hudLock = new object();
        private DragDropPayload _lastDragDropInfo;
        internal ImFontPtr _fontAwesome;

        internal DateTime _lastRender = DateTime.MinValue;
        internal bool didInit;
        internal List<Hud> huds = new List<Hud>();
        internal List<ManagedTexture> textures = new List<ManagedTexture>();
        internal static JsonSerializerSettings jsonSerializerSettings;

        public FontManager FontManager { get; }

        internal Guid IID_IDirect3DDevice9 = new Guid("{D0223B96-BF7A-43fd-92BD-A43B0D82B9EB}");
        public Device D3Ddevice { get; internal set; }
        internal IntPtr unmanagedD3dPtr;
        internal IntPtr _context;
        private IntPtr _imPlotContext;
        private readonly float MAX_UI_FRAMERATE = 60000;

        private bool needsTextures = true;
        private bool isResetting;
        private HudProfiler _hudProfiler;
        private bool _needsFontRebuild = false;

        internal string themesDir = Path.Combine(UtilityBelt.Service.UBService.AssemblyDirectory, "themes");
        internal string profilesDir = Path.Combine(UtilityBelt.Service.UBService.AssemblyDirectory, "settings");

        public event EventHandler<HudAddedEventArgs> OnHudAdded;
        public event EventHandler<HudRemovedEventArgs> OnHudRemoved;
        public event EventHandler<EventArgs> OnBeforeRender;
        public event EventHandler<EventArgs> OnAfterRender;

        public IList<Hud> Huds => huds.ToArray();

        /// <summary>
        /// The file path to the currently loaded views profile
        /// </summary>
        public string CurrentProfilePath {
            get {
                if (!UtilityBelt.Service.UBService.IsInGame) {
                    return Path.Combine(profilesDir, $"global.views.json");
                }
                else if (Profile == "[character]") {
                    try {
                        return Path.Combine(profilesDir, $"__{CoreManager.Current.CharacterFilter.AccountName}_{CoreManager.Current.CharacterFilter.Name}.views.json");
                    }
                    catch {
                        return Path.Combine(profilesDir, $"global.views.json");
                    }
                }
                else {
                    return Path.Combine(profilesDir, $"{Profile}.views.json");
                }
            }
        }

        public Toaster Toaster;
        public HudBar HudBar;

        private List<FontInstance> _fontStack = new();
        public FontInstance CurrentFont {
            get => _fontStack.LastOrDefault();
            set => _fontStack.Add(value);
        }

        #region Config
        [Summary("Theme storage directory path")]
        public ViewsProfileSetting<string> ThemeStorageDirectory = new ViewsProfileSetting<string>(Path.Combine(Path.GetDirectoryName(Assembly.GetAssembly(typeof(UBService)).Location), "themes"));

        [Summary("Current Theme")]
        public ViewsProfileSetting<string> CurrentThemeName = new ViewsProfileSetting<string>("Dark");

        //[Summary("Enable viewports. (Ability to render plugin windows outside of the client window, experimental)")]
        //public ViewsProfileSetting<bool> Viewports = new ViewsProfileSetting<bool>(false);

        [Summary("View Settings Profile")]
        public CharacterSetting<string> Profile = new CharacterSetting<string>("[character]");

        [Summary("Font")]
        [Choices("Noto Sans - Regular", typeof(GetAvailableFonts))]
        public ViewsProfileSetting<string> Font = new ViewsProfileSetting<string>("Noto Sans - Regular");

        [Summary("Font Size")]
        public ViewsProfileSetting<int> FontSize = new ViewsProfileSetting<int>(16);

        [Summary("Enable Hud profiling")]
        public Setting<bool> HudProfiling = new Global<bool>(false);
        #endregion // Config

        internal class GetAvailableFonts : IChoiceResults {
            public GetAvailableFonts() {

            }

            public IList<string> GetChoices() {
                return UBService.Huds.FontManager.AvailableFonts.Where(f => !f.Family.ToLower().Contains("fontawesome")).Select(f => $"{f.Family} - {f.SubFamily}").ToList();
            }
        }

        /// <summary>
        /// Current global theme
        /// </summary>
        public UBServiceTheme CurrentTheme { get; internal set; } = new UBServiceTheme();

        internal HudManager() {
            jsonSerializerSettings = new JsonSerializerSettings() {
                Converters = new List<JsonConverter>() {
                    new ImGuiVectorConverter()
                }
            };

            FontManager = new FontManager();

            Toaster = new Toaster();
            HudBar = new HudBar();
        }

        internal unsafe void Init() {
            _context = ImGui.CreateContext();
            _imPlotContext = ImPlot.CreateContext();
            ImGui.SetCurrentContext(_context);
            //if (Viewports)
            //    ImGui.GetIO().ConfigFlags |= ImGuiConfigFlags.ViewportsEnable;
            //else
                ImGui.GetIO().NativePtr->ConfigFlags &= ~ImGuiConfigFlags.ViewportsEnable;
            ImGui.GetIO().NativePtr->IniSavingRate = float.MinValue; // no ini saving

            var defaultThemePath = Path.Combine(themesDir, $"{CurrentThemeName}.json");
            if (File.Exists(defaultThemePath)) {
                try {
                    var themeJson = File.ReadAllText(defaultThemePath);
                    CurrentTheme = JsonConvert.DeserializeObject<UBServiceTheme>(themeJson, jsonSerializerSettings);
                }
                catch (Exception ex) { UBService.LogException(ex); }
            }

            CurrentTheme.Apply();

            object d3dDevice = UBService.iDecal.GetD3DDevice(ref IID_IDirect3DDevice9);
            Marshal.QueryInterface(Marshal.GetIUnknownForObject(d3dDevice), ref IID_IDirect3DDevice9, out unmanagedD3dPtr);
            D3Ddevice = new Device(unmanagedD3dPtr);
            var ret1 = ImGuiImpl.ImGui_ImplWin32_Init((IntPtr)UBService.iDecal.HWND);
            //ImGuiImpl.ImGui_ImplWin32_EnableDpiAwareness();
            var ret2 = ImGuiImpl.ImGui_ImplDX9_Init(unmanagedD3dPtr);
            CurrentThemeName.Changed += CurrentThemeName_Changed;
            //Viewports.Changed += Viewports_Changed;
            Font.Changed += Font_Changed;
            FontSize.Changed += FontSize_Changed;

            Toaster.Init();
            HudBar.Init();


            var io = ImGui.GetIO();

            HudProfiling.Changed += HudProfiling_Changed;
            HudProfiling_Changed(null, null);

            didInit = true;
            _needsFontRebuild = true;
        }

        private void HudProfiling_Changed(object sender, SettingChangedEventArgs e) {
            if (HudProfiling && _hudProfiler == null) {
                _hudProfiler = new HudProfiler();
            }
            else if (!HudProfiling && _hudProfiler != null) {
                _hudProfiler.Dispose();
                _hudProfiler = null;
            }
        }

        private void Font_Changed(object sender, SettingChangedEventArgs e) {
            _needsFontRebuild = true;
        }

        private void FontSize_Changed(object sender, SettingChangedEventArgs e) {
            _needsFontRebuild = true;
        }

        internal void TryReloadCurrentFont() {
            var fontParts = Font.Value.Replace(".ttf","").Split('-');
            var fontFamily = string.Join("-", fontParts.Take(fontParts.Length - 1)).Trim();
            var fontSubFamily = fontParts.LastOrDefault();


            Enum.TryParse<FontSubFamily>(fontSubFamily ?? "Regular", out var subFamily);
            var font = FontManager.FindAvailableBestMatch(new List<string> { fontFamily }, subFamily);
            if (font != null) {
                var fontInstance = FontManager.Create(font, FontSize);
                if (fontInstance != null) {
                    CurrentFont = fontInstance;
                }
            }
        }

        private void Viewports_Changed(object sender, SettingChangedEventArgs e) {
            try {
                /*
                if (Viewports)
                    ImGui.GetIO().ConfigFlags |= ImGuiConfigFlags.ViewportsEnable;
                else
                    ImGui.GetIO().ConfigFlags &= ~ImGuiConfigFlags.ViewportsEnable;
                //*/
            }
            catch (Exception ex) {
                UBService.LogException(ex);
            }
        }

        private void CurrentThemeName_Changed(object sender, SettingChangedEventArgs e) {
            var defaultThemePath = Path.Combine(themesDir, $"{CurrentThemeName}.json");
            UBService.WriteLog($"CurrentThemeName_Changed: {defaultThemePath}", LogLevel.Debug);
            if (File.Exists(defaultThemePath)) {
                try {
                    var themeJson = File.ReadAllText(defaultThemePath);
                    CurrentTheme = JsonConvert.DeserializeObject<UBServiceTheme>(themeJson, jsonSerializerSettings);
                    CurrentTheme.Apply();
                    UBService.WriteLog($"ApplyTheme: {defaultThemePath}", LogLevel.Debug);
                }
                catch (Exception ex) { UBService.LogException(ex); }
            }
        }

        /// <summary>
        /// Create a new hud
        /// </summary>
        /// <param name="name">Name of the hud</param>
        /// <param name="icon">Icon for the HudBar, if null uses first letter of Hud.Name</param>
        /// <returns>A new hud</returns>
        public Hud CreateHud(string name, Bitmap icon = null) {
            UBService.WriteLog($"CreateHud: {name} // {icon}", LogLevel.Debug);
            var hud = new Hud(name, icon);

            hud.OnDisposed += Hud_OnDisposed;

            lock (_hudLock) {
                huds.Add(hud);
                OnHudAdded?.Invoke(this, new HudAddedEventArgs(hud));
            }

            return hud;
        }

        private void Hud_OnDisposed(object sender, EventArgs e) {
            if (sender is Hud hud) {
                UBService.WriteLog($"Hud disposed: {hud.Name} {huds.Count}", LogLevel.Debug);
                hud.OnDisposed -= Hud_OnDisposed;
                RemoveHud(hud);
                UBService.WriteLog($"   - Hud disposed: {hud.Name} {huds.Count}", LogLevel.Debug);
            }
        }

        internal void RemoveHud(Hud hud) {
            lock (_hudLock) {
                OnHudRemoved?.Invoke(this, new HudRemovedEventArgs(hud));
                huds.Remove(hud);
            }
        }

        internal unsafe bool WindowMessage(int hWND, short uMsg, int wParam, int lParam) {
            bool eat = false;

            if (uMsg == 0x20/*setcursor*/ || uMsg == 0x84/*WM_NCHITTEST*/ || ImGui.GetCurrentContext() == IntPtr.Zero)
                return eat;

            ImGuiImpl.ImGui_ImplWin32_WndProcHandler((void*)(IntPtr)hWND, (uint)uMsg, (IntPtr)wParam, (IntPtr)lParam);

            var io = ImGui.GetIO();
            bool isMouseEvent = uMsg >= 0x0201 && uMsg <= 0x020e;
            bool isKeyboardEvent = uMsg >= 0x0100 && uMsg <= 0x0102;

            if ((io.WantCaptureMouse && isMouseEvent) || (io.WantCaptureKeyboard && isKeyboardEvent)) {
                // handling input should cause an immediate re-render
                _lastRender = DateTime.MinValue;
                eat = true;
            }

            /*
            try {
                if (io.WantCaptureKeyboard > 0 && isKeyboardEvent) {
                    CoreManager.Current.Actions.AddChatText($"EAT (eat {eat}) 0x{uMsg:X4} WantCaptureKeyboard: {io.WantCaptureKeyboard} WantTextInput: {io.WantTextInput} IsKeyboardEvent: {isKeyboardEvent}", 1);
                }
                else if (uMsg != 0x0084) {
                    CoreManager.Current.Actions.AddChatText($"PASS (eat: {eat}) 0x{uMsg:X4} WantCaptureKeyboard: {io.WantCaptureKeyboard} WantTextInput: {io.WantTextInput} IsKeyboardEvent: {isKeyboardEvent}", 1);
                }
            }
            catch { }
            */
            return eat;
        }

        internal unsafe void DoRender() {
            if (!didInit || isResetting)
                return;

            if (_needsFontRebuild) {
                TryReloadCurrentFont();
                _needsFontRebuild = false;
                ImGui.GetIO().Fonts.Build();
                PreReset();
                PostReset();

                CheckTextures();
            }
            else {
                CheckTextures();
            }

            OnBeforeRender?.Invoke(this, EventArgs.Empty);


            // limit uis to MaxFramerate
            if ((DateTime.UtcNow - _lastRender).TotalMilliseconds > (1000f / (float)MAX_UI_FRAMERATE)) {
                var io = ImGui.GetIO();
                _lastRender = DateTime.UtcNow;
                ImGuiImpl.ImGui_ImplDX9_NewFrame();
                ImGuiImpl.ImGui_ImplWin32_NewFrame();
                ImGui.NewFrame();
                ImGuizmo.BeginFrame();

                if (CurrentFont != null) {
                    FontManager.TryPushFont(CurrentFont);
                }

                var dragEl = UIElementManager.s_pInstance->m_dragElement;
                if (dragEl is not null) {
                    if (ImGui.BeginDragDropSource(ImGuiDragDropFlags.SourceExtern | ImGuiDragDropFlags.AcceptBeforeDelivery)) {
                        _lastDragDropInfo = GetDragDropInfo(dragEl);
                        fixed (DragDropPayload* info = &_lastDragDropInfo) {
                            ImGui.SetDragDropPayload("ACDRAGDROP", (IntPtr)info, (uint)sizeof(DragDropInfo));
                        }
                        ImGui.Text(_lastDragDropInfo.ToString());
                        ImGui.EndDragDropSource();
                    }
                }

                var _huds = huds.ToArray();
                foreach (var hud in _huds) {
                    hud?.CallRender();
                }

                HudBar.Render();

                if (CurrentFont != null) {
                    FontManager.TryPopFont(CurrentFont);
                }

                ImGui.EndFrame();
                ImGui.Render();

                // Update and Render additional Platform Windows
                if ((io.ConfigFlags & ImGuiConfigFlags.ViewportsEnable) != 0) {
                    ImGui.UpdatePlatformWindows();
                    ImGui.RenderPlatformWindowsDefault();
                }
            }

            ImGuiImpl.ImGui_ImplDX9_RenderDrawData((IntPtr)ImGui.GetDrawData().NativePtr);

            OnAfterRender?.Invoke(this, EventArgs.Empty);
        }

        private unsafe DragDropPayload GetDragDropInfo(UIElement* dragEl) {
            uint itemId = 0;
            uint spellId = 0;
            DropItemFlags flags;
            UIElement_ItemList.InqDropIconInfo(dragEl, &itemId, &spellId, &flags);

            return new DragDropPayload() {
                SpellId = spellId,
                ItemId = itemId,
                Flags = flags
            };
        }

        public void RebuildFonts() {
            this._needsFontRebuild = true;
        }

        private void CheckTextures() {
            if (needsTextures) {
                UBService.WriteLog("HudManager making new textures", LogLevel.Debug);
                ImGuiImpl.ImGui_ImplDX9_CreateDeviceObjects();

                var _managedTextures = textures.ToArray();
                foreach (var managedTexture in _managedTextures) {
                    managedTexture.CreateTexture();
                }

                var _huds = huds.ToArray();
                foreach (var hud in _huds) {
                    try {
                        hud.CallCreateTextures();
                    }
                    catch (Exception ex) { UBService.LogException(ex); }
                }
                needsTextures = false;
            }
        }

        internal void PreReset() {
            UBService.WriteLog("HudManager is clearing textures (PreReset)", LogLevel.Debug);
            isResetting = true;

            var _managedTextures = textures.ToArray();
            foreach (var managedTexture in _managedTextures) {
                managedTexture.ReleaseTexture();
            }

            var _huds = huds.ToArray();
            foreach (var hud in _huds) {
                try {
                    hud.CallDestroyTextures();
                }
                catch (Exception ex) { UBService.LogException(ex); }
            }
            ImGuiImpl.ImGui_ImplDX9_InvalidateDeviceObjects();

            needsTextures = true;
        }

        internal void PostReset() {
            isResetting = false;
        }

        internal void AddManagedTexture(ManagedTexture managedTexture) {
            textures.Add(managedTexture);
        }

        internal void RemoveManagedTexture(ManagedTexture managedTexture) {
            textures.Remove(managedTexture);
        }

        public void Dispose() {
            CurrentThemeName.Changed -= CurrentThemeName_Changed;
            //Viewports.Changed -= Viewports_Changed;
            Toaster?.Dispose();
            HudBar?.Dispose();

            ImGui.DestroyContext();
            ImPlot.DestroyContext();
        }
    }
}
