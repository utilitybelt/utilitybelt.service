﻿using ImGuiNET;
using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using UtilityBelt.Service.Views;

namespace UtilityBelt.Service.Views {
    public class Key {
        public string Name;
        public uint Order;
        public uint ScanCodePage1 = 0; // win32 scancode
        public uint ScanCodePage7 = 0; // HID (SDL, ...)
        public float Offset = 0;
        public float Width = 40;

        public Key(string name, uint order, uint scanCodePage1 = 0, uint scanCodePage7 = 0, float offset = 0, float width = 40) {
            Name = name;
            Order = order;
            ScanCodePage1 = scanCodePage1;
            ScanCodePage7 = scanCodePage7;
            Offset = offset;
            Width = width;
        }
    }

    public class HotKey {
        public string FunctionName = "";
        public string FunctionLib = "";
        public uint FunctionKeys = 0xFFFFFFFF;
        public string Description = "";

        public HotKey(string functionName, string functionLib, uint functionKeys, string description) {
            FunctionName = functionName;
            FunctionLib = functionLib;
            FunctionKeys = functionKeys;
            Description = description;
        }
    }
    /// <summary>
    /// Heavily adapted from https://github.com/CedricGuillemet/ImHotKey
    /// </summary>
    public class HotkeyEditor : IDisposable {

        static List<List<Key>> Keys = new List<List<Key>>() {
            new List<Key>() { new Key("Esc", 4, 0x1, 0x29, 18),new Key( "F1", 5, 0x3B, 0x3A, 18),new Key("F2", 6, 0x3C, 0x3B),new Key("F3", 7, 0x3D, 0x3C),new Key("F4", 8, 0x3E, 0x3D),new Key("F5", 9, 0x3F, 0x3E, 24), new Key("F6", 10, 0x40, 0x3F),new Key("F7", 11, 0x41, 0x40),new Key("F8", 12, 0x42, 0x41),new Key("F9", 13, 0x43, 0x42, 24),new Key("F10", 14, 0x44, 0x43), new Key("F11", 15, 0x57, 0x44),new Key("F12", 16, 0x58, 0x45), new Key("PrSn", 17, 0x37, 0x46, 24), new Key("ScLk", 18, 0x46), new Key("Brk", 19, 126, 0x47) },
            new List<Key>() { new Key("~", 20, 0x29, 0x35), new Key("1", 21, 0x2, 0x1E), new Key("2", 22, 0x3, 0x1F), new Key("3", 23, 0x4, 0x20), new Key("4", 24, 0x5, 0x21), new Key("5", 25, 0x6, 0x22), new Key("6", 26, 0x7, 0x23), new Key("7", 27, 0x8, 0x24), new Key("8", 28, 0x9, 0x25), new Key("9", 29, 0xA, 0x26), new Key("0", 30, 0xB, 0x27), new Key("-", 31, 0xC, 0x2D), new Key("+", 32, 0xD, 0x2E), new Key("Backspace", 33, 0xE, 0x2A, 0, 80), new Key("Ins", 34, 0x52, 0x49, 24), new Key("Hom", 35, 0x47, 0x4A), new Key("PgU", 36, 0x49, 0x4B) },
            new List<Key>() { new Key("Tab", 3, 0xF, 0x2B, 0, 60), new Key("Q", 37, 0x10, 0x14), new Key("W", 38, 0x11, 0x1A), new Key("E", 39, 0x12, 0x08), new Key("R", 40, 0x13, 0x15), new Key("T", 41, 0x14, 0x17), new Key("Y", 42, 0x15, 0x1C), new Key("U", 43, 0x16, 0x18), new Key("I", 44, 0x17, 0x0C), new Key("O", 45, 0x18, 0x12), new Key("P", 46, 0x19, 0x13), new Key("[", 47, 0x1A, 0x2F), new Key("]", 48, 0x1B, 0x30), new Key("|", 49, 0x2B, 0x31, 0, 60), new Key("Del", 50, 0x53, 0x4C, 24), new Key("End", 51, 0x4F, 0x4D), new Key("PgD", 52, 0x51, 0x4E) },
            new List<Key>() { new Key("Caps Lock", 53, 0x3A, 0x39, 0, 80), new Key("A", 54, 0x1E, 0x04), new Key("S", 55, 0x1F, 0x16), new Key("D", 56, 0x20, 0x07), new Key("F", 57, 0x21, 0x09), new Key("G", 58, 0x22, 0x0A), new Key("H", 59, 0x23, 0x0B), new Key("J", 60, 0x24, 0x0D), new Key("K", 61, 0x25, 0x0E), new Key("L", 62, 0x26, 0x0F), new Key(";", 63, 0x27, 0x33), new Key("'", 64, 0x28, 0x34), new Key("Ret", 65, 0x1C, 0X28, 0, 84) },
            new List<Key>() { new Key("Shift", 2, 0x2A, 0xE1, 0, 104), new Key("Z", 66, 0x2C, 0x1D), new Key("X", 67, 0x2D, 0x1B), new Key("C", 68, 0x2E, 0x06), new Key("V", 69, 0x2F, 0x19), new Key("B", 70, 0x30, 0x05), new Key("N", 71, 0x31, 0x11), new Key("M", 72, 0x32, 0x10), new Key(",", 73, 0x33, 0x36), new Key(".", 74, 0x34, 0x37), new Key("/", 75, 0x35, 0x38), new Key("Shift", 2, 0x2A, 0xE5, 0, 104), new Key("Up", 76, 0x48, 0x52, 68) },
            new List<Key>() { new Key("Ctrl", 0, 0x1D, 0xE0, 0, 60), new Key("Alt", 1, 0x38, 0xE2, 68, 60), new Key("Space", 77, 0x39, 0X2c, 0, 260), new Key("Alt", 1, 0x38, 0xE6, 0, 60), new Key("Ctrl", 0, 0x1D, 0xE4, 68, 60), new Key("Left", 78, 0x4B, 0x50, 24), new Key("Down", 79, 0x50, 0x51), new Key("Right", 80, 0x4D, 0x52) }
        };

        public Hud Hud { get; }

        public List<HotKey> Hotkeys = new List<HotKey>() {
            new HotKey("UtilityBelt", "ToggleDungeonMaps", 0xFFFFFFFF, "Toggle the utilitybelt dungeon maps window."),
            new HotKey("PortalBot", "ReComp", 0xFFFFFFFF, "Force PortalBot to recomp at vendor."),
        };
        private int editingHotkey;
        private bool _didOpen;

        [DllImport("user32.dll")]
        static extern uint MapVirtualKeyEx(uint uCode, uint uMapType, IntPtr dwhkl);
        [DllImport("user32.dll")]
        static extern uint MapVirtualKey(uint uCode, uint uMapType);
        
        const uint MAPVK_VK_TO_VSC = 0x00;
        const uint MAPVK_VSC_TO_VK = 0x01;
        const uint MAPVK_VK_TO_CHAR = 0x02;
        const uint MAPVK_VSC_TO_VK_EX = 0x03;
        const uint MAPVK_VK_TO_VSC_EX = 0x04;

        public HotkeyEditor() {
            Hud = UBService.Huds.CreateHud("Hotkey Editor");
            Hud.OnRender += Hud_PreRender;
            Hud.DontDrawDefaultWindow = true;
            Hud.Visible = true;
        }

        ushort[] scanCodes = new ushort[] { 0xFF, 0xFF, 0xFF, 0xFF };
        ushort[] order = new ushort[] { 0xFF, 0xFF, 0xFF, 0xFF };
        bool[] keyDown = new bool[512];

        private void Hud_PreRender(object sender, EventArgs e) {
            try {
                var viewportCenter = ImGui.GetMainViewport().Pos + (ImGui.GetMainViewport().Size / 2);
                viewportCenter.X -= (ImGui.GetMainViewport().Size.X / 4);
                //ImGui.SetNextWindowPos(viewportCenter, ImGuiCond.Once, new Vector2(0.5f, 0.5f));
                ImGui.SetNextWindowSize(new Vector2(1060, 400));
                var visible = true;
                if (ImGui.BeginPopupModal("Hotkey Editor")) {

                    ImGui.BeginChildFrame(127, new Vector2(220, -1));
                    for (var i = 0; i < Hotkeys.Count; i++) {
                        var hotkey = Hotkeys[i];
                        if (ImGui.Selectable(GetHotKeyLib(hotkey), editingHotkey == i) || editingHotkey == -1) {
                            editingHotkey = i;
                            keyDown = new bool[512];
                            for (int j = 0; j < 4; j++) {
                                int scan = (int)(Hotkeys[editingHotkey].FunctionKeys >> (8 * j)) & 0xFF;
                                if (scan != 0xFF) {
                                    keyDown[scan] = true;
                                }
                            }
                        }
                    }
                    ImGui.EndChildFrame();

                    ImGui.SameLine();
                    ImGui.BeginGroup();


                    for (int i = 0; i < 512; i++) {
                        var key = (ImGuiKey)i;
                        if (ImGui.IsKeyPressed(key, false)) {
                            int imKey = (int)MapVirtualKey((uint)i, MAPVK_VK_TO_VSC);
                            keyDown[imKey] = !keyDown[imKey];
                        }
                    }

                    for (int y = 0; y < 6; y++) {
                        ImGui.BeginGroup();
                        for (var x = 0; x < Keys[y].Count; x++) {
                            var key = Keys[y][x];
                            float ofs = key.Offset + (x > 0 ? 4f : 0f);

                            float width = key.Width;
                            if (x > 0) {
                                ImGui.SameLine(0f, ofs);
                            }
                            else {
                                if (ofs >= 1f) {
                                    ImGui.Indent(ofs);
                                }
                            }

                            ImGui.PushStyleColor(ImGuiCol.Button, keyDown[key.ScanCodePage1] ? 0xFF1040FF : 0x80000000);
                            if (ImGui.Button(key.Name, new Vector2(width, 40))) {
                                keyDown[key.ScanCodePage1] = !keyDown[key.ScanCodePage1];
                            }
                            ImGui.PopStyleColor();
                        }
                        ImGui.EndGroup();
                    }
                    ImGui.InvisibleButton("space", new Vector2(10, 55));
                    ImGui.BeginChildFrame(18, new Vector2(540, 40));
                    ImGui.TextWrapped(GetHotKeyLib(Hotkeys[editingHotkey]));
                    ImGui.TextWrapped(Hotkeys[editingHotkey].Description);
                    ImGui.EndChildFrame();
                    ImGui.SameLine();

                    int keyDownCount = 0;
                    foreach (var d in keyDown) {
                        keyDownCount += d ? 1 : 0;
                    }
                    if (ImGui.Button("Clear", new Vector2(80, 40))) {
                        keyDown = new bool[512];
                    }
                    ImGui.SameLine();

                    if (keyDownCount > 0 && keyDownCount < 5) {
                        if (ImGui.Button("Set", new Vector2(80, 40))) {
                            int scanCodeCount = 0;
                            Hotkeys[editingHotkey].FunctionKeys = 0;
                            for (uint i = 1; i < keyDown.Length; i++) {
                                if (keyDown[i]) {
                                    scanCodes[scanCodeCount] = (ushort)i;
                                    order[scanCodeCount] = (ushort)GetKeyForScanCode(i).Order;
                                    scanCodeCount++;
                                }
                            }

                            Hotkeys[editingHotkey].FunctionKeys = GetOrderedScanCodes(scanCodes, order);
                        }
                        ImGui.SameLine(0, 20);
                    }
                    else {
                        ImGui.SameLine(0, 100);
                    }

                    if (ImGui.Button("Done", new Vector2(80, 40))) {
                        ImGui.CloseCurrentPopup();
                    }
                    ImGui.EndGroup();
                    ImGui.EndPopup();
                }
                if (!_didOpen) {
                    ImGui.OpenPopup("Hotkey Editor");
                    _didOpen = true;
                }
            }
            catch (Exception ex) { UBService.LogException(ex); }
        }

        static Key GetKeyForScanCode(uint scancode) {
            for (int y = 0; y < 6; y++) {
                for (var x = 0; x < Keys[y].Count; x++) {
                    if (Keys[y][x].ScanCodePage1 == scancode)
                        return Keys[y][x];
                }
            }
            return Keys[0][0];
        }

        static string GetHotKeyLib(HotKey hotkey) {
            var lib = new List<string>() {};

            var scanCodeCount = 0;
            for (int i = 0; i< 4; i++) {
                ushort scanCode = (byte)(hotkey.FunctionKeys >> i * 8);
                if (scanCode == 0xFF) {
                    continue;
                }
                lib.Add(GetKeyForScanCode(scanCode).Name);
                scanCodeCount++;
            }
            if (scanCodeCount == 0) {
                    return "";
            }

            return $"{hotkey.FunctionName}:{hotkey.FunctionLib} ({string.Join(" + ", lib)})";
        }

        static uint GetOrderedScanCodes(ushort[] scanCodes, ushort[] order) {
            for (int pass = 0; pass < 2; pass++) {
                for (int o = 0; o < 3; o++) {
                    if (order[o] > order[o + 1]) {
                        var a = order[o];
                        order[o] = order[o + 1];
                        order[o + 1] = a;
                        var b = scanCodes[o];
                        scanCodes[o] = scanCodes[o + 1];
                        scanCodes[o + 1] = b;
                    }
                }
            }
            return (uint)((scanCodes[3] << 24) + (scanCodes[2] << 16) + (scanCodes[1] << 8) + scanCodes[0]);
        }

        public void Dispose() {
            Hud.OnPreRender -= Hud_PreRender;
            Hud?.Dispose();
        }
    }
}
