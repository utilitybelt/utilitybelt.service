﻿using ACE.DatLoader.FileTypes;
using ACE.DatLoader;
using Decal.Adapter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Common.Enums;
using UtilityBelt.Service.Lib;
using Decal.Adapter.Wrappers;

namespace UtilityBelt.Service.Views {
    public class ScriptHudManager : IDisposable {
        private List<Hud> _huds = new List<Hud>();
        private List<ManagedTexture> _textures = new List<ManagedTexture>();
        private Bitmap _bmpIcon;

        public Toaster Toaster => UBService.Huds.Toaster;
        private HudBar HudBar => UBService.Huds.HudBar;

        internal string ScriptName { get; private set; }

        internal string ScriptDirectory { get; private set; }

        internal string ScriptDataDirectory { get; private set; }


        public event EventHandler OnHudBarContextRender;

        public ScriptHudManager() {
            UBService.Huds.HudBar.OnHudBarContextRender += HudBar_OnHudBarContextRender;
        }

        private void HudBar_OnHudBarContextRender(object sender, EventArgs e) {
            OnHudBarContextRender?.InvokeSafely(this, e);
        }

        public Hud CreateHud(string name, string icon = null) {
            Bitmap bmp = null;
            var resolvedPath = ResolvePath(icon);

            if (!string.IsNullOrEmpty(icon) && File.Exists(resolvedPath)) {
                bmp = new Bitmap(resolvedPath);
            }

            var hud = UBService.Huds.CreateHud(name, bmp);
            hud.OnDisposed += Hud_OnDisposed;

            _huds.Add(hud);

            return hud;
        }

        private void Hud_OnDisposed(object sender, EventArgs e) {
            if (sender is Hud hud) {
                hud.OnDisposed -= Hud_OnDisposed;
                _huds.Remove(hud);
            }
        }

        public Hud CreateHud(string name, uint icon = 0) {
            Bitmap bmp = null;

            if (icon != 0) {
                if (icon < 0x06000000)
                    icon += 0x06000000;
                var iconFile = UBService.PortalDat.ReadFromDat<ACE.DatLoader.FileTypes.Texture>(icon);

                if (iconFile != null && iconFile.SourceData != null) {
                    bmp = GetBitmap(iconFile);
                    _bmpIcon = bmp;
                }
            }

            var hud = UBService.Huds.CreateHud(name, bmp);
            _huds.Add(hud);

            bmp?.Dispose();

            return hud;
        }

        /// <summary>
        /// Creates a managed texture for use in imgui from the specified bitmap file.
        /// </summary>
        /// <param name="file">The file path to load</param>
        /// <returns>A managed texture</returns>
        public ManagedTexture CreateTexture(string file) {
            var resolvedPath = ResolvePath(file);

            if (!string.IsNullOrEmpty(file) && File.Exists(resolvedPath)) {
                using (var bmp = new Bitmap(resolvedPath)) {
                    var texture = new ManagedTexture(bmp);
                    _textures.Add(texture);
                    return texture;
                }
            }

            return null;
        }

        /// <summary>
        /// Creates a managed texture for use in imgui from the specified ac icon id
        /// </summary>
        /// <param name="iconId">The file path to load</param>
        /// <returns>A managed texture</returns>
        public ManagedTexture GetIconTexture(uint iconId) {
            if (iconId < 0x06000000)
                iconId += 0x06000000;

            var iconFile = UBService.PortalDat.ReadFromDat<ACE.DatLoader.FileTypes.Texture>(iconId);

            if (iconFile != null && iconFile.SourceData != null) {
                using (var bmp = GetBitmap(iconFile)) {
                    var texture = new ManagedTexture(bmp);
                    _textures.Add(texture);
                    return texture;
                }
            }

            return null;
        }

        private Bitmap GetBitmap(byte[] byteArray, int Width, int Height) {
            Bitmap image = new Bitmap(Width, Height);
            for (int i = 0; i < Height; i++)
                for (int j = 0; j < Width; j++) {
                    int idx = 4 * ((i * Width) + j);
                    int r = (int)(byteArray[idx]);
                    int g = (int)(byteArray[idx + 1]);
                    int b = (int)(byteArray[idx + 2]);
                    int a = (int)(byteArray[idx + 3]);
                    image.SetPixel(j, i, Color.FromArgb(a, r, g, b));
                }

            return image;
        }

        private Bitmap GetBitmap(Texture texture) {
            Bitmap image = new Bitmap(texture.Width, texture.Height);
            var colorArray = texture.GetImageColorArray();
            switch (texture.Format) {
                case SurfacePixelFormat.PFID_R8G8B8:
                case SurfacePixelFormat.PFID_CUSTOM_LSCAPE_R8G8B8:
                    for (int i = 0; i < texture.Height; i++)
                        for (int j = 0; j < texture.Width; j++) {
                            int idx = (i * texture.Width) + j;
                            int r = (colorArray[idx] & 0xFF0000) >> 16;
                            int g = (colorArray[idx] & 0xFF00) >> 8;
                            int b = colorArray[idx] & 0xFF;
                            image.SetPixel(j, i, Color.FromArgb(r, g, b));
                        }
                    break;
                case SurfacePixelFormat.PFID_A8R8G8B8:
                    for (int i = 0; i < texture.Height; i++)
                        for (int j = 0; j < texture.Width; j++) {
                            int idx = (i * texture.Width) + j;
                            int a = (int)((colorArray[idx] & 0xFF000000) >> 24);
                            int r = (colorArray[idx] & 0xFF0000) >> 16;
                            int g = (colorArray[idx] & 0xFF00) >> 8;
                            int b = colorArray[idx] & 0xFF;
                            image.SetPixel(j, i, Color.FromArgb(a, r, g, b));
                        }
                    break;
                case SurfacePixelFormat.PFID_INDEX16:
                case SurfacePixelFormat.PFID_P8:
                    Palette pal = UBService.PortalDat.ReadFromDat<Palette>((uint)texture.DefaultPaletteId);

                    // Apply any custom palette colors, if any, to our loaded palette (note, this may be all of them!)
                    if (texture.CustomPaletteColors.Count > 0)
                        foreach (KeyValuePair<int, uint> entry in texture.CustomPaletteColors)
                            if (entry.Key <= pal.Colors.Count)
                                pal.Colors[entry.Key] = entry.Value;

                    for (int i = 0; i < texture.Height; i++)
                        for (int j = 0; j < texture.Width; j++) {
                            int idx = (i * texture.Width) + j;
                            int a = (int)((pal.Colors[colorArray[idx]] & 0xFF000000) >> 24);
                            int r = (int)(pal.Colors[colorArray[idx]] & 0xFF0000) >> 16;
                            int g = (int)(pal.Colors[colorArray[idx]] & 0xFF00) >> 8;
                            int b = (int)pal.Colors[colorArray[idx]] & 0xFF;
                            image.SetPixel(j, i, Color.FromArgb(a, r, g, b));
                        }
                    break;
                case SurfacePixelFormat.PFID_A8:
                case SurfacePixelFormat.PFID_CUSTOM_LSCAPE_ALPHA:
                    for (int i = 0; i < texture.Height; i++)
                        for (int j = 0; j < texture.Width; j++) {
                            int idx = (i * texture.Width) + j;
                            int r = colorArray[idx];
                            int g = colorArray[idx];
                            int b = colorArray[idx];
                            image.SetPixel(j, i, Color.FromArgb(r, g, b));
                        }
                    break;
                case SurfacePixelFormat.PFID_R5G6B5: // 16-bit RGB
                    for (int i = 0; i < texture.Height; i++)
                        for (int j = 0; j < texture.Width; j++) {
                            int idx = 3 * ((i * texture.Width) + j);
                            int r = (int)(colorArray[idx]);
                            int g = (int)(colorArray[idx + 1]);
                            int b = (int)(colorArray[idx + 2]);
                            image.SetPixel(j, i, Color.FromArgb(r, g, b));
                        }
                    break;
                case SurfacePixelFormat.PFID_A4R4G4B4:
                    for (int i = 0; i < texture.Height; i++)
                        for (int j = 0; j < texture.Width; j++) {
                            int idx = 4 * ((i * texture.Width) + j);
                            int a = (colorArray[idx]);
                            int r = (colorArray[idx + 1]);
                            int g = (colorArray[idx + 2]);
                            int b = (colorArray[idx + 3]);
                            image.SetPixel(j, i, Color.FromArgb(a, r, g, b));
                        }
                    break;
            }
            return image;
        }

        private string ResolvePath(string filename) {
            if (string.IsNullOrEmpty(filename))
                return null;

            if (filename.StartsWith(System.IO.Path.DirectorySeparatorChar.ToString())) {
                filename = filename.Substring(1);
            }

            var path = System.IO.Path.GetFullPath(System.IO.Path.Combine(ScriptDirectory, filename));

            if (!path.StartsWith(ScriptDirectory))
                return null;

            return path;
        }

        public void Dispose() {
            if (UBService.Huds?.HudBar != null) {
                UBService.Huds.HudBar.OnHudBarContextRender -= HudBar_OnHudBarContextRender;
            }

            var huds = _huds.ToArray();
            foreach (var hud in huds) {
                hud.Dispose();
            }
            _huds.Clear();

            foreach (var texture in _textures) {
                texture.Dispose();
            }
            _textures.Clear();

            _bmpIcon?.Dispose();
        }
    }
}
