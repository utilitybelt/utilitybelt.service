﻿using ImGuiNET;
using ImPlotNET;
using Microsoft.DirectX.Direct3D;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using UtilityBelt.Service.Lib;

namespace UtilityBelt.Service.Views {
    public class HudProfiler : IDisposable {
        public static readonly int MAX_LENGTH = 1000;

        private static double[] _buffer = new double[MAX_LENGTH];

        public class HudStats {
            public double[] PreRenderTimeMS = new double[MAX_LENGTH];

            public double[] RenderTimeMS = new double[MAX_LENGTH];
            public double[] PostRenderTimeMS = new double[MAX_LENGTH];

            public double[] ALL = new double[MAX_LENGTH * 3];

            public Hud Hud { get; }

            public HudStats(Hud hud) {
                Hud = hud;
            }

            public void Update() {
                Array.Copy(PreRenderTimeMS, 1, _buffer, 0, MAX_LENGTH - 1);
                _buffer[MAX_LENGTH - 1] = Hud.LastPreRenderTimeMS;
                Array.Copy(_buffer, PreRenderTimeMS, MAX_LENGTH);

                Array.Copy(RenderTimeMS, 1, _buffer, 0, MAX_LENGTH - 1);
                _buffer[MAX_LENGTH - 1] = Hud.LastRenderTimeMS;
                Array.Copy(_buffer, RenderTimeMS, MAX_LENGTH);

                Array.Copy(PostRenderTimeMS, 1, _buffer, 0, MAX_LENGTH - 1);
                _buffer[MAX_LENGTH - 1] = Hud.LastPostRenderTimeMS;
                Array.Copy(_buffer, PostRenderTimeMS, MAX_LENGTH);

                Array.Copy(PreRenderTimeMS, 0, ALL, 0, MAX_LENGTH);
                Array.Copy(RenderTimeMS, 0, ALL, MAX_LENGTH, MAX_LENGTH);
                Array.Copy(PostRenderTimeMS, 0, ALL, MAX_LENGTH * 2, MAX_LENGTH);
            }
        }

        private Hud hud;
        private Dictionary<Hud, HudStats> _hudStats = new Dictionary<Hud, HudStats>();

        public HudProfiler() {
            using (Stream manifestResourceStream = GetType().Assembly.GetManifestResourceStream("UtilityBelt.Service.Resources.icons.hudprofiler.png")) {
                hud = UBService.Huds.CreateHud($"Hud Profiler", new Bitmap(manifestResourceStream));
            }

            hud.OnRender += Hud_OnRender;

            var _huds = UBService.Huds.Huds.ToArray();
            foreach (var hud in _huds) {
                _hudStats.Add(hud, new HudStats(hud));
            }

            UBService.Huds.OnHudAdded += Huds_OnHudAdded;
            UBService.Huds.OnHudRemoved += Huds_OnHudRemoved;
            UBService.OnTick += UBService_OnTick;
        }


        private void Huds_OnHudAdded(object sender, HudAddedEventArgs e) {
            _hudStats.Add(e.Hud, new HudStats(e.Hud));
        }

        private void Huds_OnHudRemoved(object sender, HudRemovedEventArgs e) {
            _hudStats.Remove(e.Hud);
        }

        private void UBService_OnTick(object sender, EventArgs e) {
            var huds = _hudStats.Values.ToArray();
            foreach (var hudStat in huds) {
                hudStat.Update();
            }
        }


        private static string[] labels = new string[] { "PreRender", "Render", "PostRender" };
        private void Hud_OnRender(object sender, EventArgs e) {
            ImGui.Text($"Tracking {_hudStats.Count} Huds");
            var _huds = _hudStats.Values.ToArray();
            foreach (var stats in _huds) {
                ImGui.Text($"Stats for {stats.Hud.Name}");
                if (ImPlot.BeginPlot($"{stats.Hud.Name} Render Times", new System.Numerics.Vector2(-1, 80), ImPlotFlags.NoTitle | ImPlotFlags.NoMouseText)) {
                    ImPlot.SetupAxes("Frames", "Time (ms)", ImPlotAxisFlags.AutoFit | ImPlotAxisFlags.NoDecorations, ImPlotAxisFlags.None);
                    ImPlot.SetupAxisLimitsConstraints(ImAxis.Y1, 0, 1000);
                    ImPlot.SetupAxisLimits(ImAxis.X1, 0, MAX_LENGTH);
                    ImPlot.PlotBarGroups(labels, ref stats.ALL[0], labels.Length, MAX_LENGTH, 1, 0, ImPlotBarGroupsFlags.Stacked);
                    ImPlot.EndPlot();
                }
            }
        }

        public void Dispose() {
            UBService.OnTick -= UBService_OnTick;
            UBService.Huds.OnHudAdded -= Huds_OnHudAdded;
            UBService.Huds.OnHudRemoved -= Huds_OnHudRemoved;
            hud.OnRender -= Hud_OnRender;
            hud.Dispose();
        }
    }
}
