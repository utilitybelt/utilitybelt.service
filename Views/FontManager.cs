﻿
using ImGuiNET;
using Microsoft.Extensions.Logging;
using RoyT.TrueType;
using RoyT.TrueType.Helpers;
using RoyT.TrueType.Tables.Name;
using System;
using System.Collections.Generic;
using System.Drawing.Text;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using static UtilityBelt.Service.Views.FontManager;

namespace UtilityBelt.Service.Views {

    public class FontManager {
        private Dictionary<FontKey, FontInstance> _loadedFonts = new Dictionary<FontKey, FontInstance>();
        private Dictionary<string, FontInfo> _availableFonts = new Dictionary<string, FontInfo>();

        public IEnumerable<FontInfo> AvailableFonts => _availableFonts.Values;
        public IEnumerable<FontInstance> LoadedFonts => _loadedFonts.Values;

        private struct FontKey {
            public FontInfo Info;
            public int Size;

            public FontKey(FontInfo info, int size) {
                Info = info;
                Size = size;
            }

            public override bool Equals(object obj) {
                return obj is FontKey key && key.Info == Info && key.Size == Size;
            }

            public override int GetHashCode() {
                unchecked {
                    int hash = 17;
                    hash = hash * 23 + Info.GetHashCode();
                    hash = hash * 23 + Size.GetHashCode();
                    return hash;
                }
            }
        }

        public FontManager() {
            InitFontList();
        }

        public FontInfo FindAvailableBestMatch(List<string> families, FontSubFamily subFamily) {
            families = families.Select(f => f.ToLower().Trim()).ToList();

            var exactMatch = _availableFonts.Values.FirstOrDefault(f => families.Contains(f.Family.ToLower()) && f.SubFamily == subFamily);
            if (exactMatch != null) {
                return exactMatch;
            }

            return _availableFonts.Values.FirstOrDefault(f => families.Contains(f.Family.ToLower()) && f.SubFamily == FontSubFamily.Regular) ?? _availableFonts.Values.FirstOrDefault();
        }

        public FontInstance Create(FontInfo fontInfo, int size) {
            var key = new FontKey(fontInfo, size);

            if (_loadedFonts.TryGetValue(key, out var fontInstance)) {
                return fontInstance;
            }

            fontInstance = new FontInstance(fontInfo, size);
            _loadedFonts.Add(key, fontInstance);

            return fontInstance;
        }

        public bool TryPushFont(FontInstance font) {
            var key = new FontKey(font.Info, font.Size);
            if (_loadedFonts.ContainsKey(key)) {
                ImGui.PushFont(font.Font);
                return true;
            }
            return false;
        }

        public bool TryPopFont(FontInstance font) {
            var key = new FontKey(font.Info, font.Size);
            if (_loadedFonts.ContainsKey(key)) {
                ImGui.PopFont();
                return true;
            }
            return false;
        }

        public bool RegisterFont(string filename) {
            try {
                if (!File.Exists(filename)) {
                    UBService.WriteLog($"Font file does not exist: {filename}", LogLevel.Warning);
                }

                // TODO: i dunno why these dont work... skip for now
                var font = TrueTypeFont.FromFile(filename);
                var fontInfo = new FontInfo(filename, font);
                if (fontInfo.Family.ToLower().Contains("webdings") || fontInfo.Family.ToLower().Contains("wingdings")) {
                    return false;
                }

                if (_availableFonts.ContainsKey(filename)) {
                    _availableFonts.Remove(filename);
                }

                _availableFonts.Add(filename, fontInfo);
            }
            catch (Exception ex) {
                UBService.LogException(ex);
            }
            return false;
        }

        private void InitFontList() {
            var fontFiles = new List<string>();
            var fontsList = Directory.GetFiles(Path.Combine(UBService.AssemblyDirectory, "fonts"), "*.ttf");
            foreach (var file in fontsList) {
                fontFiles.Add(file);
            }

#if NET48_OR_GREATER
            var winfontsList = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Fonts), "*.ttf");
            foreach (var file in winfontsList) {
                var fontName = Path.GetFileNameWithoutExtension(file).ToLower();
                fontFiles.Add(file);
            }
#endif
            foreach (var file in fontFiles) {
                RegisterFont(file);
            }
        }
    }
}
