﻿using IconFonts;
using ImGuiNET;
using System.IO;
using UtilityBelt.Service.Lib;

namespace UtilityBelt.Service.Views {
    public unsafe class FontInstance {
        public FontInfo Info { get; }
        public int Size { get; }
        public ImFontPtr Font { get; }
        private ushort[] _iconFontRanges = new ushort[] { 0xf000, 0xf2e0, 0 };
        private ImFontConfig* _fontConfig = ImGuiNative.ImFontConfig_ImFontConfig();

        public unsafe FontInstance(FontInfo fontInfo, int size) {
            Info = fontInfo;
            Size = size;

            _fontConfig->MergeMode = 1;
            var configPtr = new ImFontConfigPtr(_fontConfig);
            var fontAwesomePath = Path.Combine(UBService.AssemblyDirectory, "fonts", "fontawesome-webfont.ttf");
            var cfilename = fontInfo.Filename.Replace("\\", "\\\\");
            var cwfilename = fontAwesomePath.Replace("\\", "\\\\");
            Font = ImGui.GetIO().Fonts.AddFontFromFileTTF(cfilename, size);
            //fixed (ushort* iconRangesPtr = &_iconFontRanges[0]) {
            //    ImGui.GetIO().Fonts.AddFontFromFileTTF(cwfilename, size, configPtr, (nint)iconRangesPtr);
            //}

            UBService.Huds.RebuildFonts();
        }

        public override bool Equals(object obj) {
            return obj is FontInstance fk
                && fk.Info.Family == Info.Family
                && fk.Size == Size
                && fk.Info.SubFamily == Info.SubFamily;
        }

        public override int GetHashCode() {
            unchecked {
                int hash = 17;
                hash = hash * 23 + Info.Family.GetHashCode();
                hash = hash * 23 + Info.SubFamily.GetHashCode();
                hash = hash * 23 + Size.GetHashCode();
                return hash;
            }
        }
    }
}
